﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.FleetManagement.Web
{
    public static class Constants
    {
        public static class Security
        {
            public static class CustomUserRoles
            {
                public const string Mechanic = nameof(Mechanic);

                public static readonly IDictionary<string, string> Items = new Dictionary<string, string>
                {
                    { Mechanic, Mechanic },
                };
            }
        }

    }
}
