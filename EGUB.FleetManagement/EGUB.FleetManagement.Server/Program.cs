using Autofac.Extensions.DependencyInjection;
using EGUB.Core.Contracts.DAL;
using EGUB.FleetManagement.Server;

var builder = CreateHostBuilder(args);
var app = builder.Build();

using (var scope = app.Services.CreateScope())
{
    var container = scope.ServiceProvider.GetService<EGUB.Core.Contracts.IContainer>();
    var migrator = container!.GetItem<IMigrator>(Startup.FleetManagementContext);
    await migrator.Execute();
}

await app.RunAsync();

static IHostBuilder CreateHostBuilder(string[] args) =>
    Host.CreateDefaultBuilder(args)
        .UseServiceProviderFactory(new AutofacServiceProviderFactory())
        .ConfigureWebHostDefaults(webBuilder =>
        {
            webBuilder
                .UseStartup<Startup>();
        });
