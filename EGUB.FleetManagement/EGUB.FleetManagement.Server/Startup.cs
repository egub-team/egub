﻿using Autofac;
using EGUB.Core.Contracts.DAL;
using EGUB.Core.DAL;
using EGUB.Security.Api;
using EGUB.Security.DAL;
using IContainer = EGUB.Core.Contracts.IContainer;


namespace EGUB.FleetManagement.Server
{
    public class Startup : SecurityStartup
    {
        public const string FleetManagementContext = nameof(FleetManagementContext);
        public const string FleetManagementMechanicContext = nameof(FleetManagementMechanicContext);
        protected override string Title => "EGUB.FleetManagement.Server";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterModule<Core.Common.ContainerFactory>();
            builder.RegisterModule<Core.DAL.ContainerFactory>();

            builder
                .RegisterInstance(this.Configuration)
                .SingleInstance();
            builder.RegisterModule<Security.DAL.ContainerFactory>();
            builder
                .Register(c => new Context(FleetManagementContext, c.Resolve<IConfiguration>()))
                .Named<IContext>(FleetManagementContext)
                .InstancePerLifetimeScope();
            builder
                .Register(c => new SecurityMigrator(FleetManagementContext, false, c.Resolve<IContainer>()))
                .Named<IMigrator>(FleetManagementContext)
                .InstancePerLifetimeScope();

            Security.BLL.ContainerFactory.RegisterUserService(builder, FleetManagementContext);
            builder.RegisterModule<Security.Api.ContainerFactory>();

            builder
                .Register(c => new Context(FleetManagementMechanicContext, c.Resolve<IConfiguration>()))
                .Named<IContext>(FleetManagementMechanicContext)
                .InstancePerLifetimeScope();
            builder.RegisterModule<DAL.ContainerFactory>();
            BLL.ContainerFactory.RegisterMechanicService(builder, FleetManagementMechanicContext);
            builder.RegisterModule<Api.ContainerFactory>();
        }
    }
}
