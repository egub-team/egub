﻿using Autofac;
using EGUB.FleetManagement.Contracts.DAL.Mechanic;
using EGUB.FleetManagement.DAL.Mechanic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.FleetManagement.DAL
{
    public class ContainerFactory : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            RegisterMechanicRepository(builder);
        }

        public static void RegisterMechanicRepository(ContainerBuilder builder)
        {
            builder
                .RegisterType<MechanicRepository>()
                .As<IMechanicRepository>()
                .InstancePerLifetimeScope();
        }
    }
}
