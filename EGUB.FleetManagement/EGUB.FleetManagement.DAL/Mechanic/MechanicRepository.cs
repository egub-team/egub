﻿using EGUB.Core.DAL;
using EGUB.FleetManagement.Contracts.DAL.Mechanic;
using EGUB.FleetManagement.DAL.Mechanic.SQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.FleetManagement.DAL.Mechanic
{
    internal class MechanicRepository : BaseRepository, IMechanicRepository
    {
        public Task<ICollection<(int Id, string Status, string Number, string Date, string InCharge, string Driver, string Warehouse, string Truck, string Notes)>> GetPartsOrders(string userName)
        {
            return Context
                .Query(r =>
                    (
                        Id: r.GetValue<int>("id"),
                        Status: r.GetValue<string>("Status"),
                        Number: r.GetValue<string>("Number"),
                        Date: r.GetValue<string>("Date"),
                        InCharge: r.GetValue<string>("InCharge"),
                        Driver: r.GetValue<string>("Driver"),
                        Warehouse: r.GetValue<string>("Warehouse"),
                        Truck: r.GetValue<string>("Truck"),
                        Notes: r.GetValue<string>("Notes")
                    ),
                    Commands.GetPartsOrders,
                    new { userName });
        }
    }
}
