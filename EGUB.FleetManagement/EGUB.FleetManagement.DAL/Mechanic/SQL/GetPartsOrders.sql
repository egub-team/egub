﻿SELECT 
    dpo.Id AS id,
    st.StatName AS Status,
    dpo.Number AS Number,
    UPPER(FORMAT(dpo.Date , 'dd-MMM-yyyy','en-us')) AS [Date],
    CONCAT(inch.persLastName, ' ' , inch.persFirstName) AS InCharge,
    CONCAT(dr.persLastName, ' ' , dr.persFirstName) AS Driver,
    str.storName AS Warehouse,
    ob.unitNumber AS Truck,
    dpo.Notes              
FROM DriverPartsOrder dpo
LEFT JOIN coreCurrentStatuses cs ON cs.CustObject = dpo.Id
LEFT JOIN coreStatuses st ON st.StatId = cs.CustStat
LEFT JOIN persones inch ON dpo.InCharge = inch.persId
LEFT JOIN persones dr ON dpo.Driver = dr.persId
LEFT JOIN Stores str ON str.storId = dpo.Warehouse
LEFT JOIN coreUsers ur ON ur.userPersone =  dpo.InCharge
LEFT JOIN vwObjects ob ON Ob.obId = dpo.Truck
WHERE ur.userName = @userName AND DATEDIFF(HOUR,dpo.Date,  GETDATE()) < 72