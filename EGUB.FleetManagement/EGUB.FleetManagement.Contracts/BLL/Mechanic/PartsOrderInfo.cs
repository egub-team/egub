﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.FleetManagement.Contracts.BLL.Mechanic
{
    public class PartsOrderInfo
    {
        public int Id { get; set; }
        public string Status { get; set; }
        public string Number { get; set; }
        public string Date { get; set; }
        public string InCharge { get; set; }
        public string Driver { get; set; }
        public string Warehouse { get; set; }
        public string Truck { get; set; }
        public string Notes { get; set; }

    }
}
