﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EGUB.Core.Contracts.BLL;

namespace EGUB.FleetManagement.Contracts.BLL.Mechanic
{
    public interface IMechanicService
    {
        Task<ICollection<PartsOrderInfo>> GetPartsOrders(IOperationContext context);
    }
}
