﻿using EGUB.Core.Contracts.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.FleetManagement.Contracts.DAL.Mechanic
{
    public interface IMechanicRepository : IRepository
    {
        Task<ICollection<(
            int Id,
            string Status,
            string Number,
            string Date,
            string InCharge,
            string Driver,
            string Warehouse,
            string Truck,
            string Notes)>> GetPartsOrders(string userName);
    }
}
