﻿using Autofac;
using EGUB.Core.Contracts.DAL;
using EGUB.Core.DAL;
using EGUB.FleetManagement.Contracts.DAL.Mechanic;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IContainer = EGUB.Core.Contracts.IContainer;

namespace EGUB.FleetManagement.Tests.DAL
{
    [TestClass]
    public class MechanicRepositoryTests
    {
        [TestMethod]
        public Task GetPartsOrdersTest() =>
            Core.Common.ContainerFactory.Execute(
                builder =>
                {
                    builder
                        .Register(c => new Context(nameof(GetPartsOrdersTest), c.Resolve<IConfiguration>()))
                        .Named<IContext>(nameof(GetPartsOrdersTest))
                        .InstancePerLifetimeScope();
                    builder
                        .Register(c => new GetPartsOrdersTestMigrator(nameof(GetPartsOrdersTest), c.Resolve<IContainer>()))
                        .Named<IMigrator>(nameof(GetPartsOrdersTest))
                        .InstancePerLifetimeScope();

                    ContainerFactory.RegisterContextContainer(builder);
                    ContainerFactory.RegisterRepositoryContainer(builder);

                    FleetManagement.DAL.ContainerFactory.RegisterMechanicRepository(builder);
                },
                async container =>
                {
                    var migrator = container.GetItem<IMigrator>(nameof(GetPartsOrdersTest));
                    await migrator.Execute();

                    var contextContainer = container.GetItem<IContextContainer>();
					await contextContainer.ExecuteWithContext(
						nameof(GetPartsOrdersTest),
						async c =>
						{
                            var repository = c.GetRepository<IMechanicRepository>();
                            var items = await repository.GetPartsOrders(nameof(GetPartsOrdersTestMigrator));

                            Assert.IsNotNull(items);
                            Assert.AreEqual(1, items.Count);

                            var actual = items.Single();
                            var expected = migrator as GetPartsOrdersTestMigrator;

                            Assert.AreEqual(expected.Id, actual.Id);
                            Assert.AreEqual(expected.Status, actual.Status);
                            Assert.AreEqual(expected.Number, actual.Number);
                            Assert.AreEqual(expected.Date, actual.Date);
                            Assert.AreEqual(expected.InCharge, actual.InCharge);
                            Assert.AreEqual(expected.Driver, actual.Driver);
                            Assert.AreEqual(expected.Warehouse, actual.Warehouse);
                            Assert.AreEqual(expected.Truck, actual.Truck);
                            Assert.AreEqual(expected.Notes, actual.Notes);
						});
                });

        private class GetPartsOrdersTestMigrator : Migrator, IMigrator
        {
            public int Id => 1;
			public string Status => nameof(Status);
            public string Number => nameof(Number);
			public string Date => "01-MAR-2023";
			public string InCharge => "InCharge_LastName InCharge_FirstName";
			public string Driver => "Driver_LastName Driver_FirstName";
            public string Warehouse => nameof(Warehouse);
            public string Truck => nameof(Truck);
            public string Notes => nameof(Notes);

            public GetPartsOrdersTestMigrator(string name, IContainer container) : base(name, true, container)
            {
                AddScript<GetPartsOrdersTestMigrator>(CreateTables);
                AddScript<GetPartsOrdersTestMigrator>(SeedData);
            }

            private async Task CreateTables(IContext context)
            {
                await context.Execute(
                    @"CREATE TABLE [dbo].[DriverPartsOrder](
	                    [Id] [int] NOT NULL,
	                    [Number] [nvarchar](50) NOT NULL,
	                    [Date] [date] NOT NULL,
	                    [InCharge] [int] NOT NULL,
	                    [Driver] [int] NOT NULL,
	                    [Truck] [int] NOT NULL,
	                    [Trailer] [int] NULL,
	                    [Warehouse] [int] NOT NULL,
	                    [Notes] [nvarchar](max) NULL,
                        CONSTRAINT [PK_DriverPartsOrder_Id] PRIMARY KEY CLUSTERED ([Id] ASC))");

                await context.Execute(
                    @"CREATE TABLE [dbo].[coreCurrentStatuses](
	                    [CustStat] [int] NOT NULL,
	                    [CustObject] [int] NOT NULL,
	                    [CustUser] [int] NOT NULL,
	                    [CustStatUserDate] [datetime] NOT NULL,
	                    [CustStatSetDate] [datetime] NOT NULL,
	                    [CustStatComment] [nvarchar](256) NULL,
	                    [CustAddAttr] [int] NULL,
                        CONSTRAINT [PK_coreCurrentStatuses] PRIMARY KEY CLUSTERED ([CustObject] ASC ))");

                await context.Execute(
                    @"CREATE TABLE [dbo].[coreStatuses](
	                    [StatId] [int] NOT NULL,
	                    [StatClass] [smallint] NOT NULL,
	                    [StatName] [nvarchar](128) NOT NULL,
	                    [StatCode] [nvarchar](64) NOT NULL,
	                    [StatIsBlocking] [bit] NOT NULL,
	                    [StatIsArchival] [bit] NOT NULL,
	                    [StatIsInitial] [bit] NOT NULL,
	                    [StatComment] [nvarchar](128) NULL,
	                    [StatCssStyle] [nvarchar](128) NULL,
                        CONSTRAINT [PK_coreStatuses] PRIMARY KEY CLUSTERED ([StatId] ASC))");

                await context.Execute(
                    @"CREATE TABLE [dbo].[persones](
	                    [persId] [int] NOT NULL,
	                    [persFirstName] [nvarchar](50) NOT NULL,
	                    [persLastName] [nvarchar](50) NOT NULL,
	                    [persPName] [nvarchar](50) NULL,
	                    [persCode] [nvarchar](50) NULL,
	                    [persBirthDate] [date] NULL,
	                    [persBirthPlace] [nvarchar](128) NULL,
	                    [persIsMarried] [smallint] NULL,
	                    [persFactCity] [int] NULL,
	                    [persFactPostIndex] [nvarchar](50) NULL,
	                    [persFactAdress] [nvarchar](128) NULL,
	                    [persRegCity] [int] NULL,
	                    [persRegPostIndex] [nvarchar](50) NULL,
	                    [persRegAdress] [nvarchar](128) NULL,
	                    [persMobTel] [nvarchar](50) NULL,
	                    [persWorkTel] [nvarchar](50) NULL,
	                    [persWeb] [nvarchar](50) NULL,
	                    [persEmail] [nvarchar](64) NULL,
	                    [persTaxId] [nvarchar](50) NULL,
	                    [persPassportSerialNumber] [nvarchar](50) NULL,
	                    [persPassportNumber] [nvarchar](50) NULL,
	                    [persPassportIssued] [nvarchar](128) NULL,
	                    [persPassportIssuedDate] [date] NULL,
	                    [persNotes] [ntext] NULL,
	                    [persWorkPhoneExt] [nvarchar](16) NULL,
	                    [persIsBusinessman] [bit] NULL,
	                    [persSpousePerson] [int] NULL,
	                    [persHireDate] [datetime] NULL,
	                    [persTerminateDate] [datetime] NULL,
                        CONSTRAINT [PK_persones] PRIMARY KEY CLUSTERED ([persId] ASC ))");

                await context.Execute(
                    @"CREATE TABLE [dbo].[Stores](
	                    [storId] [int] NOT NULL,
	                    [storName] [nvarchar](256) NOT NULL,
	                    [storCode] [nvarchar](32) NULL,
	                    [storCont] [int] NULL,
                        CONSTRAINT [PK_Stores_storId] PRIMARY KEY CLUSTERED ([storId] ASC))");

				await context.Execute(
					@"CREATE TABLE [dbo].[coreUsers](
						[userId] [int] NOT NULL,
						[userIdentityId] [nvarchar](128) NULL,
						[userName] [nvarchar](64) NULL,
						[userDomainName] [nchar](64) NULL,
						[userPersone] [int] NULL,
						[userLocked] [bit] NULL,
						[userAspAuthEnabled] [bit] NULL,
						[userFilter] [bit] NOT NULL,
						CONSTRAINT [PK_coreUsers] PRIMARY KEY CLUSTERED ([userId] ASC))");

				await context.Execute(
					@"CREATE TABLE [dbo].[vwObjects](
						[obId] [int] NOT NULL,
						[obRegNum] [nvarchar](32) NULL,
						[obCode] [nvarchar](64) NULL,
						[obType] [varchar](16) NOT NULL,
						[fullModName] [nvarchar](256) NULL,
						[obFullName] [nvarchar](99) NULL,
						[unitNumber] [nvarchar](32) NULL,
						[obIssueYear] [smallint] NULL,
						[lookupText] [nvarchar](134) NULL,
						[lookupInfo] [nvarchar](422) NULL,
						[lookupSearchIndex] [nvarchar](32) NULL,
						[objtUser] [int] NULL,
						[StatCode] [nvarchar](64) NULL)");
            }

            private async Task SeedData(IContext context)
            {
				await context.Execute(
					$@"INSERT INTO [dbo].[DriverPartsOrder]
						([Id]
						,[Number]
						,[Date]
						,[InCharge]
						,[Driver]
						,[Truck]
						,[Trailer]
						,[Warehouse]
						,[Notes])
					VALUES
						({Id}
						,'{Number}'
						,'2023-03-1'
						,1
						,2
						,1
						,NULL
						,1
						,'{Notes}')");

				await context.Execute(
					$@"INSERT INTO [dbo].[coreCurrentStatuses]
						([CustStat]
						,[CustObject]
						,[CustUser]
						,[CustStatUserDate]
						,[CustStatSetDate]
						,[CustStatComment]
						,[CustAddAttr])
					VALUES
						(1
						,{Id}
						,0
						,'2023-01-01'
						,'2023-01-01'
						,NULL
						,NULL)");


				await context.Execute(
					$@"INSERT INTO [dbo].[coreStatuses]
						([StatId]
						,[StatClass]
						,[StatName]
						,[StatCode]
						,[StatIsBlocking]
						,[StatIsArchival]
						,[StatIsInitial]
						,[StatComment]
						,[StatCssStyle])
					VALUES
						(1
						,0
						,'{Status}'
						,''
						,0
						,0
						,0
						,NULL
						,NULL)");

				await context.Execute(
					$@"INSERT INTO [dbo].[persones]
						([persId]
						,[persFirstName]
						,[persLastName]
						,[persPName]
						,[persCode]
						,[persBirthDate]
						,[persBirthPlace]
						,[persIsMarried]
						,[persFactCity]
						,[persFactPostIndex]
						,[persFactAdress]
						,[persRegCity]
						,[persRegPostIndex]
						,[persRegAdress]
						,[persMobTel]
						,[persWorkTel]
						,[persWeb]
						,[persEmail]
						,[persTaxId]
						,[persPassportSerialNumber]
						,[persPassportNumber]
						,[persPassportIssued]
						,[persPassportIssuedDate]
						,[persNotes]
						,[persWorkPhoneExt]
						,[persIsBusinessman]
						,[persSpousePerson]
						,[persHireDate]
						,[persTerminateDate])
					VALUES
						(1
						,'InCharge_FirstName'
						,'InCharge_LastName'
						,NULL
						,NULL
						,NULL
						,NULL
						,NULL
						,NULL
						,NULL
						,NULL
						,NULL
						,NULL
						,NULL
						,NULL
						,NULL
						,NULL
						,NULL
						,NULL
						,NULL
						,NULL
						,NULL
						,NULL
						,NULL
						,NULL
						,NULL
						,NULL
						,NULL
						,NULL),
						(2
						,'Driver_FirstName'
						,'Driver_LastName'
						,NULL
						,NULL
						,NULL
						,NULL
						,NULL
						,NULL
						,NULL
						,NULL
						,NULL
						,NULL
						,NULL
						,NULL
						,NULL
						,NULL
						,NULL
						,NULL
						,NULL
						,NULL
						,NULL
						,NULL
						,NULL
						,NULL
						,NULL
						,NULL
						,NULL
						,NULL)");

				await context.Execute(
					$@"INSERT INTO [dbo].[Stores]
						([storId]
						,[storName]
						,[storCode]
						,[storCont])
					VALUES
						(1
						,'{Warehouse}'
						,NULL
						,NULL)");

				await context.Execute(
					$@"INSERT INTO [dbo].[coreUsers]
						([userId]
						,[userIdentityId]
						,[userName]
						,[userDomainName]
						,[userPersone]
						,[userLocked]
						,[userAspAuthEnabled]
						,[userFilter])
					VALUES
						(1
						,NULL
						,'{nameof(GetPartsOrdersTestMigrator)}'
						,NULL
						,1
						,NULL
						,NULL
						,0)");


				await context.Execute(
					$@"INSERT INTO [dbo].[vwObjects]
						([obId]
						,[obRegNum]
						,[obCode]
						,[obType]
						,[fullModName]
						,[obFullName]
						,[unitNumber]
						,[obIssueYear]
						,[lookupText]
						,[lookupInfo]
						,[lookupSearchIndex]
						,[objtUser]
						,[StatCode])
					VALUES
						(1
						,NULL
						,NULL
						,'obType'
						,NULL
						,NULL
						,'{Truck}'
						,NULL
						,NULL
						,NULL
						,NULL
						,NULL
						,NULL)");
            }
        }

    }


}
