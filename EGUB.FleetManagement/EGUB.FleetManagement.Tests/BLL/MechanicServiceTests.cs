﻿using Autofac;
using EGUB.Core.Contracts.BLL;
using EGUB.Core.Contracts.DAL;
using EGUB.FleetManagement.Contracts.BLL.Mechanic;
using EGUB.FleetManagement.Contracts.DAL.Mechanic;
using EGUB.Security.Contracts;
using EGUB.Security.Contracts.BLL;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IContainer = EGUB.Core.Contracts.IContainer;

namespace EGUB.FleetManagement.Tests.BLL
{
    [TestClass]
    public class MechanicServiceTests
    {

        [TestMethod]
        public Task GetPartsOrdersTest() =>
            Core.Common.ContainerFactory.Execute(
                builder =>
                {
                    builder.RegisterType<TestContextContainer>().As<IContextContainer>().InstancePerLifetimeScope();
                    builder.RegisterType<TestRepositoryContainer>().As<IRepositoryContainer>().InstancePerLifetimeScope();
                    builder.RegisterType<TestMechanicRepository>().As<IMechanicRepository>().InstancePerLifetimeScope();

                    builder.RegisterType<TestUserService>().As<IUserService>().InstancePerLifetimeScope();

                    FleetManagement.BLL.ContainerFactory.RegisterMechanicService(builder, string.Empty);
                },
                async container =>
                {
                    var service = container.GetItem<IMechanicService>();
                    var actual = await service.GetPartsOrders(new TestOperationContext());

                    var repository = container.GetItem<IMechanicRepository>() as TestMechanicRepository;
                    var expected = repository.Items;

                    Assert.AreEqual(expected.Count, actual.Count);

                    for ( int i = 0; i < actual.Count; i++ )
                    {
                        var actualItem = actual.ElementAt(i);
                        var expectedItem = expected.ElementAt(i);

                        Assert.AreEqual(expectedItem.Id, actualItem.Id);
                        Assert.AreEqual(expectedItem.Number, actualItem.Number);
                        Assert.AreEqual(expectedItem.Status, actualItem.Status);
                        Assert.AreEqual(expectedItem.Date, actualItem.Date);
                        Assert.AreEqual(expectedItem.InCharge, actualItem.InCharge);
                        Assert.AreEqual(expectedItem.Driver, actualItem.Driver);
                        Assert.AreEqual(expectedItem.Truck, actualItem.Truck);
                        Assert.AreEqual(expectedItem.Warehouse, actualItem.Warehouse);
                        Assert.AreEqual(expectedItem.Notes, actualItem.Notes);
                    }
                });


        private class TestOperationContext : IOperationContext
        {
            public Guid GetCurrentUserId()
            {
                return Guid.Empty;
            }
        }

        private class TestUserService : IUserService
        {
            public Task ChangePassword(IChangePasswordContext context)
            {
                throw new NotImplementedException();
            }

            public Task<bool> CheckPassword(string passwordHash, string password)
            {
                throw new NotImplementedException();
            }

            public Task ConfirmEmail(IConfirmEmailContext context)
            {
                throw new NotImplementedException();
            }

            public Task<Guid> CreateUser(ICreateUserContext context)
            {
                throw new NotImplementedException();
            }

            public Task<ICollection<User>> GetEntities(IGetEntitiesContext context)
            {
                var result = new List<User>
                {
                    new User
                    {
                        Email = "Email"
                    }
                };
                return Task.FromResult<ICollection<User>>(result);
            }

            public Task<ICollection<User>> GetEntities(IRepositoryContainer container, IGetEntitiesContext context)
            {
                throw new NotImplementedException();
            }

            public Task<User> GetUser(IGetUserContext context)
            {
                throw new NotImplementedException();
            }

            public Task<ICollection<User>> GetUsers(IGetUsersContext context)
            {
                throw new NotImplementedException();
            }

            public Task Remove(IRemoveEntityContext context)
            {
                throw new NotImplementedException();
            }

            public Task Remove(IRepositoryContainer container, IRemoveEntityContext context)
            {
                throw new NotImplementedException();
            }

            public Task ResetPassword(IResetPasswordContext context)
            {
                throw new NotImplementedException();
            }

            public Task Save(ISaveEntityContext<User> context)
            {
                throw new NotImplementedException();
            }

            public Task Save(IRepositoryContainer container, ISaveEntityContext<User> context)
            {
                throw new NotImplementedException();
            }

            public Task UpdateUser(IUpdateUserContext context)
            {
                throw new NotImplementedException();
            }
        }

        private class TestMechanicRepository : IMechanicRepository
        {
            public readonly ICollection<(int Id, string Status, string Number, string Date, string InCharge, string Driver, string Warehouse, string Truck, string Notes)> Items = new[]
            {
                (
                    Id: 1,
                    Status: "Status",
                    Number: "Number",
                    Date: "Date",
                    InCharge: "InCharge",
                    Driver: "Driver",
                    Warehouse: "Warehouse",
                    Truck: "Truck",
                    Notes: "Notes"
                )
            };

            public Task<ICollection<(int Id, string Status, string Number, string Date, string InCharge, string Driver, string Warehouse, string Truck, string Notes)>> GetPartsOrders(string userName)
            {
                return Task.FromResult(Items);
            }

            public void Init(IContext context)
            {
                throw new NotImplementedException();
            }
        }

        private class TestContextContainer : IContextContainer
        {
            private readonly IRepositoryContainer _repositoryContainer;

            public TestContextContainer(IRepositoryContainer repositoryContainer)
            {
                _repositoryContainer = repositoryContainer;
            }

            public Task ExecuteWithContext(string name, Func<IRepositoryContainer, Task> task)
            {
                return task(_repositoryContainer);
            }
        }

        private class TestRepositoryContainer : IRepositoryContainer
        {
            private readonly IContainer _container;

            public TestRepositoryContainer(IContainer container)
            {
                _container = container;
            }

            public void Init(IContext context)
            {
            }

            public TRepository GetRepository<TRepository>() where TRepository : IRepository
            {
                return _container.GetItem<TRepository>();
            }
        }
    }
}
