﻿using EGUB.Core.BLL;
using EGUB.Core.Contracts.BLL;
using EGUB.Core.Contracts.DAL;
using EGUB.FleetManagement.Contracts.BLL.Mechanic;
using EGUB.FleetManagement.Contracts.DAL.Mechanic;
using EGUB.Security.Contracts.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.FleetManagement.BLL.Mechanic
{
    internal class MechanicService : IMechanicService
    {
        private readonly string _contextName;
        private readonly IContextContainer _contextContainer;
        private readonly IUserService _userService;

        public MechanicService(
            string contextName,
            IContextContainer contextContainer,
            IUserService userService)
        {
            _contextName = contextName;
            _contextContainer = contextContainer;
            _userService = userService;
        }

        public async Task<ICollection<PartsOrderInfo>> GetPartsOrders(IOperationContext context)
        {
            var users = await _userService.GetEntities(new GetEntitiesContext(context, new[] { context.GetCurrentUserId() }));
            var userName = users
                .Single()
                .Email;

            var items = await Get(repository => repository.GetPartsOrders(userName));

            return items?
                .Select(i =>
                    new PartsOrderInfo
                    {
                        Id = i.Id,
                        Status = i.Status,
                        Number = i.Number,
                        Date = i.Date,
                        InCharge = i.InCharge,
                        Driver = i.Driver,
                        Warehouse = i.Warehouse,
                        Truck = i.Truck,
                        Notes = i.Notes,
                    })
                .ToList();
        }

        private async Task<T> Get<T>(Func<IMechanicRepository, Task<T>> func)
        {
            var result = default(T);
            await Execute(async repository => result = await func(repository));

            return result;
        }

        private Task Execute(Func<IMechanicRepository, Task> func) =>
            _contextContainer.ExecuteWithContext(_contextName, container =>
            {
                var repository = container.GetRepository<IMechanicRepository>();
                return func(repository);
            });
    }
}
