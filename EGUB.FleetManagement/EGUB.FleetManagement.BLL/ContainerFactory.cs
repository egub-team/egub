﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using EGUB.Core.Contracts.DAL;
using EGUB.FleetManagement.BLL.Mechanic;
using EGUB.FleetManagement.Contracts.BLL.Mechanic;
using EGUB.Security.Contracts.BLL;

namespace EGUB.FleetManagement.BLL
{
    public class ContainerFactory
    {
        public static void RegisterMechanicService(ContainerBuilder builder, string contextName)
        {
            builder
                .Register(c => new MechanicService(contextName, c.Resolve<IContextContainer>(), c.Resolve<IUserService>()))
                .As<IMechanicService>()
                .InstancePerLifetimeScope();
        }
    }
}
