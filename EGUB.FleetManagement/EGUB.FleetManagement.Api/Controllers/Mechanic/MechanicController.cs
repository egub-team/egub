﻿using EGUB.Core.Api;
using EGUB.FleetManagement.Contracts.BLL.Mechanic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace EGUB.FleetManagement.Api.Controllers.Mechanic
{
    [Route("api/[controller]")]
    [Authorize(Roles = Contracts.Constants.CustomRoles.Mechanic)]
    [ApiController]
    public class MechanicController : ControllerBase
    {
        private readonly IMechanicService _mechanicService;

        public MechanicController(IMechanicService mechanicService)
        {
            _mechanicService = mechanicService;
        }


        [HttpGet(nameof(GetPartsOrders))]
        [ProducesResponseType(typeof(ICollection<PartsOrderInfo>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetPartsOrders()
        {
            try
            {
                var context = new OperationContext(HttpContext);
                var items = await _mechanicService.GetPartsOrders(context);

                return Ok(items?
                    .Select(i => new PartsOrderInfo
                    {
                        Id = i.Id,
                        Status = i.Status,
                        Number = i.Number,
                        Date = i.Date,
                        InCharge = i.InCharge,
                        Driver = i.Driver,
                        Warehouse = i.Warehouse,
                        Truck = i.Truck,
                        Notes = i.Notes,
                    }).ToList());
            }
            catch (Exception ex)
            {
                return UnprocessableEntity(ex.Message);
            }
        }
    }
}
