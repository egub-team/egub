﻿using Autofac;
using EGUB.FleetManagement.Api.Controllers.Mechanic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.FleetManagement.Api
{
    public class ContainerFactory : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .RegisterType<MechanicController>()
                .AsSelf()
                .InstancePerLifetimeScope();
        }
    }
}
