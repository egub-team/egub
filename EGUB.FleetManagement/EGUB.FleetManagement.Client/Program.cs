using EGUB.FleetManagement.Client;
using EGUB.Security.Web;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;

var builder = WebAssemblyHostBuilder.CreateDefault(args);
builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");

builder.Services.AddScoped(sp => builder.HostEnvironment);
builder.Services
    .AddScoped(sp => 
        new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });
builder.Services.AddSecurity(EGUB.FleetManagement.Web.Constants.Security.CustomUserRoles.Items);

await builder.Build().RunAsync();
