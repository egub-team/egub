﻿using EGUB.Core.BLL;
using EGUB.Core.Contracts;
using EGUB.Core.Contracts.BLL;
using EGUB.Core.Contracts.DAL;
using EGUB.TravelPortal.Contracts;
using EGUB.TravelPortal.Contracts.BLL.Excursions;
using EGUB.TravelPortal.Contracts.DAL;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EGUB.TravelPortal.BLL
{
    internal class ExcursionService : BaseEntityService<ActiveStatusEnum, Excursion>, IExcursionService
    {
        private readonly IConfiguration _configuration;
        public ExcursionService(
            string contextName,
            IContextContainer contextContainer,
            IConfiguration configuration) : base(contextContainer)
        {
            ContextName = contextName;
            _configuration = configuration;
        }

        protected override string ContextName { get; }

        public async Task<Guid> CreateExcursion(ICreateExcursionContext context)
        {
            var excursion = new Excursion
            {
                StatusInfo = new StatusInfo<ActiveStatusEnum>
                {
                    Status = ActiveStatusEnum.Active,
                },
                Name = context.Name,
                Sights = null
            };

            var saveEntityContext = new SaveEntityContext<Excursion>(context, excursion);
            await Save(saveEntityContext);

            return excursion.Id;
        }

        public Task UpdateExcursion(IUpdateExcursionContext context)
        {
            throw new NotImplementedException();
        }

        public Task<Excursion> GetExcursion(IGetExcursionContext context)
        {
            throw new NotImplementedException();
        }

        public Task<ICollection<Excursion>> GetExcursions(IGetExcursionsContext context)
        {
            throw new NotImplementedException();
        }

        #region overrides
        protected override async Task SaveEntity(IRepositoryContainer container, ISaveEntityContext<Excursion> context)
        {
            await base.SaveEntity(container, context);
            var excursionRepository = container.GetRepository<IExcursionRepository>();
            var travelServiceRepository = container.GetRepository<ITravelServiceRepository>();
            var entity = context.Entity;

            await travelServiceRepository.SaveTravelService(entity.Id, entity.Name, entity.Type.ToString());
            await excursionRepository.SaveExcursion(entity.Id);
        }

        protected override Task ValidateEntity(IRepositoryContainer container, ISaveEntityContext<Excursion> context)
        {
            throw new NotImplementedException();
            //await base.ValidateEntity(container, context);
            //var entity = context.Entity;
            //if (entity.FirstName.IsNullOrEmpty())
            //{
            //    throw new ArgumentNullException(nameof(entity.FirstName));
            //}

            //if (entity.SecondName.IsNullOrEmpty())
            //{
            //    throw new ArgumentNullException(nameof(entity.SecondName));
            //}

            //var repository = container.GetRepository<IUserRepository>();
            //var personId = await repository.GetUserId(entity.Code);
            //if (userId != Guid.Empty && userId != entity.Id)
            //{
            //    throw new ArgumentOutOfRangeException(nameof(entity.Code), "There is a user with the same Code");
            //}

            //userId = await repository.GetUserId(entity.Email);
            //if (userId != Guid.Empty && userId != entity.Id)
            //{
            //    throw new ArgumentOutOfRangeException(nameof(entity.Email), "There is a user with the same Email");
            //}
        }
        #endregion
    }
}
