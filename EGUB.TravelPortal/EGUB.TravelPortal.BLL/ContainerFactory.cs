﻿using Autofac;
using EGUB.Core.Contracts.DAL;
using EGUB.Security.Contracts.BLL;
using EGUB.Security.Contracts.DAL;
using EGUB.TravelPortal.Contracts.BLL.Excursions;
using EGUB.TravelPortal.Contracts.BLL.Persons;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.TravelPortal.BLL
{
    public class ContainerFactory
    {
        public static void RegisterPersonService(ContainerBuilder builder, string contextName)
        {
            builder
                .Register(c => new PersonService(contextName, c.Resolve<IContextContainer>(), c.Resolve<IUserService>()))
                .As<IPersonService>()
                .InstancePerLifetimeScope();
        }

        public static void RegisterExcursionService(ContainerBuilder builder, string contextName)
        {
            builder
                .Register(c => new ExcursionService(contextName, c.Resolve<IContextContainer>(), c.Resolve<IConfiguration>()))
                .As<IExcursionService>()
                .InstancePerLifetimeScope();
        }
    }
}
