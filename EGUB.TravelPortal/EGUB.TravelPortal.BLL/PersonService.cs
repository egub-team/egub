﻿using EGUB.Core.BLL;
using EGUB.Core.Contracts;
using EGUB.Core.Contracts.BLL;
using EGUB.Core.Contracts.DAL;
using EGUB.Security.Contracts.BLL;
using EGUB.Security.Contracts.DAL;
using EGUB.TravelPortal.Contracts;
using EGUB.TravelPortal.Contracts.BLL.Persons;
using EGUB.TravelPortal.Contracts.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace EGUB.TravelPortal.BLL
{
    internal class PersonService : BaseEntityService<ActiveStatusEnum, Person>, IPersonService
    {
        private readonly IUserService _userService;

        public PersonService(
            string contextName,
            IContextContainer contextContainer,
            IUserService userService) : base(contextContainer)
        {
            ContextName = contextName;
            _userService = userService;
        }

        protected override string ContextName { get; }

        public async Task<Guid> CreatePerson(ICreatePersonContext context)
        {
            var person = new Person
            {
                StatusInfo = new StatusInfo<ActiveStatusEnum>
                {
                    Status = ActiveStatusEnum.Active,
                },
                FirstName = context.FirstName,
                SecondName = context.SecondName,
                ParentName = context.ParentName,
                BirthDay = context.BirthDay,
                Gender = context.Gender,
                PhoneNumber = context.PhoneNumber,
                Email = context.Email,
                Address =
                    context.CountryCode.HasValue ||
                    !context.City.IsNullOrEmpty() ||
                    !context.Street.IsNullOrEmpty() ||
                    !context.Number.IsNullOrEmpty() ? new Address
                    {
                        CountryCode = context.CountryCode.GetValueOrDefault(),
                        City = context.City,
                        Street = context.Street,
                        Number = context.Number
                    }
                    : null
            };

            var saveEntityContext = new SaveEntityContext<Person>(context, person);
            await Save(saveEntityContext);

            return person.Id;
        }

        public async Task UpdatePerson(IUpdatePersonContext context)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));
            if (context.Id == Guid.Empty)
                throw new ArgumentNullException(nameof(context.Id));

            await Execute(async container =>
            {
                var getEntitiesContext = new GetEntitiesContext(context, new[] { context.Id });
                var persones = await GetEntities(container, getEntitiesContext);
                var person = persones.SingleOrDefault();
                if (person == null)
                    throw new ArgumentNullException(nameof(context.Id), "Person is not found!");

                person.FirstName = context.FirstName;
                person.SecondName = context.SecondName;
                person.ParentName = context.ParentName;
                person.BirthDay = context.BirthDay;
                person.Gender = context.Gender;
                person.PhoneNumber = context.PhoneNumber;
                person.Email = context.Email;
                person.Address =
                    context.CountryCode.HasValue ||
                    !context.City.IsNullOrEmpty() ||
                    !context.Street.IsNullOrEmpty() ||
                    !context.Number.IsNullOrEmpty() ? new Address
                    {
                        CountryCode = context.CountryCode.GetValueOrDefault(),
                        City = context.City,
                        Street = context.Street,
                        Number = context.Number
                    }
                    : null;

                var saveEntityContext = new SaveEntityContext<Person>(context, person);
                await Save(container, saveEntityContext);
            });
        }

        public async Task<Person> GetPerson(IGetPersonContext context)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));
            var result = default(Person);
            await Execute(async container =>
            {
                var repository = container.GetRepository<IPersonRepository>();
                var getEntitiesContext = new GetEntitiesContext(context, new[] { context.Id });
                var entities = await GetEntities(container, getEntitiesContext);
                result = entities.SingleOrDefault();
            });

            return result;
        }

        public Task<ICollection<Person>> GetPersones(IGetPersonesContext context)
        {
            throw new NotImplementedException();
        }

        #region overrides
        protected override async Task SaveEntity(IRepositoryContainer container, ISaveEntityContext<Person> context)
        {
            await base.SaveEntity(container, context);
            var personRepository = container.GetRepository<IPersonRepository>();
            var addressRepository = container.GetRepository<IAddressRepository>();
            var entity = context.Entity;
            var genderValue = context.Entity.Gender.HasValue
                ? context.Entity.Gender == GenderEnum.Male
                : default(bool?);

            await personRepository.SavePerson(entity.Id, entity.User?.Id, entity.FirstName, entity.SecondName, entity.ParentName,
                entity.BirthDay, genderValue, entity.Email, entity.PhoneNumber);
            await addressRepository.SaveAddress(entity.Id, entity.Address?.CountryCode.ToString(), entity.Address?.City,
                entity.Address?.Street, entity.Address?.Number);
        }

        protected override async Task ValidateEntity(IRepositoryContainer container, ISaveEntityContext<Person> context)
        {
            await base.ValidateEntity(container, context);
            var entity = context.Entity;
            if (entity.FirstName.IsNullOrEmpty())
            {
                throw new ArgumentNullException(nameof(entity.FirstName));
            }

            if (entity.SecondName.IsNullOrEmpty())
            {
                throw new ArgumentNullException(nameof(entity.SecondName));
            }

            if (entity.Address != null)
            {
                if (!entity.Address.Value.Street.IsNullOrEmpty() && entity.Address.Value.City.IsNullOrEmpty())
                    throw new ArgumentNullException(nameof(entity.Address.Value.City));

                if (!entity.Address.Value.Number.IsNullOrEmpty() && entity.Address.Value.Street.IsNullOrEmpty())
                    throw new ArgumentNullException(nameof(entity.Address.Value.Street));
            }

            if (!string.IsNullOrEmpty(entity.Email))
                if (!MailAddress.TryCreate(entity.Email, out _))
                    throw new ArgumentException("Invalid email", nameof(entity.Email));

            if (entity.BirthDay.HasValue)
            {
                int currentYear = DateTime.Today.Year;
                int birthYear = entity.BirthDay.Value.Year;
                int personYears = currentYear - birthYear;
                // -1 < x < 120, x - N
                var isBirthDayValid = -1 < personYears && personYears < 120;
                if (!isBirthDayValid)
                    throw new ArgumentOutOfRangeException(nameof(entity.BirthDay));
            }

            if (entity.User != null)
            {
                var ids = new[] { entity.User.Id };
                var users = await _userService.GetEntities(new GetEntitiesContext(context, ids));
                if (!users.Any())
                {
                    throw new ArgumentOutOfRangeException(nameof(entity.User));
                }
            }


        }

        protected override async Task InitEntities(IRepositoryContainer container, IInitEntitiesContext<Person> context)
        {
            await base.InitEntities(container, context);
            var ids = context.Entities
                .Select(i => i.Id)
                .ToList();
            if (!ids.Any())
            {
                return;
            }

            var personRepository = container.GetRepository<IPersonRepository>();
            var addressRepository = container.GetRepository<IAddressRepository>();
            var items = (await personRepository.GetItems(ids)).ToDictionary(i => i.Id);

            var userIds = items.Values
                .Where(i => i.UserId.HasValue)
                .Select(i => i.UserId.Value)
                .Distinct()
                .ToArray();

            var users = await _userService.GetEntities(new GetEntitiesContext(context, userIds));
            var userDictionary = users.ToDictionary(i => i.Id);

            var addresses = (await addressRepository.GetItems(ids)).ToDictionary(i => i.EntityId);

            foreach (var person in context.Entities)
            {
                if (items.TryGetValue(person.Id, out var item))
                {
                    person.Id = item.Id;
                    person.FirstName = item.FirstName;
                    person.SecondName = item.SecondName;
                    person.ParentName = item.ParentName;
                    person.BirthDay = item.BirthDay;
                    person.Gender = item.Gender.HasValue ? (item.Gender.Value ? GenderEnum.Male : GenderEnum.Female) : null;
                    person.Email = item.Email;
                    person.PhoneNumber = item.PhoneNumber;
                    person.User = item.UserId.HasValue
                        ? userDictionary[item.UserId.Value]
                        : null;
                    person.Address = addresses.TryGetValue(item.Id, out var value)
                        ? new Address()
                        {
                            CountryCode = Enum.Parse<CountryCodeEnum>(value.CountryCode),
                            City = value.City,
                            Street = value.Street,
                            Number = value.Number
                        }
                        : default;
                }
            }
        }
        #endregion
    }
}
