﻿using EGUB.Core.BLL;
using EGUB.Core.Contracts;
using EGUB.Core.Contracts.BLL;
using EGUB.Core.Contracts.DAL;
using EGUB.TravelPortal.Contracts;
using EGUB.TravelPortal.Contracts.BLL.TravelPoints;
using EGUB.TravelPortal.Contracts.DAL;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.TravelPortal.BLL
{
    internal class TravelPointService : BaseEntityService<ActiveStatusEnum, TravelPoint>, ITravelPointService
    {
        private readonly IConfiguration _configuration;
        public TravelPointService(
            string contextName,
            IContextContainer contextContainer,
            IConfiguration configuration) : base(contextContainer)
        {
            ContextName = contextName;
            _configuration = configuration;
        }

        protected override string ContextName { get; }

        public async Task<Guid> CreateTravelPoint(ICreateTravelPointContext context)
        {
            var travelPoint = new TravelPoint
            {
                StatusInfo = new StatusInfo<ActiveStatusEnum>
                {
                    Status = ActiveStatusEnum.Active,
                },
                Name = context.Name,
                Coordinate = context.Coordinate,
                Description = context.Description
            };

            var saveEntityContext = new SaveEntityContext<TravelPoint>(context, travelPoint);
            await Save(saveEntityContext);

            return travelPoint.Id;
        }

        public Task<TravelPoint> GetTravelPoint(IGetTravelPointContext context)
        {
            throw new NotImplementedException();
        }

        public Task<ICollection<TravelPoint>> GetTravelPoints(IGetTravelPointContext context)
        {
            throw new NotImplementedException();
        }

        public Task UpdateTravelPoint(IUpdateTravelPointContext context)
        {
            throw new NotImplementedException();
        }

        #region overrides
        protected override async Task SaveEntity(IRepositoryContainer container, ISaveEntityContext<TravelPoint> context)
        {
            await base.SaveEntity(container, context);
            var travelPointRepository = container.GetRepository<ITravelPointRepository>();
            var entity = context.Entity;

            await travelPointRepository.SaveTravelPoint(entity.Id, entity.Name, entity.Coordinate.Latitude,
                entity.Coordinate.Longitude, entity.Description);
        }

        protected override Task ValidateEntity(IRepositoryContainer container, ISaveEntityContext<TravelPoint> context)
        {
            throw new NotImplementedException();
            //await base.ValidateEntity(container, context);
            //var entity = context.Entity;
            //if (entity.FirstName.IsNullOrEmpty())
            //{
            //    throw new ArgumentNullException(nameof(entity.FirstName));
            //}

            //if (entity.SecondName.IsNullOrEmpty())
            //{
            //    throw new ArgumentNullException(nameof(entity.SecondName));
            //}

            //var repository = container.GetRepository<IUserRepository>();
            //var personId = await repository.GetUserId(entity.Code);
            //if (userId != Guid.Empty && userId != entity.Id)
            //{
            //    throw new ArgumentOutOfRangeException(nameof(entity.Code), "There is a user with the same Code");
            //}

            //userId = await repository.GetUserId(entity.Email);
            //if (userId != Guid.Empty && userId != entity.Id)
            //{
            //    throw new ArgumentOutOfRangeException(nameof(entity.Email), "There is a user with the same Email");
            //}
        }
        #endregion
    }
}
