using Autofac;
using EGUB.Core.BLL;
using EGUB.Core.Common;
using EGUB.TravelPortal.Contracts;
using EGUB.TravelPortal.Contracts.BLL.Persons;
using EGUB.Core.DAL;
using EGUB.Core.Contracts.DAL;
using Microsoft.Extensions.Configuration;
using EGUB.TravelPortal.DAL;
using EGUB.Security.Contracts;
using EGUB.TravelPortal.Contracts.BLL.Excursions;
using Microsoft.VisualBasic;
using EGUB.Security.DAL;
using System;
using EGUB.Core.Contracts;
using EGUB.Core.Contracts.BLL;

namespace EGUB.TravelPortal.Tests
{
    [TestClass]
    public class PersonServiceTests
    {
        const string TravelPortalContextNameForCreateTest = $"{nameof(TravelPortalMigrator)}_{nameof(PersonServiceTests)}_{nameof(CreateTest)}";
        const string SecurityContextNameForCreateTest = $"{nameof(SecurityMigrator)}_{nameof(PersonServiceTests)}_{nameof(CreateTest)}";

        [TestMethod]
        public Task CreateTest() =>
           Core.Common.ContainerFactory.Execute(
                builder =>
                {
                    builder
                        .Register(c => new Context(TravelPortalContextNameForCreateTest, c.Resolve<IConfiguration>()))
                        .Named<IContext>(TravelPortalContextNameForCreateTest)
                        .InstancePerLifetimeScope();

                    builder
                        .Register(c => new TravelPortalMigrator(TravelPortalContextNameForCreateTest, true, c.Resolve<Core.Contracts.IContainer>()))
                        .Named<IMigrator>(TravelPortalContextNameForCreateTest)
                        .InstancePerLifetimeScope();

                    builder
                        .Register(c => new Context(SecurityContextNameForCreateTest, c.Resolve<IConfiguration>()))
                        .Named<IContext>(SecurityContextNameForCreateTest)
                        .InstancePerLifetimeScope();

                    builder
                        .Register(c => new SecurityMigrator(SecurityContextNameForCreateTest, true, c.Resolve<Core.Contracts.IContainer>()))
                        .Named<IMigrator>(SecurityContextNameForCreateTest)
                        .InstancePerLifetimeScope();

                    Core.DAL.ContainerFactory.RegisterContextContainer(builder);
                    Core.DAL.ContainerFactory.RegisterRepositoryContainer(builder);
                    Core.DAL.ContainerFactory.RegisterEntityRepository(builder);
                    DAL.ContainerFactory.RegisterPersonRepository(builder);
                    DAL.ContainerFactory.RegisterAddressRepository(builder);
                    DAL.ContainerFactory.RegisterTravelServiceRepository(builder);
                    BLL.ContainerFactory.RegisterPersonService(builder, TravelPortalContextNameForCreateTest);
                    Security.BLL.ContainerFactory.RegisterUserService(builder, SecurityContextNameForCreateTest);
                    Security.DAL.ContainerFactory.RegisterUserRepository(builder);
                },
                async container =>
                {
                    var travelMigrator = container.GetItem<IMigrator>(TravelPortalContextNameForCreateTest);
                    await travelMigrator.Execute();
                    var securityMigrator = container.GetItem<IMigrator>(SecurityContextNameForCreateTest);
                    await securityMigrator.Execute();
                    var personService = container.GetItem<IPersonService>();
                    var createInfo = new CreatePersonContextTest()
                    {
                        FirstName = "Valeriia",
                        SecondName = "Dobrydina",
                        ParentName = "Evhenivna",
                        BirthDay = new DateOnly(2000, 02, 02),
                        Gender = GenderEnum.Female,
                        PhoneNumber = "0955156216",
                        Email = "yotoshiria@gmail.com",
                        CountryCode = CountryCodeEnum.UA,
                        City = "Irpin",
                        Street = "Kievska",
                        Number = "50A"
                    };

                    var personId = await personService.CreatePerson(createInfo);

                    var getInfo = new GetPersonContextTest()
                    {
                        Id = personId
                    };

                    var person = await personService.GetPerson(getInfo);

                    var exceptionAddressCreateInfo = new CreatePersonContextTest()
                    {
                        FirstName = "Valeriia",
                        SecondName = "Dobrydina",
                        ParentName = "Evhenivna",
                        BirthDay = new DateOnly(2000, 02, 02),
                        Gender = GenderEnum.Female,
                        PhoneNumber = "0955156216",
                        Email = "yotoshiriagmail.com",
                        CountryCode = CountryCodeEnum.UA,
                        City = "Irpin",
                        Street = "Kievska",
                        Number = "50A"
                    };

                    var exceptionBirthDayCreateInfo = new CreatePersonContextTest()
                    {
                        FirstName = "Valeriia",
                        SecondName = "Dobrydina",
                        ParentName = "Evhenivna",
                        BirthDay = new DateOnly(1890, 02, 02),
                        Gender = GenderEnum.Female,
                        PhoneNumber = "0955156216",
                        Email = "yotoshiria@gmail.com",
                        CountryCode = CountryCodeEnum.UA,
                        City = "Irpin",
                        Street = "Kievska",
                        Number = "50A"
                    };

                    await Assert.ThrowsExceptionAsync<ArgumentException>(async () =>
                    {
                        await personService.CreatePerson(exceptionAddressCreateInfo);
                    });

                    await Assert.ThrowsExceptionAsync<ArgumentOutOfRangeException>(async () =>
                    {
                        await personService.CreatePerson(exceptionBirthDayCreateInfo);
                    });

                    Assert.AreNotEqual(Guid.Empty, personId);
                    Assert.AreEqual(createInfo.FirstName, person.FirstName);
                    Assert.AreEqual(createInfo.City, person.Address.Value.City);
                });


        [TestMethod]
        public Task ValidateEntityTest()
            =>
           Core.Common.ContainerFactory.Execute(
                builder =>
                {
                    builder
                        .Register(c => new Context(TravelPortalContextNameForCreateTest, c.Resolve<IConfiguration>()))
                        .Named<IContext>(TravelPortalContextNameForCreateTest)
                        .InstancePerLifetimeScope();

                    builder
                        .Register(c => new Context(SecurityContextNameForCreateTest, c.Resolve<IConfiguration>()))
                        .Named<IContext>(SecurityContextNameForCreateTest)
                        .InstancePerLifetimeScope();

                    builder
                        .Register(c => new SecurityMigrator(SecurityContextNameForCreateTest, true, c.Resolve<Core.Contracts.IContainer>()))
                        .Named<IMigrator>(SecurityContextNameForCreateTest)
                        .InstancePerLifetimeScope();

                    Core.DAL.ContainerFactory.RegisterContextContainer(builder);
                    Core.DAL.ContainerFactory.RegisterRepositoryContainer(builder);
                    Core.DAL.ContainerFactory.RegisterEntityRepository(builder);
                    BLL.ContainerFactory.RegisterPersonService(builder, TravelPortalContextNameForCreateTest);
                    Security.BLL.ContainerFactory.RegisterUserService(builder, SecurityContextNameForCreateTest);
                    Security.DAL.ContainerFactory.RegisterUserRepository(builder);
                },
                async container =>
                {
                    var securityMigrator = container.GetItem<IMigrator>(SecurityContextNameForCreateTest);
                    await securityMigrator.Execute();
                    var personService = container.GetItem<IPersonService>();
                    var person = new Person()
                    {
                        StatusInfo = new StatusInfo<ActiveStatusEnum>
                        {
                            Status = ActiveStatusEnum.Active,
                        },
                        User = new User
                        {
                            Id = Guid.NewGuid()
                        },
                        FirstName = "First name",
                        SecondName = "Second name"
                    };

                    var saveEntityContext = new SaveEntityContext<Person>(new OperationContextTest(), person);
                    await Assert.ThrowsExceptionAsync<ArgumentOutOfRangeException>(async () =>
                    {
                        await personService.Save(saveEntityContext);
                    });
                });

    }

    internal class ValidatePersonContextTest : ICreatePersonContext
    {
        public User User { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string ParentName { get; set; }
        public DateOnly? BirthDay { get; set; }
        public GenderEnum? Gender { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public CountryCodeEnum? CountryCode { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string Number { get; set; }
        public Guid GetCurrentUserId() => Guid.Empty;
    }

    internal class CreatePersonContextTest : ICreatePersonContext
    {
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string ParentName { get; set; }
        public DateOnly? BirthDay { get; set; }
        public GenderEnum? Gender { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public CountryCodeEnum? CountryCode { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string Number { get; set; }
        public Guid GetCurrentUserId() => Guid.Empty;
    }

    internal class GetPersonContextTest : IGetPersonContext
    {
        public Guid Id { get; set; }
        public Guid GetCurrentUserId() => Guid.Empty;
    }

    internal class OperationContextTest : IOperationContext
    {
        public Guid GetCurrentUserId()
        {
            throw new NotImplementedException();
        }
    }
}