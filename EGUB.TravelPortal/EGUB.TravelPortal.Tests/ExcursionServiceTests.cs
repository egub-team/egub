﻿using Autofac;
using EGUB.Core.BLL;
using EGUB.Core.Common;
using EGUB.TravelPortal.Contracts;
using EGUB.TravelPortal.Contracts.BLL.Persons;
using EGUB.Core.DAL;
using EGUB.Core.Contracts.DAL;
using Microsoft.Extensions.Configuration;
using EGUB.TravelPortal.DAL;
using EGUB.Security.Contracts;
using EGUB.TravelPortal.Contracts.BLL.Excursions;
using Microsoft.VisualBasic;

namespace EGUB.TravelPortal.Tests
{
    [TestClass]
    public class ExcursionServiceTests
    {
        const string ContextNameForCreateTest = $"{nameof(ExcursionServiceTests)}_{nameof(CreateTest)}";

        [TestMethod]
        public Task CreateTest() =>
           Core.Common.ContainerFactory.Execute(
                builder =>
                {
                    builder
                        .Register(c => new Context(ContextNameForCreateTest, c.Resolve<IConfiguration>()))
                        .Named<IContext>(ContextNameForCreateTest)
                        .InstancePerLifetimeScope();

                    builder
                        .Register(c => new TravelPortalMigrator(ContextNameForCreateTest, true, c.Resolve<Core.Contracts.IContainer>()))
                        .Named<IMigrator>(ContextNameForCreateTest)
                        .InstancePerLifetimeScope();

                    Core.DAL.ContainerFactory.RegisterContextContainer(builder);
                    Core.DAL.ContainerFactory.RegisterRepositoryContainer(builder);
                    Core.DAL.ContainerFactory.RegisterEntityRepository(builder);
                    DAL.ContainerFactory.RegisterPersonRepository(builder);
                    DAL.ContainerFactory.RegisterAddressRepository(builder);
                    DAL.ContainerFactory.RegisterTravelServiceRepository(builder);
                    DAL.ContainerFactory.RegisterExcursionRepository(builder);
                    BLL.ContainerFactory.RegisterExcursionService(builder, ContextNameForCreateTest);
                },
                async container =>
                {
                    var migrator = container.GetItem<IMigrator>(ContextNameForCreateTest);
                    await migrator.Execute();
                    var excursionService = container.GetItem<IExcursionService>();
                    var createInfo = new CreateExcursionContextTest()
                    {
                        Name = "Test Excursion",
                    };

                    var excursionId = await excursionService.CreateExcursion(createInfo);
                    Assert.AreNotEqual(Guid.Empty, excursionId);
                });
    }

    internal class CreateExcursionContextTest : ICreateExcursionContext
    {
        public string Name { get; set; }
        public ICollection<Guid> SightIds { get; set; }
        public Guid GetCurrentUserId() => Guid.Empty;
    }
}

