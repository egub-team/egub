﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.TravelPortal.Contracts
{
    public class Excursion : TravelService
    {
        public override string EntityType => EntityTypes.Excursion.ToString();
        public override TravelServiceTypeEnum Type => TravelServiceTypeEnum.Excursion;
        public ICollection<TravelPoint> Sights { get; set; }
    }
}
