﻿using EGUB.Core.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.TravelPortal.Contracts
{
    public class Client : BaseEntity<ActiveStatusEnum>
    {
        public override string EntityType => EntityTypes.Client.ToString();
        public Person Person { get; set; }
    }
}
