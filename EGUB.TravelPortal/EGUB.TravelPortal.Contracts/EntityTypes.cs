﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.TravelPortal.Contracts
{
    public enum EntityTypes
    {
        HotelAccommodation,
        Excursion,
        Person,
        Client,
        Guide,
        PassportData,
        HotelPoint,
        TravelPoint,
        HotelAccommodationRoom
    }
}
