﻿using EGUB.Core.Contracts;
using EGUB.Security.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.TravelPortal.Contracts
{
    public class Person : BaseEntity<ActiveStatusEnum>
    {
        public override string EntityType => EntityTypes.Person.ToString();
        public User User { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string ParentName { get; set; }
        public DateOnly? BirthDay { get; set; }
        public GenderEnum? Gender { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public Address? Address { get; set; }
    }
}
