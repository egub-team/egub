﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.TravelPortal.Contracts
{
    public class HotelPoint : TravelPoint
    {
        public override string EntityType => EntityTypes.HotelPoint.ToString();
        public StarEnum Star { get; set; }
    }
}
