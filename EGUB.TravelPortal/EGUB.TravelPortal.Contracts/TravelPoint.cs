﻿using EGUB.Core.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.TravelPortal.Contracts
{
    public class TravelPoint : BaseEntity<ActiveStatusEnum>
    {
        public override string EntityType => EntityTypes.TravelPoint.ToString();
        public string Name { get; set; }
        public GeoCoordinate Coordinate { get; set; }
        public Address Address { get; set; }
        public string Description { get; set; }
    }
}
