﻿using EGUB.Core.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.TravelPortal.Contracts
{
    public class PassportData : BaseEntity<ActiveStatusEnum>
    {
        public override string EntityType => EntityTypes.PassportData.ToString();
        public Person Person { get; set; }
        public PassportDataTypeEnum Type { get; set; }
        public string Code { get; set; }
        public DateOnly DateOfIssue { get; set; }
        public DateOnly? DateOfExpiry { get; set; }
        public string Authority { get; set; }
        public CountryCodeEnum CountryCode { get; set; }
    }
}
