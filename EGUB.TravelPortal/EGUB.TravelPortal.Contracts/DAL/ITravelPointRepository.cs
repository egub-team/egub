﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.TravelPortal.Contracts.DAL
{
    public interface ITravelPointRepository : Core.Contracts.DAL.IRepository
    {
        Task SaveTravelPoint(
            Guid id,
            string name,
            decimal latitude,
            decimal longitude,
            string description);

        Task<ICollection<Guid>> GetTravelPointIds((string name, decimal latitude, decimal longitude, string description) filter);
    }
}
