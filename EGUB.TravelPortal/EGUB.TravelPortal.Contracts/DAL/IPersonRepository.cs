﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.TravelPortal.Contracts.DAL
{
    public interface IPersonRepository : Core.Contracts.DAL.IRepository
    {
        Task SavePerson(
            Guid id,
            Guid? userId,
            string firstName,
            string secondName,
            string parentName,
            DateOnly? birthDay,
            bool? gender,
            string email,
            string phonenumber);

        Task<ICollection<(Guid Id, Guid? UserId, string FirstName, string SecondName, string ParentName, DateOnly? BirthDay,
            bool? Gender, string Email, string PhoneNumber)>> GetItems(ICollection<Guid> ids);
    }
}
