﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.TravelPortal.Contracts.DAL
{
    public interface IExcursionRepository : Core.Contracts.DAL.IRepository
    {
        Task SaveExcursion(Guid id);
    }
}
