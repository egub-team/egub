﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.TravelPortal.Contracts.DAL
{
    public interface IAddressRepository : Core.Contracts.DAL.IRepository
    {
        Task SaveAddress(
            Guid entityId,
            string countryCode, 
            string city, 
            string street, 
            string number);

        Task<ICollection<(Guid EntityId, string CountryCode, string City, string Street, string Number)>>
            GetItems(ICollection<Guid> ids);
    }
}
