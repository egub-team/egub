﻿using EGUB.Core.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.TravelPortal.Contracts
{
    public class HotelAccommodationRoom : BaseEntity<ActiveStatusEnum>
    {
        public override string EntityType => EntityTypes.HotelAccommodationRoom.ToString();
        public byte SingleBeds { get; set; }
        public byte DoubleBeds { get; set; }
        public byte BunkBeds { get; set; }
        public string RoomType { get; set; }
        public byte Rooms { get; set; }
        public string Description { get; set; }
        public ICollection<HotelAccommodationRoomAdditionalEnum> Additionals { get; set; }
    }
}
