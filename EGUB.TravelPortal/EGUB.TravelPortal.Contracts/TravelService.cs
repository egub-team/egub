﻿using EGUB.Core.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.TravelPortal.Contracts
{
    public abstract class TravelService : BaseEntity<ActiveStatusEnum>
    {
        public string Name { get; set; }
        public abstract TravelServiceTypeEnum Type { get; }
    }
}
