﻿using EGUB.Core.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.TravelPortal.Contracts
{
    public class Guide : BaseEntity<ActiveStatusEnum>
    {
        public override string EntityType => EntityTypes.Guide.ToString();
        public Person Person { get; set; }
    }
}
