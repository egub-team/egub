﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.TravelPortal.Contracts
{
    public class HotelAccommodation : TravelService
    {
        public override string EntityType => EntityTypes.HotelAccommodation.ToString();
        public override TravelServiceTypeEnum Type => TravelServiceTypeEnum.HotelBooking;
        public HotelPoint Hotel { get; set; }
        public HotelAccommodationRoom Room { get; set; }
    }
}
