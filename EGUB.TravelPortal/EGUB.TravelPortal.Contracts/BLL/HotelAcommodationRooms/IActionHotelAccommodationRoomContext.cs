﻿using EGUB.Core.Contracts.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.TravelPortal.Contracts.BLL.HotelAcommodationRooms
{
    public interface IActionHotelAccommodationRoomContext : IOperationContext
    {
        byte SingleBeds { get; }
        byte DoubleBeds { get; }
        byte BunkBeds { get; }
        string RoomType { get; }
        byte Rooms { get; }
        string Description { get; }
        ICollection<HotelAccommodationRoomAdditionalEnum> Additionals { get; }
    }
}
