﻿using EGUB.Core.Contracts.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.TravelPortal.Contracts.BLL.HotelAcommodationRooms
{
    public interface IHotelAccommodationRoomService : IEntityService<ActiveStatusEnum, HotelAccommodationRoom>
    {
        Task<Guid> CreateHotelAccommodationRoom(ICreateHotelAccommodationRoomContext context);
        Task UpdateHotelAccommodationRoom(IUpdateHotelAccommodationRoomContext context);
        Task<HotelAccommodationRoom> GetHotelAccommodationRoom(IGetHotelAccommodationRoomContext context);
        Task<ICollection<HotelAccommodationRoom>> GetHotelAccommodationRooms(IGetHotelAccommodationRoomsContext context);
    }
}
