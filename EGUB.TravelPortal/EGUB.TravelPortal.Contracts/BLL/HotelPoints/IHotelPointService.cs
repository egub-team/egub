﻿using EGUB.Core.Contracts.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.TravelPortal.Contracts.BLL.HotelPoints
{
    public interface IHotelPointService : IEntityService<ActiveStatusEnum, HotelPoint>
    {
        Task<Guid> CreateHotelPoint(ICreateHotelPointContext context);
        Task UpdateHotelPoint(IUpdateHotelPointContext context);
        Task<HotelPoint> GetHotelPoint(IGetHotelPointContext context);
        Task<ICollection<HotelPoint>> GetHotelPoints(IGetHotelPointsContext context);
    }
}
