﻿using EGUB.Core.Contracts.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.TravelPortal.Contracts.BLL.Passports
{
    public interface IPassportDataService : IEntityService<ActiveStatusEnum, PassportData>
    {
        Task<Guid> CreatePassportData(ICreatePassportDataContext context);
        Task UpdatePassportData(IUpdatePassportDataContext context);
        Task<PassportData> GetPassportData(IGetPassportDataContext context);
        Task<ICollection<PassportData>> GetPassportDatas(IGetPassportDatasContext context);
    }
}
