﻿using EGUB.Core.Contracts.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.TravelPortal.Contracts.BLL.Passports
{
    public interface IActionPassportDataContext : IOperationContext
    {
        Guid PersonId { get; }
        PassportDataTypeEnum Type { get; }
        string Code { get; }
        DateOnly DateOfIssue { get; }
        DateOnly? DateOfExpiry { get; }
        string Authority { get; }
        CountryCodeEnum CountryCode { get; }
    }
}
