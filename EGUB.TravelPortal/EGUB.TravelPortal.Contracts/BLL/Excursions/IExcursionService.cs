﻿using EGUB.Core.Contracts.BLL;
using EGUB.TravelPortal.Contracts.BLL.Persons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.TravelPortal.Contracts.BLL.Excursions
{
    public interface IExcursionService : IEntityService<ActiveStatusEnum, Excursion>
    {
        Task<Guid> CreateExcursion(ICreateExcursionContext context);
        Task UpdateExcursion(IUpdateExcursionContext context);
        Task<Excursion> GetExcursion(IGetExcursionContext context);
        Task<ICollection<Excursion>> GetExcursions(IGetExcursionsContext context);
    }
}
