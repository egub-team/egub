﻿using EGUB.Core.Contracts.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.TravelPortal.Contracts.BLL.Persons
{
    public interface IPersonService : IEntityService<ActiveStatusEnum, Person>
    {
        Task<Guid> CreatePerson(ICreatePersonContext context);
        Task UpdatePerson(IUpdatePersonContext context);
        Task<Person> GetPerson(IGetPersonContext context);
        Task<ICollection<Person>> GetPersones(IGetPersonesContext context);
    }
}
