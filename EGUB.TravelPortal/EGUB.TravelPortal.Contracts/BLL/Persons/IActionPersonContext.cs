﻿using EGUB.Core.Contracts.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.TravelPortal.Contracts.BLL.Persons
{
    public interface IActionPersonContext : IOperationContext
    {
        string FirstName { get; }
        string SecondName { get; }
        string ParentName { get; }
        DateOnly? BirthDay { get; }
        GenderEnum? Gender { get; }
        string PhoneNumber { get; }
        string Email { get; }
        CountryCodeEnum? CountryCode { get; }
        string City { get; }
        string Street { get; }
        string Number { get; }
    }
}
