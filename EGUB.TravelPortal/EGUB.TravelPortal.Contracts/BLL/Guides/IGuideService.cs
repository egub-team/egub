﻿using EGUB.Core.Contracts.BLL;
using EGUB.TravelPortal.Contracts.BLL.Persons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.TravelPortal.Contracts.BLL.Guides
{
    public interface IGuideService : IEntityService<ActiveStatusEnum, Guide>
    {
        Task<Guid> CreateGuide(ICreateGuideContext context);
        Task UpdateGuide(IUpdateGuideContext context);
        Task<Guide> GetGuide(IGetGuideContext context);
    }
}
