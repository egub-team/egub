﻿using EGUB.Core.Contracts.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.TravelPortal.Contracts.BLL.HotelAccommodations
{
    public interface IActionHotelAccommodationContext : IOperationContext
    {
        string Name { get; }
        Guid HotelId { get; }
        Guid RoomId { get; }
        ICollection<Guid> ClientsIds { get; }
    }
}
