﻿using EGUB.Core.Contracts.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.TravelPortal.Contracts.BLL.HotelAccommodations
{
    public interface IHotelAccommodationService : IEntityService<ActiveStatusEnum, HotelAccommodation>
    {
        Task<Guid> CreateHotelAccommodation(ICreateHotelAccommodationContext context);
        Task UpdateHotelAccommodation(IUpdateHotelAccommodationContext context);
        Task<HotelAccommodation> GetHotelAccommodation(IGetHotelAccommodationContext context);
        Task<ICollection<HotelAccommodation>> GetHotelAccommodations(IGetHotelAccommodationsContext context);
    }
}
