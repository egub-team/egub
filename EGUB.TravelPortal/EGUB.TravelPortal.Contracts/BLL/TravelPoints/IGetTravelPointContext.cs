﻿using EGUB.Core.Contracts.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.TravelPortal.Contracts.BLL.TravelPoints
{
    public interface IGetTravelPointContext : IOperationContext
    {
        Guid Id { get; }
    }
}
