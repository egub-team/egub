﻿using EGUB.Core.Contracts.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.TravelPortal.Contracts.BLL.TravelPoints
{
    public interface IActionTravelPointContext : IOperationContext
    {
        string Name { get; }
        GeoCoordinate Coordinate { get; }
        Address Address { get; }
        string Description { get; }
    }
}
