﻿using EGUB.Core.Contracts.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.TravelPortal.Contracts.BLL.TravelPoints
{
    public interface ITravelPointService : IEntityService<ActiveStatusEnum, TravelPoint>
    {
        Task<Guid> CreateTravelPoint(ICreateTravelPointContext context);
        Task UpdateTravelPoint(IUpdateTravelPointContext context);
        Task<TravelPoint> GetTravelPoint(IGetTravelPointContext context);
        Task<ICollection<TravelPoint>> GetTravelPoints(IGetTravelPointContext context);
    }
}
