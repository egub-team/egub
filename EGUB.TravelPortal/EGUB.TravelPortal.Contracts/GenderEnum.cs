﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.TravelPortal.Contracts
{
    public enum GenderEnum
    {
        Female = 0,
        Male = 1
    }
}
