﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.TravelPortal.Contracts
{
    public enum HotelAccommodationRoomAdditionalEnum
    {
        Kitchen,
        Shower,
        Bath,
        Toilet,
        Towels,
        Linen,
        SocketNearTheBed,
        Desk,
        TV,
        Refrigerator,
        Iron,
        IroningFacilities,
        Heating,
        Hairdryer,
        Kitchenware,
        Kitchenette,
        ElectricKettle,
        CableChannels,
        Wardrobe,
        Oven,
        Stovetop,
        DiningTable,
        ToiletPaper,
        Bathrobe,
        SafetyDepositBox,
        Slippers,
        Telephone,
        AirConditioning,
        Minibar,
        ClothesRack,
        CoffeeMachine,
        Soundproofing
    }
}
