﻿using Autofac;
using EGUB.TravelPortal.Contracts.DAL;

namespace EGUB.TravelPortal.DAL
{
    public class ContainerFactory : Module
    {
        public static void RegisterPersonRepository(ContainerBuilder builder)
        {
            builder
                .RegisterType<PersonRepository>()
                .As<IPersonRepository>()
                .InstancePerLifetimeScope();
        }

        public static void RegisterAddressRepository(ContainerBuilder builder)
        {
            builder
                .RegisterType<AddressRepository>()
                .As<IAddressRepository>()
                .InstancePerLifetimeScope();
        }

        public static void RegisterTravelPointRepository(ContainerBuilder builder)
        {
            builder
                .RegisterType<TravelPointRepository>()
                .As<ITravelPointRepository>()
                .InstancePerLifetimeScope();
        }

        public static void RegisterTravelServiceRepository(ContainerBuilder builder)
        {
            builder
                .RegisterType<TravelServiceRepository>()
                .As<ITravelServiceRepository>()
                .InstancePerLifetimeScope();
        }

        public static void RegisterExcursionRepository(ContainerBuilder builder)
        {
            builder
                .RegisterType<ExcursionRepository>()
                .As<IExcursionRepository>()
                .InstancePerLifetimeScope();
        }

        protected override void Load(ContainerBuilder builder)
        {
            RegisterPersonRepository(builder);
        }
    }
}
