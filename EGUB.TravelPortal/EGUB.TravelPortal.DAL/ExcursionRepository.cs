﻿using EGUB.TravelPortal.Contracts.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.TravelPortal.DAL
{
    internal class ExcursionRepository : Core.DAL.BaseRepository, IExcursionRepository
    {
        public async Task SaveExcursion(Guid id)
        {
            await Context.Execute(SQL.Excursion.Commands.SaveExcursion, new
            {
                id
            });
        }
    }
}
