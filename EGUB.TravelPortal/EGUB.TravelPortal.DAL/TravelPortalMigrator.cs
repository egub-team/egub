﻿using EGUB.Core.Contracts;
using EGUB.Core.Contracts.DAL;
using EGUB.Core.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.TravelPortal.DAL
{
    public class TravelPortalMigrator : Migrator
    {
        public TravelPortalMigrator(string name, bool isDropIfExists, IContainer container) : base(name, isDropIfExists, container)
        {
            AddScript<TravelPortalMigrator>(CreatePersonTable);
            AddScript<TravelPortalMigrator>(CreateAddressTable);
            AddScript<TravelPortalMigrator>(CreateTravelServiceTable);
            AddScript<TravelPortalMigrator>(CreateExcursionTable);
            AddScript<TravelPortalMigrator>(CreateTravelPointTable);
            AddScript<TravelPortalMigrator>(CreateExcursionSightTable);
        }

        private async Task CreatePersonTable(IContext context)
        {
            await context.Execute(SQL.Person.Commands.CreatePersonTable);
            await context.Execute(SQL.Person.Commands.CreatePersonReferences);
        }

        private async Task CreateAddressTable(IContext context)
        {
            await context.Execute(SQL.Address.Commands.CreateAddressTable);
            await context.Execute(SQL.Address.Commands.CreateAddressReferences);
        }

        private async Task CreateTravelServiceTable(IContext context)
        {
            await context.Execute(SQL.TravelService.Commands.CreateTravelServiceTable);
            await context.Execute(SQL.TravelService.Commands.CreateTravelServiceReferences);
        }
        private async Task CreateExcursionTable(IContext context)
        {
            await context.Execute(SQL.Excursion.Commands.CreateExcursionTable);
            await context.Execute(SQL.Excursion.Commands.CreateExcursionReferences);
        }

        private async Task CreateExcursionSightTable(IContext context)
        {
            await context.Execute(SQL.ExcursionSight.Commands.CreateExcursionSightTable);
            await context.Execute(SQL.ExcursionSight.Commands.CreateExcursionSightReferenceExcursion);
            await context.Execute(SQL.ExcursionSight.Commands.CreateExcursionSightReferenceTravelPoint);
        }
        private async Task CreateTravelPointTable(IContext context)
        {
            await context.Execute(SQL.TravelPoint.Commands.CreateTravelPointTable);
            await context.Execute(SQL.TravelPoint.Commands.CreateTravelPointReferences);
        }

    }
}
