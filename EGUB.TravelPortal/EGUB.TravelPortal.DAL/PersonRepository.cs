﻿using EGUB.Security.Contracts.DAL;
using EGUB.TravelPortal.Contracts;
using EGUB.TravelPortal.Contracts.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.TravelPortal.DAL
{
    internal class PersonRepository : Core.DAL.BaseRepository, IPersonRepository
    {
        public async Task SavePerson(Guid id, Guid? userId, string firstName, string secondName, string parentName, DateOnly? birthDay,
            bool? gender, string email, string phoneNumber)
        {
            string birthText = birthDay?.ToString("yyyy-MM-dd");
            await Context.Execute(SQL.Person.Commands.SavePerson, new
            {
                id,
                userId,
                firstName,
                secondName,
                parentName,
                birthDay = birthText,
                gender,
                email,
                phoneNumber
            });
        }

        public async Task<ICollection<(Guid Id, Guid? UserId, string FirstName, string SecondName, string ParentName, DateOnly? BirthDay,
            bool? Gender, string Email, string PhoneNumber)>> GetItems(ICollection<Guid> ids)
        {
            if (!ids.Any())
            {
                return ArraySegment<(Guid Id, Guid? UserId, string FirstName, string SecondName, string ParentName, DateOnly? BirthDay,
            bool? Gender, string Email, string PhoneNumber)>.Empty;
            }
            return await this.Context
                .Query(
                    r =>
                        (
                            Id: r.ParseGuid("Id").Value,
                            UserId: r.ParseGuid("UserId"),
                            FirstName: r.GetValue<string>("FirstName"),
                            SecondName: r.GetValue<string>("SecondName"),
                            ParentName: r.GetValue<string>("ParentName"),
                            BirthDay: r.ParseDateOnly("BirthDay"),
                            Gender: r.GetValue<bool?>("Gender"),
                            Email: r.GetValue<string>("Email"),
                            PhoneNumber: r.GetValue<string>("PhoneNumber")
                        ),
                    SQL.Person.Commands.GetItems,
                    new { xmlData = this.ConvertToXmlData(ids) });
        }
    }
}
