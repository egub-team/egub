﻿using EGUB.TravelPortal.Contracts;
using EGUB.TravelPortal.Contracts.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.TravelPortal.DAL
{
    internal class AddressRepository : Core.DAL.BaseRepository, IAddressRepository
    {
        public async Task SaveAddress(Guid entityId, string countryCode, string city, string street, string number)
        {
            await Context.Execute(SQL.Address.Commands.SaveAddress, new
            {
                entityId,
                countryCode,
                city,
                street,
                number
            });
        }

        public async Task<ICollection<(Guid EntityId, string CountryCode, string City, string Street, string Number)>>
            GetItems(ICollection<Guid> ids)
        {
            if (!ids.Any())
            {
                return ArraySegment<(Guid EntityId, string CountryCode, string City, string Street, string Number)>.Empty;
            }
            return await this.Context
                .Query(
                    r =>
                        (
                            EntityId: r.ParseGuid("EntityId").Value,
                            CountryCode: r.GetValue<string>("CountryCode"),
                            City: r.GetValue<string>("City"),
                            Street: r.GetValue<string>("Street"),
                            Number: r.GetValue<string>("Number")
                        ),
                    SQL.Address.Commands.GetItems,
                    new { xmlData = this.ConvertToXmlData(ids) });
        }
    }
}
