﻿using EGUB.Core.DAL;
using EGUB.TravelPortal.Contracts.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.TravelPortal.DAL
{
    internal class TravelServiceRepository : Core.DAL.BaseRepository, ITravelServiceRepository
    {
        public async Task SaveTravelService(Guid id, string name, string type)
        {
            await Context.Execute(SQL.TravelService.Commands.SaveTravelService, new
            {
                id,
                name,
                type
            });
        }
    }
}
