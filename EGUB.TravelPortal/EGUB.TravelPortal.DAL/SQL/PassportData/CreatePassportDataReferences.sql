﻿alter table PassportData  
    add constraint FK_PassportData_Entity foreign key(Id)
    references core.Entity (Id)
    on delete cascade;