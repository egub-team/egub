﻿create table PassportData(
    Id uniqueidentifier not null,
    PersonId uniqueidentifier null,
    Type nvarchar(255) not null,
    Code nvarchar(255) not null,
    DateOfIssue date not null,
    DateOfExpiry date null,
    Authority nvarchar(255) not null,
    CountryCode nvarchar(2) not null,
    constraint PK_PassportData primary key clustered 
    (
        Id asc
    ));