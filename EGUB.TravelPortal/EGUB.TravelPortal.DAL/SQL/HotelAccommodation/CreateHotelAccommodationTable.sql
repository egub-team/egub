﻿create table HotelAccommodation(
    Id uniqueidentifier not null,
    HotelId uniqueidentifier not null,
    RoomId uniqueidentifier not null,
    constraint PK_HotelAccommodation primary key clustered 
    (
        Id asc
    ));