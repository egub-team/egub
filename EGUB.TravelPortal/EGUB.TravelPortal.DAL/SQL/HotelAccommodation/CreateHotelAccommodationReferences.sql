﻿alter table HotelAccommodation  
    add constraint FK_HotelAccommodation_TravelService foreign key(Id)
    references TravelService(Id)
    on delete cascade;