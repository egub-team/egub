﻿update Person
set 
	UserId = @userId,
	FirstName = @firstName,
	SecondName = @secondName,
	ParentName = @parentName,
	BirthDay = @birthDay,
	Gender = @gender,
	Email = @email,
	PhoneNumber = @phoneNumber
where Id = @id

if (@@ROWCOUNT > 0)
begin
	return;
end

insert into Person(Id, UserId, FirstName, SecondName, ParentName, BirthDay, Gender, Email, PhoneNumber)
values(@id, @userId, @firstName, @secondName, @parentName, @birthDay, @gender, @email, @phoneNumber)