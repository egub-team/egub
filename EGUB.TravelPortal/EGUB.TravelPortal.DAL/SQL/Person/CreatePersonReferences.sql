﻿alter table Person  
    add constraint FK_Person_Entity foreign key(Id)
    references core.Entity (Id)
    on delete cascade;