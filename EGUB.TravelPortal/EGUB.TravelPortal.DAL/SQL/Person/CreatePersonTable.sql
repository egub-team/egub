﻿create table Person(
    Id uniqueidentifier not null,
    UserId uniqueidentifier null,
    FirstName nvarchar(255) not null,
    SecondName nvarchar(255) not null,
    ParentName nvarchar(255) null,
    BirthDay char(10) null,
    Gender bit null,
    Email nvarchar(255) null,
    PhoneNumber nvarchar(50) null,
    constraint PK_Person primary key clustered 
    (
        Id asc
    ));