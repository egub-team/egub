﻿declare @xml xml = @xmlData

declare @data table (Id uniqueidentifier)
insert into @data(Id)
select x.value('.', 'uniqueidentifier')
from @xml.nodes('id') f(x)

select p.Id,
	   p.UserId,
	   p.FirstName,
       p.SecondName,
       p.ParentName,
       p.BirthDay,
       p.Gender,
       p.Email,
       p.PhoneNumber
from @data i
join Person p on p.Id = i.Id