﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EGUB.TravelPortal.DAL.SQL.Person {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "17.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Commands {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Commands() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("EGUB.TravelPortal.DAL.SQL.Person.Commands", typeof(Commands).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to alter table Person  
        ///    add constraint FK_Person_Entity foreign key(Id)
        ///    references core.Entity (Id)
        ///    on delete cascade;.
        /// </summary>
        internal static string CreatePersonReferences {
            get {
                return ResourceManager.GetString("CreatePersonReferences", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to create table Person(
        ///    Id uniqueidentifier not null,
        ///    UserId uniqueidentifier null,
        ///    FirstName nvarchar(255) not null,
        ///    SecondName nvarchar(255) not null,
        ///    ParentName nvarchar(255) null,
        ///    BirthDay char(10) null,
        ///    Gender bit null,
        ///    Email nvarchar(255) null,
        ///    PhoneNumber nvarchar(50) null,
        ///    constraint PK_Person primary key clustered 
        ///    (
        ///        Id asc
        ///    ));.
        /// </summary>
        internal static string CreatePersonTable {
            get {
                return ResourceManager.GetString("CreatePersonTable", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to .
        /// </summary>
        internal static string GetItems {
            get {
                return ResourceManager.GetString("GetItems", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to update Person
        ///set 
        ///	UserId = @userId,
        ///	FirstName = @firstName,
        ///	SecondName = @secondName,
        ///	ParentName = @parentName,
        ///	BirthDay = @birthDay,
        ///	Gender = @gender,
        ///	Email = @email,
        ///	PhoneNumber = @phoneNumber
        ///where Id = @id
        ///
        ///if (@@ROWCOUNT &gt; 0)
        ///begin
        ///	return;
        ///end
        ///
        ///insert into Person(Id, UserId, FirstName, SecondName, ParentName, BirthDay, Gender, Email, PhoneNumber)
        ///values(@id, @userId, @firstName, @secondName, @parentName, @birthDay, @gender, @email, @phoneNumber).
        /// </summary>
        internal static string SavePerson {
            get {
                return ResourceManager.GetString("SavePerson", resourceCulture);
            }
        }
    }
}
