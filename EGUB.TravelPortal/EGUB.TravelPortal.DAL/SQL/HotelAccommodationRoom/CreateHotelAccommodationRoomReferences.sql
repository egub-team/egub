﻿alter table HotelAccommodationRoom  
    add constraint FK_HotelAccommodationRoom_Entity foreign key(Id)
    references core.Entity(Id)
    on delete cascade;