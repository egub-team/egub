﻿create table HotelAccommodationRoom(
    Id uniqueidentifier not null,
    SingleBeds tinyint null,
    DoubleBeds tinyint null,
    BunkBeds tinyint null,
    RoomType nvarchar(255) null,
    Rooms tinyint null,
    Description nvarchar(255) null,
    constraint PK_HotelAccommodationRoom primary key clustered 
    (
        EntityId asc
    ));