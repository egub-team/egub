﻿create table HotelPoint(
    Id uniqueidentifier not null,
    Name nvarchar(255) not null,
    Latitude decimal(8,6) not null,
    Longitude decimal(9,6) not null,
    Description nvarchar(500) null,
    Star nvarchar(5) not null,
    constraint PK_HotelPoint primary key clustered 
    (
        Id asc
    ));