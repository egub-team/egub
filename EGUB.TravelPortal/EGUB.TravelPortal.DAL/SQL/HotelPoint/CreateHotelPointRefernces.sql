﻿alter table HotelPoint  
    add constraint FK_HotelPoint_Entity foreign key(Id)
    references core.Entity (Id)
    on delete cascade;