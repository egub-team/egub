﻿update TravelPoint
set 
	Name = @name,
	Latitude = @latitude,
	Longitude = @longitude,
	Description = @description
where Id = @id

if (@@ROWCOUNT > 0)
begin
	return;
end

insert into TravelPoint(Id, Name, Latitude, Longitude, Description)
values(@id, @name, @latitude, @longitude, @description)