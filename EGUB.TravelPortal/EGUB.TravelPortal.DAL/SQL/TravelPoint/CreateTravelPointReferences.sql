﻿alter table TravelPoint  
    add constraint FK_TravelPoint_Entity foreign key(Id)
    references core.Entity (Id)
    on delete cascade;