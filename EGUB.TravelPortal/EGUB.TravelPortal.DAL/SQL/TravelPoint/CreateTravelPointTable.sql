﻿create table TravelPoint(
    Id uniqueidentifier not null,
    Name nvarchar(255) not null,
    Latitude decimal(8,6) not null,
    Longitude decimal(9,6) not null,
    Description nvarchar(500) null,
    constraint PK_TravelPoint primary key clustered 
    (
        Id asc
    ));