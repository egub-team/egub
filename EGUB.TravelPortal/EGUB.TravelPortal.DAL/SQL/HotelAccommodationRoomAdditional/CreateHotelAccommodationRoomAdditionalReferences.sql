﻿alter table HotelAccommodationRoomAdditional  
    add constraint FK_HotelAccommodationRoomAdditional_HotelAccommodationRoom foreign key(HotelAccommodationRoomId)
    references HotelAccommodationRoom(Id)
    on delete cascade;