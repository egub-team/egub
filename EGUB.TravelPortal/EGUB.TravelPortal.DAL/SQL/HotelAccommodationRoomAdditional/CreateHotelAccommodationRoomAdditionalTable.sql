﻿create table HotelAccommodationRoomAdditional(
    HotelAccommodationRoomId uniqueidentifier not null,
    Additional nvarchar(255) not null,
    constraint PK_HotelAccommodationRoomAdditional primary key clustered 
    (
        HotelAccommodationRoomId asc,
        Additional asc
    ));