﻿create table Address(
    EntityId uniqueidentifier not null,
    CountryCode nvarchar(2) not null,
    City nvarchar(255) not null,
    Street nvarchar(255) null,
    Number nvarchar(255) null,
    constraint PK_Address primary key clustered 
    (
        EntityId asc
    ));