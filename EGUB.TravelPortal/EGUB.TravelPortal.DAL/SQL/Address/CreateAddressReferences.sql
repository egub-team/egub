﻿alter table Address  
    add constraint FK_Address_Entity foreign key(EntityId)
    references core.Entity(Id)
    on delete cascade;