﻿update Address
set 
	EntityId = @entityId,
	CountryCode = @countryCode,
	City = @city,
	Street = @street,
	Number = @number
where EntityId = @entityId

if (@@ROWCOUNT > 0)
begin
	return;
end

insert into Address(EntityId, CountryCode, City, Street, Number)
values(@entityId, @countryCode, @city, @street, @number)