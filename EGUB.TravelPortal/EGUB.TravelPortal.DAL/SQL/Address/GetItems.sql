﻿declare @xml xml = @xmlData

declare @data table (Id uniqueidentifier)
insert into @data(Id)
select x.value('.', 'uniqueidentifier')
from @xml.nodes('id') f(x)

select a.EntityId,
	   a.CountryCode,
	   a.City,
       a.Street,
       a.Number
from @data i
join Address a on a.EntityId = i.Id