﻿create table Excursion(
    Id uniqueidentifier not null,
    constraint PK_Excursion primary key clustered 
    (
        Id asc
    ));