﻿alter table Excursion  
    add constraint FK_Excursion_TravelService foreign key(Id)
    references TravelService(Id)
    on delete cascade;