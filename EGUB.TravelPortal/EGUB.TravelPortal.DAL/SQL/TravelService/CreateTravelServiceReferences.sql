﻿alter table TravelService  
    add constraint FK_TravelService_Entity foreign key(Id)
    references core.Entity (Id)
    on delete cascade;