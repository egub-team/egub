﻿update TravelService
set 
	Name = @name,
	Type = @type
where Id = @id

if (@@ROWCOUNT > 0)
begin
	return;
end

insert into TravelService(Id, Name, Type)
values(@id, @name, @type)