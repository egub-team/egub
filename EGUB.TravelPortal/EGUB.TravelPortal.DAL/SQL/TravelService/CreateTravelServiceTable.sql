﻿create table TravelService(
    Id uniqueidentifier not null,
    Name nvarchar(255) not null,
    Type nvarchar(50) not null,
    constraint PK_TravelService primary key clustered 
    (
        Id asc
    )); 