﻿alter table ExcursionSight
    add constraint FK_ExcursionSight_Excursion foreign key(ExcursionId)
    references Excursion(Id)
    on delete cascade;