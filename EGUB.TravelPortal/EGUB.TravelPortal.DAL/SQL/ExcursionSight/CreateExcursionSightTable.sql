﻿create table ExcursionSight(
    ExcursionId uniqueidentifier not null,
    TravelPointId uniqueidentifier not null,
    constraint PK_ExcursionSight primary key clustered 
    (
        ExcursionId asc,
        TravelPointId asc
    ));