﻿alter table ExcursionSight
    add constraint FK_ExcursionSight_TravelPoint foreign key(TravelPointId)
    references TravelPoint(Id);