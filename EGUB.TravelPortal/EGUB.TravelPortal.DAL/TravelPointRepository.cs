﻿using EGUB.Core.Contracts;
using EGUB.TravelPortal.Contracts.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.TravelPortal.DAL
{
    internal class TravelPointRepository : Core.DAL.BaseRepository, ITravelPointRepository
    {
        public async Task SaveTravelPoint(Guid id, string name, decimal latitude, decimal longitude, string description)
        {
            await Context.Execute(SQL.TravelPoint.Commands.SaveTravelPoint, new
            {
                id,
                name,
                latitude,
                longitude,
                description
            });
        }

        public Task<ICollection<Guid>> GetTravelPointIds((string name, decimal latitude, decimal longitude, string description) filter)
        {
            throw new NotImplementedException();

//            var sql = new StringBuilder($@"select t.Id from TravelPoint t")
//               .AppendLine();
//            var paramsDictionary = new Dictionary<string, object>();

//            var filterSql = new List<string>();
//            if (!filter.name.IsNullOrEmpty())
//            {
//                paramsDictionary.Add(nameof(filter.name), filter.name);
//                filterSql.Add("t.Name like '%' + @name + '%'");
//            }
//            if (!filter.latitude.IsNullOrEmpty())
//            {
//                paramsDictionary.Add(nameof(filter.email), filter.email);
//                filterSql.Add("u.Email like '%' + @email + '%'");
//            }
//            if (!filter.personName.IsNullOrEmpty())
//            {
//                paramsDictionary.Add(nameof(filter.personName), filter.personName);
//                filterSql.Add("u.PersonName like '%' + @personName + '%'");
//            }
//            if (!filter.phoneNumber.IsNullOrEmpty())
//            {
//                paramsDictionary.Add(nameof(filter.phoneNumber), filter.phoneNumber);
//                filterSql.Add("u.PhoneNumber like '%' + @phoneNumber + '%'");
//            }

//            if (!filterSql.IsNullOrEmpty())
//            {
//                sql.AppendLine($@"
//where 
//    {string.Join(@" and 
//    ", filterSql)}");
//            }

//            var query = sql.ToString();
//            var ids = await Context
//                .Query(
//                    r => r.GetValue<Guid>("Id"),
//                    query,
//                    paramsDictionary);
//            return ids
//                .Distinct()
//                .ToList();
        }
    }
}
