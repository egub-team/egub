﻿using EGUB.Core.Contracts;
using EGUB.Core.Contracts.DAL;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EGUB.Core.DAL
{
    public class Migrator : IMigrator
    {
        private readonly IContainer _container;
        private readonly IConfiguration _configuration;
        private readonly IDictionary<string, ICollection<Func<IContext, Task>>> _scripts = new Dictionary<string, ICollection<Func<IContext, Task>>>();
        private readonly string _name;
        private readonly bool _isDropIfExists;

        public Migrator(string name, bool isDropIfExists, IContainer container, IConfiguration configuration)
        {
            _name = name;
            _isDropIfExists = isDropIfExists;
            _container = container;
            _configuration = configuration;

            AddScript<Migrator>(CreateMigrationTable);
            AddScript<Migrator>(CreateEntityTable);
            AddScript<Migrator>(CreateEntityNoteTable);
            AddScript<Migrator>(CreateEntityStatusTable);
            AddScript<Migrator>(CreateEntityChangeTable);
        }

        public async Task Execute()
        {
            using (var context = new Context(_name, _container.GetItem<IConfiguration>()))
                await context.CheckDataBase(_isDropIfExists);    
                
            foreach (var item in _scripts)
            {
                var currentVersion = _isDropIfExists
                    ? default
                    : await GetCurrentVersion(item.Key);
                foreach (var script in item.Value.Skip(currentVersion))
                {
                    currentVersion++;
                    using var context = new Context(_name, _container.GetItem<IConfiguration>());
                    await script(context);
                    await AddVersion(context, item.Key, currentVersion);
                }
            }
        }

        protected void AddContextScript<TCode, TScript>()
            where TScript : BaseContextScript, new()
        {
            var script = new TScript();
            script.Init(_container);
            AddScript<TCode>(c => script.Execute(c, _configuration));
        }

        protected void AddScript<TCode, TScript>()
            where TScript : BaseScript, new()
        {
            var script = new TScript();
            AddScript<TCode>(c => script.Execute(c, _configuration));
        }

        protected void AddScript<TCode>(Func<IContext, Task> script)
        {
            var code = typeof(TCode).Name;
            if (!_scripts.TryGetValue(code, out var scripts))
            {
                scripts = new List<Func<IContext, Task>>();
                _scripts.Add(code, scripts);
            }

            scripts.Add(script);
        }

        private async Task CreateMigrationTable(IContext context)
        {
            await context.Execute("create schema core");
            await context.Execute(SQL.Migration.Commands.CreateTable);
        }

        private Task CreateEntityTable(IContext context) =>
            context.Execute(SQL.Entity.Commands.CreateEntityTable);

        private async Task CreateEntityNoteTable(IContext context)
        {
            await context.Execute(SQL.Entity.Commands.CreateEntityNoteTable);
            await context.Execute(SQL.Entity.Commands.CreateEntityNoteReferences);
        }

        private async Task CreateEntityStatusTable(IContext context)
        {
            await context.Execute(SQL.Entity.Commands.CreateEntityStatusTable);
            await context.Execute(SQL.Entity.Commands.CreateEntityStatusReferences);
        }

        private Task CreateEntityChangeTable(IContext context) =>
            context.Execute(SQL.Entity.Commands.CreateEntityChangeTable);

        private Task AddVersion(IContext context, string code, int version) =>
            context.Execute(SQL.Migration.Commands.AddVersion, new { code, version });

        private async Task<int> GetCurrentVersion(string code)
        {
            using var context = new Context(_name, _container.GetItem<IConfiguration>());
            var transaction = await context.BeginTransaction();
            var result = (await context
                .Query(
                    r => Convert.ToInt32(r["CurrentVersion"]), 
                    SQL.Migration.Commands.GetCurrentVersion, 
                    new { code })).Single();
            await transaction.Commit();
            return result;
        }
    }
}
