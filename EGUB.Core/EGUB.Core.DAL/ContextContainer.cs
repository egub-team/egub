﻿using EGUB.Core.Contracts;
using EGUB.Core.Contracts.DAL;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;

namespace EGUB.Core.DAL
{
    internal class ContextContainer : IContextContainer
    {
        private readonly IContainer _container;

        public ContextContainer(IContainer container)
        {
            _container = container;
        }

        public async Task ExecuteWithContext(string name, Func<IRepositoryContainer, Task> task, bool hasTransaction = true) 
        {
            var repositoryContainer = _container.GetItem<IRepositoryContainer>();

            using var context = new Context(name, _container.GetItem<IConfiguration>());
            ITransaction transaction = null;
            if (hasTransaction)
                transaction = await context.BeginTransaction();

            repositoryContainer.Init(context);
            await task(repositoryContainer);

            if (hasTransaction)
                await transaction.Commit();
        }
    }
}
