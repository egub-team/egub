﻿using EGUB.Core.Contracts.DAL;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Threading.Tasks;

namespace EGUB.Core.DAL
{
    public abstract class BaseScript
    {
        public abstract Task Execute(IContext context, IConfiguration configuration);

        protected IEnumerable<TItem> QueryFromLines<TItem>(Func<IDictionary<int, string>, TItem> mapFunc, string lines, char separator = ';')
        {
            if (string.IsNullOrEmpty(lines))
                yield break;

            var lineArray = lines.Split('\n');
            foreach (var line in lineArray)
            {
                var rows = line
                    .Split(separator)
                    .Select((x, i) => new { Key = i, Value = x.Trim() })
                    .ToDictionary(i => i.Key, i => i.Value);

                yield return mapFunc(rows);
            }
        }
    }
}
