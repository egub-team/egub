﻿using EGUB.Core.Contracts.DAL;

namespace EGUB.Core.DAL
{
    internal interface IContextRepository : IRepository
    {
        IContext Context { get; }
    }
}
