﻿using Autofac;
using EGUB.Core.Contracts.DAL;

namespace EGUB.Core.DAL
{
    public class ContainerFactory : Module
    {
        public static void RegisterContextContainer(ContainerBuilder builder)
        {
            builder
                .RegisterType<ContextContainer>()
                .As<IContextContainer>();
        }

        public static void RegisterRepositoryContainer(ContainerBuilder builder)
        {
            builder
                .RegisterType<RepositoryContainer>()
                .As<IRepositoryContainer>();
        }

        public static void RegisterEntityRepository(ContainerBuilder builder)
        {
            builder
                .RegisterType<EntityRepository>()
                .As<IEntityRepository>();
        }

        public static void RegisterMigrationItems(ContainerBuilder builder)
        {
            RegisterContextContainer(builder);
            RegisterRepositoryContainer(builder);
            builder
                .RegisterType<ContextRepository>()
                .As<IContextRepository>();
        }


        protected override void Load(ContainerBuilder builder)
        {
            RegisterMigrationItems(builder);
            RegisterEntityRepository(builder);
        }
    }
}
