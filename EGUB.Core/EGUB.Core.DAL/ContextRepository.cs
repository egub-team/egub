﻿using EGUB.Core.Contracts.DAL;

namespace EGUB.Core.DAL
{
    internal class ContextRepository : IContextRepository
    {
        public IContext Context { get; private set; } = default!;

        public void Init(IContext context)
        {
            Context = context;
        }
    }
}
