﻿using EGUB.Core.Contracts.DAL;
using System;
using System.Data;

namespace EGUB.Core.DAL
{
    internal class DataRowInfo : IDataRow
    {
        private readonly IDataReader _dataReader;

        public DataRowInfo(IDataReader dataReader)
        {
            _dataReader = dataReader;
        }

        public object this[string name] => _dataReader[name];

        public T GetValue<T>(string name)
        {
            var value = this[name];
            return value == DBNull.Value || value == null ? default : (T)value;
        }

        public Guid? ParseGuid(string name)
        {
            var value = this[name];
            
            return value == DBNull.Value || value == null 
                ? default(Guid?)
                : Guid.Parse(value.ToString());
        }

        public DateOnly? ParseDateOnly(string name)
        {
            var value = this[name];

            return value == DBNull.Value || value == null
                ? default(DateOnly?)
                : DateOnly.Parse(value.ToString());
        }
    }
}
