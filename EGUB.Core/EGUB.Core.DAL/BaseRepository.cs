﻿using EGUB.Core.Contracts.DAL;
using System.Collections.Generic;
using System.Linq;

namespace EGUB.Core.DAL
{
    public abstract class BaseRepository : IRepository
    {
        protected IContext Context { get; private set; }

        public void Init(IContext context)
        {
            Context = context;
        }

        protected string ConvertToXmlData<T>(ICollection<T> items, string tagName = "id") =>
            !items.Any() ? default : $@"<{tagName}>{string.Join($"</{tagName}><{tagName}>", items)}</{tagName}>";
    }
}
