﻿declare @CurrentVersion int = 0;

if (object_id('core.Migration') is null)
begin 
	select @CurrentVersion as CurrentVersion
	return 
end

select @CurrentVersion = max(VersionNumber)
from core.Migration
where Code = @code

select isnull(@CurrentVersion, 0) as CurrentVersion