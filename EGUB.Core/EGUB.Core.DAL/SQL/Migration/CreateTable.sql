﻿create table core.Migration(
    Code nvarchar(255) not null,
	VersionNumber int not null,
	Created datetime not null,
    constraint [PK_Migration] primary key clustered 
    (
        Code asc,
	    VersionNumber asc
    ));