﻿declare @lastStatusValue nvarchar(50);
select 
	@lastStatusValue = StatusValue
from (
	select 
		Created,
		StatusValue,
		LastCreated = max([Created]) over (partition by Id)
	from [core].[EntityStatus]
	where Id = @id
	) s
where s.Created = s.LastCreated

if (isnull(@lastStatusValue, '') = isnull(@statusValue, ''))
begin
	return;
end

insert into core.EntityStatus(Id, Created, NoteValue, StatusValue)
values (@id, getdate(), @noteValue, @statusValue)