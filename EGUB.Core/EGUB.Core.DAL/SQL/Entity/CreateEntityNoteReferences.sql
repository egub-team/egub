﻿alter table core.EntityNote  
    add constraint FK_EntityNote_Entity foreign key(Id)
    references core.Entity (Id)
    on delete cascade;