﻿declare @xml xml = @xmlData

declare @data table (Id uniqueidentifier)
insert into @data(Id)
select x.value('.', 'uniqueidentifier')
from @xml.nodes('id') f(x)

select e.Id
from @data i
join core.Entity e on
	e.Id = i.Id and
	e.EntityType = @entityType