﻿create table core.EntityChange(
	Id uniqueidentifier not null,
	Created datetime not null,
	EntityData nvarchar(max),
	UserId uniqueidentifier not null,
    constraint PK_EntityChange primary key clustered 
    (
        Id asc,
		Created asc
    ))