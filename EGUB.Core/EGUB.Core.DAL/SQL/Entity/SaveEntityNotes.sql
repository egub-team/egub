﻿declare @xml xml = @itemsData
declare @data table (Id uniqueidentifier, Created datetime, NoteValue nvarchar(max))
insert into @data(Id, Created, NoteValue)
select 
	@id,
	x.value('Created[1]', 'datetime'),
	x.value('Note[1]', 'nvarchar(max)')
from @xml.nodes('item') f(x)

insert into core.EntityNote(Id, Created, NoteValue)
select s.Id, s.Created, s.NoteValue
from @data s
left join core.EntityNote t on 
	t.Id = s.Id and
	t.Created = s.Created
where t.Id is null