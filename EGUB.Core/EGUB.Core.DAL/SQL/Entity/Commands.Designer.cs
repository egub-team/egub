﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EGUB.Core.DAL.SQL.Entity {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "17.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Commands {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Commands() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("EGUB.Core.DAL.SQL.Entity.Commands", typeof(Commands).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to create table core.EntityChange(
        ///	Id uniqueidentifier not null,
        ///	Created datetime not null,
        ///	EntityData nvarchar(max),
        ///	UserId uniqueidentifier not null,
        ///    constraint PK_EntityChange primary key clustered 
        ///    (
        ///        Id asc,
        ///		Created asc
        ///    )).
        /// </summary>
        internal static string CreateEntityChangeTable {
            get {
                return ResourceManager.GetString("CreateEntityChangeTable", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to alter table core.EntityNote  
        ///    add constraint FK_EntityNote_Entity foreign key(Id)
        ///    references core.Entity (Id)
        ///    on delete cascade;.
        /// </summary>
        internal static string CreateEntityNoteReferences {
            get {
                return ResourceManager.GetString("CreateEntityNoteReferences", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to create table core.EntityNote(
        ///	Id uniqueidentifier not null,
        ///	Created datetime not null,
        ///	NoteValue nvarchar(max),
        ///    constraint PK_EntityNote primary key clustered 
        ///    (
        ///        Id asc,
        ///		Created asc
        ///    )).
        /// </summary>
        internal static string CreateEntityNoteTable {
            get {
                return ResourceManager.GetString("CreateEntityNoteTable", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to alter table core.EntityStatus  
        ///    add constraint FK_EntityStatus_Entity foreign key(Id)
        ///    references core.Entity (Id)
        ///    on delete cascade;.
        /// </summary>
        internal static string CreateEntityStatusReferences {
            get {
                return ResourceManager.GetString("CreateEntityStatusReferences", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to create table core.EntityStatus(
        ///	Id uniqueidentifier not null,
        ///	Created datetime not null,
        ///	NoteValue nvarchar(max),
        ///	StatusValue nvarchar(50) not null,
        ///    constraint PK_EntityStatus primary key clustered 
        ///    (
        ///        Id asc,
        ///		Created asc
        ///    )).
        /// </summary>
        internal static string CreateEntityStatusTable {
            get {
                return ResourceManager.GetString("CreateEntityStatusTable", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to create table core.Entity(
        ///    Id uniqueidentifier not null primary key,
        ///    EntityType nvarchar(50) not null,
        ///    Created datetime not null,
        ///    LastModified datetime null);.
        /// </summary>
        internal static string CreateEntityTable {
            get {
                return ResourceManager.GetString("CreateEntityTable", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to delete from [core].[Entity]
        ///where [Id] = @id.
        /// </summary>
        internal static string Delete {
            get {
                return ResourceManager.GetString("Delete", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to .
        /// </summary>
        internal static string GetEntityIds {
            get {
                return ResourceManager.GetString("GetEntityIds", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to declare @xml xml = @xmlData
        ///
        ///declare @data table (Id uniqueidentifier)
        ///insert into @data(Id)
        ///select x.value(&apos;.&apos;, &apos;uniqueidentifier&apos;)
        ///from @xml.nodes(&apos;id&apos;) f(x)
        ///
        ///select s.*
        ///from @data i
        ///join core.EntityNote s on s.Id = i.Id.
        /// </summary>
        internal static string GetNotes {
            get {
                return ResourceManager.GetString("GetNotes", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to declare @xml xml = @xmlData
        ///
        ///declare @data table (Id uniqueidentifier)
        ///insert into @data(Id)
        ///select x.value(&apos;.&apos;, &apos;uniqueidentifier&apos;)
        ///from @xml.nodes(&apos;id&apos;) f(x)
        ///
        ///select s.*
        ///from @data i
        ///join core.EntityStatus s on s.Id = i.Id.
        /// </summary>
        internal static string GetStatuses {
            get {
                return ResourceManager.GetString("GetStatuses", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to update core.Entity
        ///set LastModified = getdate()
        ///where Id = @id;
        ///
        ///if (@@ROWCOUNT &gt; 0)
        ///begin
        ///	return;
        ///end
        ///
        ///insert into core.Entity(Id, EntityType, Created)
        ///values (@id, @entityType, getdate()).
        /// </summary>
        internal static string SaveEntity {
            get {
                return ResourceManager.GetString("SaveEntity", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to insert into core.EntityChange(Id, Created, EntityData, UserId)
        ///values (@id, getdate(), @entityData, @userId).
        /// </summary>
        internal static string SaveEntityChange {
            get {
                return ResourceManager.GetString("SaveEntityChange", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to declare @xml xml = @itemsData
        ///declare @data table (Id uniqueidentifier, Created datetime, NoteValue nvarchar(max))
        ///insert into @data(Id, Created, NoteValue)
        ///select 
        ///	@id,
        ///	x.value(&apos;Created[1]&apos;, &apos;datetime&apos;),
        ///	x.value(&apos;Note[1]&apos;, &apos;nvarchar(max)&apos;)
        ///from @xml.nodes(&apos;item&apos;) f(x)
        ///
        ///insert into core.EntityNote(Id, Created, NoteValue)
        ///select s.Id, s.Created, s.NoteValue
        ///from @data s
        ///left join core.EntityNote t on 
        ///	t.Id = s.Id and
        ///	t.Created = s.Created
        ///where t.Id is null.
        /// </summary>
        internal static string SaveEntityNotes {
            get {
                return ResourceManager.GetString("SaveEntityNotes", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to declare @lastStatusValue nvarchar(50);
        ///select 
        ///	@lastStatusValue = StatusValue
        ///from (
        ///	select 
        ///		Created,
        ///		StatusValue,
        ///		LastCreated = max([Created]) over (partition by Id)
        ///	from [core].[EntityStatus]
        ///	where Id = @id
        ///	) s
        ///where s.Created = s.LastCreated
        ///
        ///if (isnull(@lastStatusValue, &apos;&apos;) = isnull(@statusValue, &apos;&apos;))
        ///begin
        ///	return;
        ///end
        ///
        ///insert into core.EntityStatus(Id, Created, NoteValue, StatusValue)
        ///values (@id, getdate(), @noteValue, @statusValue).
        /// </summary>
        internal static string SaveEntityStatus {
            get {
                return ResourceManager.GetString("SaveEntityStatus", resourceCulture);
            }
        }
    }
}
