﻿create table core.EntityStatus(
	Id uniqueidentifier not null,
	Created datetime not null,
	NoteValue nvarchar(max),
	StatusValue nvarchar(50) not null,
    constraint PK_EntityStatus primary key clustered 
    (
        Id asc,
		Created asc
    ))