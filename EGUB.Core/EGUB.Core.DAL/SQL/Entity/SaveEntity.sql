﻿update core.Entity
set LastModified = getdate()
where Id = @id;

if (@@ROWCOUNT > 0)
begin
	return;
end

insert into core.Entity(Id, EntityType, Created)
values (@id, @entityType, getdate())