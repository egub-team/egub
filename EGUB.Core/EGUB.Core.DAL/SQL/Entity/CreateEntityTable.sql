﻿create table core.Entity(
    Id uniqueidentifier not null primary key,
    EntityType nvarchar(50) not null,
    Created datetime not null,
    LastModified datetime null);