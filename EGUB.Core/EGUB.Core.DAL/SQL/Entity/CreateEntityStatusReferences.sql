﻿alter table core.EntityStatus  
    add constraint FK_EntityStatus_Entity foreign key(Id)
    references core.Entity (Id)
    on delete cascade;