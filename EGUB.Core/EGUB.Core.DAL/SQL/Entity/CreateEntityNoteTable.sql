﻿create table core.EntityNote(
	Id uniqueidentifier not null,
	Created datetime not null,
	NoteValue nvarchar(max),
    constraint PK_EntityNote primary key clustered 
    (
        Id asc,
		Created asc
    ))