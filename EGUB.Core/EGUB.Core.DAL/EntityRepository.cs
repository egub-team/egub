﻿using EGUB.Core.Contracts.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EGUB.Core.Helpers;

namespace EGUB.Core.DAL
{
    internal class EntityRepository : BaseRepository, IEntityRepository
    {
        public Task SaveEntity(Guid id, string entityType) =>
            Context.Execute(SQL.Entity.Commands.SaveEntity, new { id, entityType });

        public Task SaveEntityNotes(Guid id, ICollection<SaveEntityNoteItemData> notes)
        {
            var items = notes
                .Select(i => $"{ConvertToXmlData(new[] { i.Note }, nameof(i.Note))}{ConvertToXmlData(new[] { i.Created.ToString("o") }, nameof(i.Created))}")
                .ToArray();
            var itemsData = this.ConvertToXmlData(items, "item");
            return Context.Execute(SQL.Entity.Commands.SaveEntityNotes, new { id, itemsData });
        }

        public Task SaveEntityStatus(Guid id, string noteValue, string statusValue) =>
            Context.Execute(SQL.Entity.Commands.SaveEntityStatus, new { id, noteValue, statusValue });

        public Task Delete(Guid id) =>
            Context.Execute(SQL.Entity.Commands.Delete, new { id });

        public async Task<ICollection<EntityNoteItemData>> GetNotes(ICollection<Guid> ids)
        {
            if (ids.IsNullOrEmpty())
                return ArraySegment<EntityNoteItemData>.Empty;

            return await Context
                .Query(
                    r => new EntityNoteItemData(
                        Guid.Parse(r["Id"].ToString()!),
                        r.GetValue<DateTime>("Created"),
                        r.GetValue<string>("NoteValue")),
                    SQL.Entity.Commands.GetNotes,
                    new { xmlData = this.ConvertToXmlData(ids) });
        }

        public async Task<ICollection<EntityStatusItemData>> GetStatuses(ICollection<Guid> ids)
        {
            if (ids.IsNullOrEmpty())
                return ArraySegment<EntityStatusItemData>.Empty;

            var xmlData = $@"<id>{string.Join(@"</id><id>", ids)}</id>";
            return await Context
                 .Query(
                    r => new EntityStatusItemData(
                        Guid.Parse(r["Id"].ToString()!),
                        r.GetValue<DateTime>("Created"),
                        r.GetValue<string>("NoteValue"),
                        r.GetValue<string>("StatusValue")),
                    SQL.Entity.Commands.GetStatuses,
                    new { xmlData });
        }

        public Task SaveEntityChange(Guid id, string entityData, Guid userId) =>
            Context.Execute(SQL.Entity.Commands.SaveEntityChange, new { id, entityData, userId });

        public async Task<ICollection<Guid>> GetEntityIds(ICollection<Guid> ids, string entityType)
        {
            if (ids.IsNullOrEmpty())
                return ArraySegment<Guid>.Empty;

            return await Context
                 .Query(r =>  r.ParseGuid("Id").Value, SQL.Entity.Commands.GetEntityIds,
                    new { xmlData = this.ConvertToXmlData(ids), entityType });
        }
    }
}
