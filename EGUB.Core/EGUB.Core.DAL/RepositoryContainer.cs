﻿using EGUB.Core.Contracts;
using EGUB.Core.Contracts.DAL;

namespace EGUB.Core.DAL
{
    internal class RepositoryContainer : IRepositoryContainer
    {
        private readonly IContainer _container;
        private IContext _context;

        public RepositoryContainer(IContainer container)
        {
            _container = container;
        }

        public void Init(IContext context)
        {
            _context = context;
        }

        public TRepository GetRepository<TRepository>() where TRepository : IRepository
        {
            var repository = _container.GetItem<TRepository>();
            repository.Init(_context);
            return repository;
        }

    }
}
