﻿using EGUB.Core.Contracts;
using EGUB.Core.Contracts.DAL;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;

namespace EGUB.Core.DAL
{
    public abstract class BaseContextScript : BaseScript
    {
        private IContainer _container = default!;

        public void Init(IContainer container)
        {
            _container = container;
        }

        protected abstract string ContextName { get; }

        protected abstract Task Execute(IContext sourceContext, IContext context, IConfiguration configuration);

        public override Task Execute(IContext context, IConfiguration configuration)
        {
            if (string.IsNullOrEmpty(configuration.GetConnectionString(ContextName)))
                return Task.CompletedTask;

            return ExecuteWithContext(sourceContext => Execute(sourceContext, context, configuration));
        }

        private Task ExecuteWithContext(Func<IContext, Task> func)
        {
            var contextContainer = _container.GetItem<IContextContainer>();
            return contextContainer.ExecuteWithContext(ContextName, repositoryContainer =>
            {
                var repository = repositoryContainer.GetRepository<IContextRepository>();
                return func(repository.Context);
            });
        }
    }
}
