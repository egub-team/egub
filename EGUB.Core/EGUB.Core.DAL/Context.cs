﻿using EGUB.Core.Contracts.DAL;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace EGUB.Core.DAL
{
    internal class Context : IContext, ITransaction
    {
        private SqlTransaction _transaction;
        private readonly IConfiguration _configuration;
        private readonly string _name;

        public Context(string name, IConfiguration configuration)
        {
            _name = name;
            _configuration = configuration;
        }
        
        #region IDBTransaction
        public async Task Commit()
        {
            var connection = _transaction.Connection;
            await _transaction.CommitAsync();
            await connection.CloseAsync();
            SqlConnection.ClearPool(connection);
            connection.Dispose();
        }

        public void Dispose()
        {
            var connection = _transaction?.Connection;
            if (connection != null)
            {
                _transaction.Rollback();
                connection.Close();
                SqlConnection.ClearPool(connection);
                connection.Dispose();
            }
            _transaction?.Dispose();

            _transaction = null;
        }
        #endregion
        
        public async Task<ITransaction> BeginTransaction()
        {
            var connection = new SqlConnection(GetConnectionString());
            await connection.OpenAsync();
            _transaction = connection.BeginTransaction(IsolationLevel.Snapshot);

            return this;
        }

        public async Task CheckDataBase(bool isDropIfExists)
        {
            var builder = new SqlConnectionStringBuilder(GetConnectionString());
            var dataBaseName = builder.InitialCatalog;
            builder.InitialCatalog = "master";

            var dropIfExistsSql = string.Empty;
            if (isDropIfExists)
            {
                dropIfExistsSql = $@"
if (db_id(@dataBaseName) is not null)
begin
	alter database [{dataBaseName}] set single_user with rollback immediate;
	exec msdb.dbo.sp_delete_database_backuphistory @database_name = N'{dataBaseName}'
    drop database [{dataBaseName}];
end";
            }

            var sql = $@"
declare @dataBaseName nvarchar(255) = '{dataBaseName}'

{dropIfExistsSql}

if (db_id(@dataBaseName) is null)
begin
    create database {dataBaseName};
end

alter database {dataBaseName} set allow_snapshot_isolation on;";

            await using var connection = new SqlConnection(builder.ToString());
            await using var command = new SqlCommand(sql, connection);
            await connection.OpenAsync();
            await command.ExecuteNonQueryAsync();
        }

        public async Task Execute(string sql, object paramsObject = null, int? commandTimeout = default) =>
            await Execute(sql, this.GetParameters(paramsObject), commandTimeout);

        public async Task Execute(string sql, IDictionary<string, object> paramsDictionary, int? commandTimeout = default)
        {
            await using var command = await GetSqlCommand(sql);

            if (commandTimeout.HasValue)
                command.CommandTimeout = commandTimeout.Value;

            SetParameter(command.Parameters, paramsDictionary);
            await command.ExecuteNonQueryAsync();
        }

        public async Task<ICollection<T>> Query<T>(Func<IDataRow, T> mapFunc, string sql, object paramsObject = null, int? commandTimeout = default) =>
            await Query(mapFunc, sql, GetParameters(paramsObject), commandTimeout);

        public async Task<ICollection<T>> Query<T>(Func<IDataRow, T> mapFunc, string sql, IDictionary<string, object> paramsDictionary, int? commandTimeout = default)
        {
            var result = new List<T>();
            await using var command = await GetSqlCommand(sql);
            if (commandTimeout.HasValue)
                command.CommandTimeout = commandTimeout.Value;

            SetParameter(command.Parameters, paramsDictionary);
            await using var reader = await command.ExecuteReaderAsync(CommandBehavior.Default);
            while (await reader.ReadAsync())
                result.Add(mapFunc(new DataRowInfo(reader)));

            return result;
        }

        public string GetDataBaseName()
        {
            var builder = new SqlConnectionStringBuilder(GetConnectionString());
            return builder.InitialCatalog;
        }

        private async Task<SqlCommand> GetSqlCommand(string sql) 
        {
            if (_transaction != null)
                return new SqlCommand(sql, _transaction.Connection, _transaction);

            var connection = new SqlConnection(GetConnectionString());
            await connection.OpenAsync();

            var result = new SqlCommand(sql, connection);

            result.Disposed += (s, e) =>
            {
                connection.Close();
                SqlConnection.ClearPool(connection);
                connection.Dispose();
            };

            return result;
        }

        private string GetConnectionString() => 
            _configuration.GetConnectionString(_name);

        private IDictionary<string, object> GetParameters(object paramsObject)
        {
            var result = new Dictionary<string, object>();
            if (paramsObject == null)
                return result;

            foreach (var property in paramsObject.GetType().GetProperties())
            {
                result.Add(property.Name, property.GetValue(paramsObject));
            }

            return result;
        }

        private void SetParameter(SqlParameterCollection collection, IDictionary<string, object> paramsDictionary)
        {
            foreach (var item in paramsDictionary)
                collection.AddWithValue(item.Key, item.Value ?? DBNull.Value);
        }
    }
}
