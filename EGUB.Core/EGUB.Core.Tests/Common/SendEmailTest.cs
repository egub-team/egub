﻿using EGUB.Core.Contracts.Common.Email;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;

namespace EGUB.Core.Tests.Common
{
    [TestClass]
    public class SendEmailTest
    {
        [Ignore("Ckeck appsetings")]
        [TestMethod]
        public async Task TestSendEmail()
        {
            await Core.Common.ContainerFactory.Execute(async c =>
            {
                var service = c.GetItem<IEmailService>();
                var info = new EmailInfo
                {
                    From = "fleetproa@gmail.com",
                    To = "jeka_tg@ukr.net",
                    Subject = "TestSubject",
                    Html = "TestHtml"
                };
                await service.SendEmail(info);
            });
        }

        [Ignore("Ckeck appsetings")]
        [TestMethod]
        public async Task TestCleanSentMails()
        {
            await Core.Common.ContainerFactory.Execute(async c =>
            {
                var service = c.GetItem<IEmailService>();
                await service.CleanSentMails();
            });
        }

        [Ignore("Ckeck appsetings")]
        [TestMethod]
        public async Task TestExecuteWithClient()
        {
            await Core.Common.ContainerFactory.Execute(async c =>
            {
                var service = c.GetItem<IEmailService>();

                await service.ExecuteWithClient(async client =>
                {
                    var message = await client.Extract();

                    Console.WriteLine(message?.Id ?? string.Empty);

                    message = await client.Extract();

                    Console.WriteLine(message?.Id ?? string.Empty);
                });
            });
        }
    }
}
