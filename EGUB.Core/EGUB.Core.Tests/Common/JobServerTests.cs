﻿using Autofac;
using EGUB.Core.Common.Jobs;
using EGUB.Core.Contracts.Common.Jobs;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;

namespace EGUB.Core.Tests.Common
{
    [TestClass]
    public class JobServerTests
    {
        [Ignore]
        [TestMethod]
        public Task TestRunJob() =>
            Core.Common.ContainerFactory.Execute(
                b =>
                {
                    Core.Common.ContainerFactory.RegisterJobServer(b);
                    b.RegisterType<TestJob>().InstancePerLifetimeScope();
                },
                async container =>
                {
                    var jobServer = container.GetItem<IJobServer>();

                    var id = await jobServer.RunJob<TestJob>(1, "0/30 * * * * ?");

                    await Task.Delay(TimeSpan.FromMinutes(2));

                });

        public class TestJob : BaseJob
        {
            public TestJob(IConfiguration configuration) : base(configuration)
            {
            }

            protected override bool CanExecute(string message)
            {
                return true;
            }

            protected override Task Execute(string message, ILogger logger)
            {
                Console.WriteLine(message);
                return Task.CompletedTask;
            }
        }
    }
}
