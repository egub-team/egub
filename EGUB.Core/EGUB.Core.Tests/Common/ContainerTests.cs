using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;

namespace EGUB.Core.Tests.Common
{
    [TestClass]
    public class ContainerTests
    {
        [TestMethod]
        public Task TestGetItem() =>
            Core.Common.ContainerFactory.Execute(
                builder => 
                    builder
                        .RegisterType<GetItemTest>()
                        .As<IGetItemTest>()
                        .InstancePerLifetimeScope(),
                container =>
                {
                    Assert.IsFalse(container.IsRegistered<ContainerTests>());
                    Assert.IsTrue(container.IsRegistered<IGetItemTest>());
                    var getItemTest = container.GetItem<IGetItemTest>();
                    Assert.IsNotNull(getItemTest);
                    return Task.CompletedTask;
                });
    }

    public interface IGetItemTest
    {

    }

    public class GetItemTest : IGetItemTest
    {

    }
}

