﻿using Autofac;
using EGUB.Core.Contracts.DAL;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Threading.Tasks;
using EGUB.Core.DAL;
using Microsoft.Extensions.Configuration;

namespace EGUB.Core.Tests.DAL
{
    [TestClass]
    public class ContextTests
    {
        [TestMethod]
        public Task TestGetServerNow() =>
            Core.Common.ContainerFactory.Execute(
                builder =>
                {
                    ContainerFactory.RegisterContextContainer(builder);
                    ContainerFactory.RegisterRepositoryContainer(builder);

                    builder
                        .Register(c => new Migrator(nameof(ContextTests), true, c.Resolve<Contracts.IContainer>(), c.Resolve<IConfiguration>()))
                        .Named<IMigrator>(nameof(ContextTests))
                        .InstancePerLifetimeScope();
                    
                    builder
                        .RegisterType<TestRepository>()
                        .As<ITestRepository>();
                },
                async container =>
                {
                    var migrator = container.GetItem<IMigrator>(nameof(ContextTests));
                    await migrator.Execute();

                    var instance = container.GetItem<IContextContainer>();
                    await instance.ExecuteWithContext(nameof(ContextTests), async c =>
                    {
                        var repository = c.GetRepository<ITestRepository>();
                        var now = await repository.GetNowInfo();
                        Console.WriteLine($@"{now.ServerNow} - {now.LocalNow}");
                    });
                });
        
        private interface ITestRepository : IRepository
        {
            Task<(DateTime ServerNow, DateTime LocalNow)> GetNowInfo();
        }

        private class TestRepository : BaseRepository, ITestRepository
        {
            public async Task<(DateTime ServerNow, DateTime LocalNow)> GetNowInfo()
            {
                var sql = @"select getdate() as serverNow, @localNow as localNow";
                var items = await this.Context
                    .Query(
                        r => (ServerNow: Convert.ToDateTime(r["serverNow"]), LocalNow: Convert.ToDateTime(r["localNow"])),
                        sql, 
                        new {localNow = DateTime.Now});
                return items.Single();
            }
        }
    }

}
