﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using EGUB.Core.Contracts.DAL;
using EGUB.Core.DAL;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EGUB.Core.Tests.DAL
{
    [TestClass]
    public class EntityRepositoryTests
    {
        [TestMethod]
        public Task TestSaveEntity() =>
            Core.Common.ContainerFactory.Execute(
                builder =>
                {
                    ContainerFactory.RegisterContextContainer(builder);
                    ContainerFactory.RegisterRepositoryContainer(builder);
                    ContainerFactory.RegisterEntityRepository(builder);

                    builder
                        .Register(c => new Migrator(nameof(EntityRepositoryTests), true, c.Resolve<Contracts.IContainer>(), c.Resolve<IConfiguration>()))
                        .Named<IMigrator>(nameof(EntityRepositoryTests))
                        .InstancePerLifetimeScope();
                    
                    builder
                        .RegisterType<TestRepository>()
                        .As<ITestRepository>();
                },
                async container =>
                {
                    var migrator = container.GetItem<IMigrator>(nameof(EntityRepositoryTests));
                    await migrator.Execute();
                    
                    var contextContainer = container.GetItem<IContextContainer>();
                    await contextContainer.ExecuteWithContext(nameof(EntityRepositoryTests), async c =>
                    {
                        var testRepository = c.GetRepository<ITestRepository>();
                        var entityRepository = c.GetRepository<IEntityRepository>();
                        var id = Guid.NewGuid();
                        await entityRepository.SaveEntity(id, nameof(TestSaveEntity));

                        var row = (await testRepository
                                .Query(dr =>
                                (
                                    EntityType: dr.GetValue<string>("EntityType"),
                                    Created: dr.GetValue<DateTime>("Created"),
                                    LastModified: dr.GetValue<DateTime?>("LastModified")
                                ), "select * from [core].[Entity] where Id = @id", new {id}))
                            .Single();
                        
                        Assert.AreEqual(nameof(TestSaveEntity), row.EntityType);
                        Assert.AreNotEqual(DateTime.MinValue, row.Created);
                        Assert.IsNull(row.LastModified);

                        await entityRepository.SaveEntity(id, nameof(TestSaveEntity));
                        row = (await testRepository
                                .Query(dr =>
                                (
                                    EntityType: dr.GetValue<string>("EntityType"),
                                    Created: dr.GetValue<DateTime>("Created"),
                                    LastModified: dr.GetValue<DateTime?>("LastModified")
                                ), "select * from [core].[Entity] where Id = @id", new {id}))
                            .Single();

                        Assert.IsNotNull(row.LastModified);
                        Assert.AreNotEqual(DateTime.MinValue, row.LastModified);
                        var expectedNotes = new List<SaveEntityNoteItemData>
                        {
                            new SaveEntityNoteItemData("Test 1 Note", new DateTime(2021, 6, 6)),
                            new SaveEntityNoteItemData("Test 2 Note", new DateTime(2021, 6, 7))
                        };
                        await entityRepository.SaveEntityNotes(id, expectedNotes);
                        var notes = (await entityRepository.GetNotes(new[] {id}))
                            .ToList();
                        Assert.AreEqual(expectedNotes[0].Note, notes[0].NoteValue);
                        Assert.AreEqual(expectedNotes[0].Created, notes[0].Created);
                        Assert.AreEqual(expectedNotes[1].Note, notes[1].NoteValue);
                        Assert.AreEqual(expectedNotes[1].Created, notes[1].Created);
                        
                        expectedNotes.Add(new SaveEntityNoteItemData("Test 3 Note", new DateTime(2021, 7, 6)));                        
                        await entityRepository.SaveEntityNotes(id, expectedNotes);
                        notes = (await entityRepository.GetNotes(new[] {id}))
                            .ToList();
                        Assert.AreEqual(expectedNotes[0].Note, notes[0].NoteValue);
                        Assert.AreEqual(expectedNotes[0].Created, notes[0].Created);
                        Assert.AreEqual(expectedNotes[1].Note, notes[1].NoteValue);
                        Assert.AreEqual(expectedNotes[1].Created, notes[1].Created);
                        Assert.AreEqual(expectedNotes[2].Note, notes[2].NoteValue);
                        Assert.AreEqual(expectedNotes[2].Created, notes[2].Created);
                    });
                }
            );
        
        private interface ITestRepository : IRepository
        {
            Task<ICollection<T>> Query<T>(Func<IDataRow, T> mapFunc, string sql, object paramsObject = null);
        }

        private class TestRepository : BaseRepository, ITestRepository
        {
            public Task<ICollection<T>> Query<T>(Func<IDataRow, T> mapFunc, string sql, object paramsObject = null) =>
                Context.Query(mapFunc, sql, paramsObject);
        }        
    }
}