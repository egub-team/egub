﻿using EGUB.Core.Common.Logging;
using EGUB.Core.Contracts.Common.Jobs;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Threading.Tasks;

namespace EGUB.Core.Common.Jobs
{
    public abstract class BaseJob : IJob
    {
        protected BaseJob(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        protected IConfiguration Configuration { get; }

        public async Task Execute(string message)
        {
            if (!CanExecute(message))
                return;

            var path = Path.Combine(Configuration["LogsPath"] ?? $@"{Directory.GetCurrentDirectory()}\Logs", GetType().Name);

            using var factory = LoggerFactory
                .Create(b => b.AddProvider(new FileLoggerProvider(path)));

            var logger = factory.CreateLogger(GetType().Name);

            try
            {
                logger.LogInformation("Start execution");
                await Execute(message, logger);
                logger.LogInformation("Finish execution");
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Error execution");
            }
        }

        protected abstract bool CanExecute(string message);

        protected abstract Task Execute(string message, ILogger logger);
    }
}
