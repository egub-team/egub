﻿using EGUB.Core.Contracts.Common.Jobs;
using Hangfire;
using Hangfire.Annotations;
using Hangfire.States;
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;

namespace EGUB.Core.Common.Jobs
{
    internal class JobServer : IJobServer
    {
        private readonly ConcurrentDictionary<string, BackgroundJobServer> _items = new ConcurrentDictionary<string, BackgroundJobServer>();
        private readonly JobActivator _jobActivator;

        public JobServer(JobActivator jobActivator)
        {
            _jobActivator = jobActivator;
        }

        public ValueTask DisposeAsync()
        {
            foreach (var item in _items.Values.ToList())
                item.Dispose();

            _items.Clear();

            return default;
        }

        public Task RemoveJob(string jobId)
        {
            return Task.Run(() => RecurringJob.RemoveIfExists(jobId));
        }

        public Task<string> RunJob<TJob>(int workerCount, string cronExpression)
            where TJob : IJob
        {
            return Task.Run(() => RunJobInternal<TJob>(workerCount, cronExpression));
        }

        public Task<string> Enqueue<TJob>(string message) where TJob : IJob
        {
            return Task.Run(() => EnqueueInternal<TJob>(message));
        }

        private string EnqueueInternal<TJob>(string message) where TJob : IJob
        {
            var name = typeof(TJob).Name.ToLowerInvariant();

            var queueName = $"{name}_queue";

            _items.GetOrAdd(name, k => GetItem(queueName, 1));

            var hangFireClient = new BackgroundJobClient();
            var queueState = new EnqueuedState(queueName);

            return hangFireClient.Create<TJob>(j => j.Execute(message), queueState);
        }

        private string RunJobInternal<TJob>(int workerCount, string cronExpression)
            where TJob : IJob
        {
            var name = typeof(TJob).Name.ToLowerInvariant();

            var queueName = $"{name}_queue";

            _items.GetOrAdd(name, k => GetItem(queueName, workerCount));

            var jobId = $"{name}_jobId";

            var options = new RecurringJobOptions
            {
                TimeZone = TimeZoneInfo.Local,
            };
            RecurringJob.AddOrUpdate<TJob>(jobId, queueName, job => job.Execute(jobId), cronExpression, options);

            return jobId;
        }

        private BackgroundJobServer GetItem(string name, int workerCount)
        {
            return new BackgroundJobServer(
                new BackgroundJobServerOptions
                {
                    ServerName = $"{name}_server",
                    WorkerCount = workerCount,
                    Activator = _jobActivator,
                    Queues = [name],
                    TimeZoneResolver = new LocalTimeZoneResolver(),
                    SchedulePollingInterval = TimeSpan.FromSeconds(1),
                    HeartbeatInterval = TimeSpan.FromSeconds(1),
                    ServerCheckInterval = TimeSpan.FromSeconds(1),
                    IsLightweightServer = false,
                }, JobStorage.Current);
        }

        private class LocalTimeZoneResolver : ITimeZoneResolver
        {
            public TimeZoneInfo GetTimeZoneById([NotNull] string timeZoneId)
            {
                return TimeZoneInfo.Local;
            }
        }
    }
}
