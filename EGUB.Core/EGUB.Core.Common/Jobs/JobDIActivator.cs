﻿using Autofac;
using Hangfire;
using Hangfire.Server;
using System;

namespace EGUB.Core.Common.Jobs
{
    internal class JobDIActivator : JobActivator
    {
        private readonly ILifetimeScope _lifetimeScope;

        public JobDIActivator(ILifetimeScope lifetimeScope)
        {
            _lifetimeScope = lifetimeScope;
        }

        public override JobActivatorScope BeginScope(PerformContext context)
        {
            return new JobDIActivatorScope(_lifetimeScope.BeginLifetimeScope());
        }

        private class JobDIActivatorScope : JobActivatorScope
        {
            private readonly ILifetimeScope _scope;

            public JobDIActivatorScope(ILifetimeScope scope)
            {
                _scope = scope;
            }

            public override object Resolve(Type type)
            {
                return _scope.Resolve(type);
            }
        }
    }
}
