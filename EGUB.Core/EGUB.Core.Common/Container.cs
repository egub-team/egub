﻿using Autofac;
using EGUB.Core.Helpers;

namespace EGUB.Core.Common
{
    internal class Container : Contracts.IContainer
    {
        private readonly ILifetimeScope _scope;
        public Container(ILifetimeScope scope)
        {
            _scope = scope;
        }

        public T GetItem<T>(string name = null) => 
            name.IsNullOrEmpty() ? _scope.Resolve<T>() : _scope.ResolveNamed<T>(name!);

        public bool IsRegistered<T>(string name = null) =>
            name.IsNullOrEmpty() ? _scope.IsRegistered<T>() : _scope.IsRegisteredWithName<T>(name!);
    }
}
