﻿using EGUB.Core.Contracts.Common.Email;
using MailKit;
using MailKit.Net.Imap;
using MailKit.Net.Pop3;
using MailKit.Net.Smtp;
using MailKit.Search;
using MailKit.Security;
using Microsoft.Extensions.Configuration;
using MimeKit;
using MimeKit.Text;
using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace EGUB.Core.Common.Email
{
    internal class EmailService : IEmailService
    {
        private readonly IConfiguration _configuration;

        public EmailService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task CleanSentMails()
        {
            var settings = _configuration.GetSection("EmailSettings");
            using var imap = new ImapClient();
            await imap.ConnectAsync(
                settings["ImapHost"],
                int.Parse(settings["ImapPort"]),
                Enum.TryParse<SecureSocketOptions>(settings["ImapSecureSocketOptions"], true, out var imapOptions)
                    ? imapOptions
                    : SecureSocketOptions.Auto);
            await imap.AuthenticateAsync(settings["UserName"], settings["Password"]);

            var sentFolder = imap.GetFolder(SpecialFolder.Sent);
            await sentFolder.OpenAsync(FolderAccess.ReadWrite);

            var ids = await sentFolder.SearchAsync(SearchQuery.All);
            if (ids.Count > 0)
            {
                foreach (var uniqueId in ids)
                {
                    await sentFolder.AddFlagsAsync(uniqueId, MessageFlags.Deleted, true);
                }
                await sentFolder.ExpungeAsync();
            }

            await imap.DisconnectAsync(true);
        }

        public async Task SendEmail(EmailInfo info)
        {
            // create message
            var email = new MimeMessage();
            email.From.Add(MailboxAddress.Parse(info.From));
            foreach (var toItem in info.To)
                email.To.Add(MailboxAddress.Parse(toItem));

            if ((info.Cc?.Length ?? 0) > 0) 
                foreach (var ccItem in info.Cc)
                    email.Cc.Add(MailboxAddress.Parse(ccItem));

            email.Subject = info.Subject;
            email.Body = new TextPart(TextFormat.Html) { Text = info.Html };

            // send email
            using var smtp = new SmtpClient();

            var settings = _configuration.GetSection("EmailSettings");

            await smtp.ConnectAsync(
                settings["SmtpHost"],
                int.Parse(settings["SmtpPort"]),
                Enum.TryParse<SecureSocketOptions>(settings["SmtpSecureSocketOptions"], true, out var smtpOptions)
                    ? smtpOptions
                    : SecureSocketOptions.Auto);
            await smtp.AuthenticateAsync(settings["UserName"], settings["Password"]);
            await smtp.SendAsync(email);
            await smtp.DisconnectAsync(true);
        }

        public async Task ExecuteWithClient(Func<IEmailClient, Task> func)
        {
            using var client = await GetClient();
            var count = await client.GetMessageCountAsync();
            var emailClient = new EmailClient(client, count);
            await func(emailClient);
            await client.DisconnectAsync(true);
        }

        private async Task<Pop3Client> GetClient()
        {
            var settings = _configuration.GetSection("EmailSettings");

            var userName = settings["UserName"];
            var password = settings["Password"];

            var popHost = settings["PopHost"];
            var popPort = int.Parse(settings["PopPort"]);

            var result = new Pop3Client();

            await result.ConnectAsync(
                popHost,
                popPort,
                Enum.TryParse<SecureSocketOptions>(settings["PopSecureSocketOptions"], true, out var options)
                    ? options
                    : SecureSocketOptions.Auto);

            await result.AuthenticateAsync(userName, password);

            return result;
        }

        private class EmailClient : IEmailClient
        {
            private readonly Pop3Client _client;
            private readonly int _count;
            private int _index = 0;

            private static readonly Regex inlineTagRegex = new Regex("<\\/?(a|span|sub|sup|b|i|strong|small|big|em|label|q)[^>]*>", RegexOptions.Compiled | RegexOptions.Singleline);
            private static readonly Regex scriptRegex = new Regex("<(script|style)[^>]*?>.*?</\\1>", RegexOptions.Compiled | RegexOptions.Singleline);
            private static readonly Regex tagRegex = new Regex("<[^>]+>", RegexOptions.Compiled | RegexOptions.Singleline);
            private static readonly Regex multiWhitespaceRegex = new Regex("\\s+", RegexOptions.Compiled | RegexOptions.Singleline);

            public EmailClient(
                Pop3Client client,
                int count)
            {
                _client = client;
                _count = count;
            }

            public async Task<ClientEmailInfo> Extract()
            {
                if (_index >= _count)
                    return default;

                var message = await _client.GetMessageAsync(_index);

                var result = new ClientEmailInfo
                {
                    Id = message.MessageId,
                    Subject = message.Subject,
                    SentTime = message.Date.LocalDateTime,
                    From = message.From?
                        .Cast<MailboxAddress>()
                        .FirstOrDefault()?
                        .Address,
                    To = message.To?
                        .Cast<MailboxAddress>()
                        .FirstOrDefault()?
                        .Address,
                    Text = GetTextPlain(message)
                };

                await _client.DeleteMessageAsync(_index);

                _index++;
                return result;
            }

            private string GetTextPlain(MimeMessage message)
            {
                var result = message.GetTextBody(TextFormat.Text);
                if (!string.IsNullOrEmpty(result))
                    return result;

                result = message.HtmlBody;
                if (string.IsNullOrEmpty(result))
                {
                    return result;
                }

                result = scriptRegex.Replace(result, string.Empty);
                result = inlineTagRegex.Replace(result, string.Empty);
                result = tagRegex.Replace(result, " ");
                result = HttpUtility.HtmlDecode(result);
                result = multiWhitespaceRegex.Replace(result, " ");

                return result.Trim();
            }
        }
    }
}
