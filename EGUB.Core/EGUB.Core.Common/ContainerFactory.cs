﻿using Autofac;
using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using IContainer = EGUB.Core.Contracts.IContainer;
using EGUB.Core.Common.Email;
using EGUB.Core.Contracts.Common.Email;
using EGUB.Core.Common.Jobs;
using Hangfire;
using Microsoft.AspNetCore.Builder;
using EGUB.Core.Contracts.Common.Jobs;
using Microsoft.Extensions.DependencyInjection;
using Hangfire.InMemory;

namespace EGUB.Core.Common
{
    public class ContainerFactory : Module
    {
        public static void RegisterContainer(ContainerBuilder builder)
        {
            builder
                .RegisterType<Container>()
                .As<IContainer>()
                .InstancePerLifetimeScope();
            builder
                .RegisterType<EmailService>()
                .As<IEmailService>()
                .InstancePerLifetimeScope();

            IConfiguration configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json",
                    optional: false,
                    reloadOnChange: true)
                .Build();
            builder
                .RegisterInstance(configuration)
                .SingleInstance();
        }

        public static void RegisterJobServer(ContainerBuilder builder)
        {
            GlobalConfiguration.Configuration
                .SetDataCompatibilityLevel(CompatibilityLevel.Version_180)
                .UseStorage(new InMemoryStorage(
                    new InMemoryStorageOptions
                    {
                        MaxExpirationTime = TimeSpan.FromHours(2),
                    }));

            builder.RegisterType<JobServer>().As<IJobServer>().InstancePerLifetimeScope();
            builder.RegisterType<JobDIActivator>().As<JobActivator>().InstancePerLifetimeScope();
        }

        public static Action<IApplicationBuilder> ConfigureJobDashboard(IServiceCollection services)
        {
            services.AddHangfire(c => c
                .SetDataCompatibilityLevel(CompatibilityLevel.Version_180)
                .UseSimpleAssemblyNameTypeSerializer()
                .UseRecommendedSerializerSettings());

            return app => app.UseHangfireDashboard("/hangfire", new DashboardOptions { Authorization = new[] { new DashboardAuthorizationFilter() } });
        }

        public static Task Execute(Func<IContainer, Task> taskFunc) =>
            Execute(null, taskFunc);
        
        public static async Task Execute(Action<ContainerBuilder> buildAction, Func<IContainer, Task> taskFunc)
        {
            var builder = new ContainerBuilder();
            RegisterContainer(builder);
            buildAction?.Invoke(builder);
            await using var scope = builder
                .Build()
                .BeginLifetimeScope();
            var container = scope.Resolve<IContainer>();
            await taskFunc(container);
        }        
        
        protected override void Load(ContainerBuilder builder)
        {
            RegisterContainer(builder);
            RegisterJobServer(builder);
        }
    }
}
