﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.Core.Common.Logging
{
    internal class FileLogger : ILogger, IDisposable
    {
        private readonly string _path;
        private readonly object _lock = new();

        public FileLogger(string path)
        {
            _path = path;
        }

        public IDisposable BeginScope<TState>(TState state)
        {
            return this;
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            return true;
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            if (!Directory.Exists(_path))
                Directory.CreateDirectory(_path);

            string fileName = Path.Combine(_path, $"{DateTime.Now:yyyy-MM-dd}.log");
            var messageBuilder = new StringBuilder();
            messageBuilder.AppendLine($"{logLevel.ToString()}: {DateTime.Now:o} - {formatter(state, exception)}");

            if (exception != null)
            {
                messageBuilder
                    .AppendLine($"{exception.GetType()}: {exception.Message}")
                    .AppendLine(exception.StackTrace);
            }

            lock (_lock)
            {
                try
                {
                    File.AppendAllText(fileName, messageBuilder.ToString());
                }
                catch
                {
                    // ignored
                }
            }
        }

        public void Dispose()
        {
        }
    }
}
