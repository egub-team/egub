﻿using System;

namespace EGUB.Core.Contracts.BLL
{
    public interface IRemoveEntityContext : IOperationContext
    {
        Guid Id { get; }
    }
}
