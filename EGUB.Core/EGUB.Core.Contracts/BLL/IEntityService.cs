﻿using EGUB.Core.Contracts.DAL;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EGUB.Core.Contracts.BLL
{
    public interface IEntityService<TEntity>
        where TEntity : IEntity
    {
        Task Save(ISaveEntityContext<TEntity> context);
        Task Save(IRepositoryContainer container, ISaveEntityContext<TEntity> context);
        Task<ICollection<TEntity>> GetEntities(IGetEntitiesContext context);
        Task<ICollection<TEntity>> GetEntities(IRepositoryContainer container, IGetEntitiesContext context);
        Task Remove(IRemoveEntityContext context);
        Task Remove(IRepositoryContainer container, IRemoveEntityContext context);
    }

    public interface IEntityService<TStatusValue, TEntity> : IEntityService<TEntity>
        where TStatusValue : struct, IConvertible
        where TEntity : IEntity<TStatusValue>
    {
    }
}
