﻿namespace EGUB.Core.Contracts.BLL
{
    public interface ISaveEntityContext<out TEntity> : IOperationContext
        where TEntity : IEntity
    {
        TEntity Entity { get; }
    }
}
