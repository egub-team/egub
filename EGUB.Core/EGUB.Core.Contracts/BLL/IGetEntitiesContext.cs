﻿using System;
using System.Collections.Generic;

namespace EGUB.Core.Contracts.BLL
{
    public interface IGetEntitiesContext : IOperationContext
    {
        ICollection<Guid> Ids { get; }
    }
}
