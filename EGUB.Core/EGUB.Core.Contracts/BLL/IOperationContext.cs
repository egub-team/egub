﻿using System;

namespace EGUB.Core.Contracts.BLL
{
    public interface IOperationContext
    {
        Guid GetCurrentUserId();
    }
}
