﻿using System;
using System.Collections.Generic;

namespace EGUB.Core.Contracts
{
    public interface IEntity
    {
        public Guid Id { get; }

        public ICollection<NoteInfo> Notes { get; }
    }

    public interface IEntity<TStatusValue> : IEntity
        where TStatusValue : struct, IConvertible
    {
        public StatusInfo<TStatusValue> StatusInfo { get; }
    }
}
