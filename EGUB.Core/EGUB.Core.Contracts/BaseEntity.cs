﻿using System;
using System.Collections.Generic;

namespace EGUB.Core.Contracts
{
    public abstract class BaseEntity : IEntity
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public abstract string EntityType { get; }
        public ICollection<NoteInfo> Notes { get; set; }
    }

    public abstract class BaseEntity<TStatusValue> : BaseEntity, IEntity<TStatusValue>
        where TStatusValue : struct, IConvertible
    {
        public StatusInfo<TStatusValue> StatusInfo { get; set; }
    }
}
