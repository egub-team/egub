﻿using System;

namespace EGUB.Core.Contracts
{
    public class NoteInfo
    {
        public DateTime Created { get; set; }
        public string Note { get; set; }
    }
}
