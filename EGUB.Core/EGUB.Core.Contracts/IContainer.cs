﻿namespace EGUB.Core.Contracts
{
    public interface IContainer
    {
        bool IsRegistered<TItem>(string name = default);
        TItem GetItem<TItem>(string name = default);
    }
}
