﻿using System.Threading.Tasks;

namespace EGUB.Core.Contracts.Common.Jobs
{
    public interface IJob
    {
        Task Execute(string message);
    }
}
