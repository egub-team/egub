﻿using System;
using System.Threading.Tasks;

namespace EGUB.Core.Contracts.Common.Jobs
{
    public interface IJobServer : IAsyncDisposable
    {
        Task<string> RunJob<TJob>(int workerCount, string cronExpression)
            where TJob : IJob;

        Task RemoveJob(string jobId);

        Task<string> Enqueue<TJob>(string message)
            where TJob : IJob;
    }
}
