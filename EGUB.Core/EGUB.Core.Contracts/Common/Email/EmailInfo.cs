﻿namespace EGUB.Core.Contracts.Common.Email
{
    public class EmailInfo
    {
        public string From { get; set; }
        public string[] To { get; set; }
        public string[] Cc { get; set; }
        public string Subject { get; set; }
        public string Html { get; set; }
    }
}
