﻿using System.Threading.Tasks;

namespace EGUB.Core.Contracts.Common.Email
{
    public interface IEmailClient
    {
        Task<ClientEmailInfo> Extract();
    }
}
