﻿using System;

namespace EGUB.Core.Contracts.Common.Email
{
    public class ClientEmailInfo
    {
        public string Id { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string Subject { get; set; }
        public string Text { get; set; }
        public DateTime SentTime { get; set; }
    }
}
