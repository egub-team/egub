﻿using System;
using System.Threading.Tasks;

namespace EGUB.Core.Contracts.Common.Email
{
    public interface IEmailService
    {
        Task SendEmail(EmailInfo info);
        Task CleanSentMails();
        Task ExecuteWithClient(Func<IEmailClient, Task> func);
    }
}
