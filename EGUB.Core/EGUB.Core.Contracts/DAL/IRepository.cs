﻿namespace EGUB.Core.Contracts.DAL
{
    public interface IRepository
    {
        void Init(IContext context);
    }
}
