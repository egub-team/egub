﻿using System;
using System.Threading.Tasks;

namespace EGUB.Core.Contracts.DAL
{
    public interface IContextContainer
    {
        Task ExecuteWithContext(string name, Func<IRepositoryContainer, Task> task, bool hasTransaction = true);
    }
}
