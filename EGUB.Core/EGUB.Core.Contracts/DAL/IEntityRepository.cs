﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EGUB.Core.Contracts.DAL
{
    public interface IEntityRepository : IRepository
    {
        Task SaveEntity(Guid id, string entityType);
        Task SaveEntityNotes(Guid id, ICollection<SaveEntityNoteItemData> notes);
        Task SaveEntityStatus(Guid id, string noteValue, string statusValue);
        Task SaveEntityChange(Guid id, string entityData, Guid userId);
        Task Delete(Guid id);
        Task<ICollection<EntityNoteItemData>> GetNotes(ICollection<Guid> ids);
        Task<ICollection<EntityStatusItemData>> GetStatuses(ICollection<Guid> ids);
        Task<ICollection<Guid>> GetEntityIds(ICollection<Guid> ids, string entityType);
    }

    public record struct SaveEntityNoteItemData(string Note, DateTime Created);

    public record struct EntityNoteItemData(Guid Id, DateTime Created, string NoteValue);

    public record struct EntityStatusItemData(Guid Id, DateTime Created, string NoteValue, string StatusValue);
}
