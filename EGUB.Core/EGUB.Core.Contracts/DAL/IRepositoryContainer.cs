﻿namespace EGUB.Core.Contracts.DAL
{
    public interface IRepositoryContainer
    {
        void Init(IContext context);

        TRepository GetRepository<TRepository>()
            where TRepository : IRepository;
    }
}
