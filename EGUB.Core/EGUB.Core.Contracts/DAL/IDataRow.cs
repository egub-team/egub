﻿using System;

namespace EGUB.Core.Contracts.DAL
{
    public interface IDataRow
    {
        object this[string name] { get; }

        T GetValue<T>(string name);

        Guid? ParseGuid(string name);

        DateOnly? ParseDateOnly(string name);
    }
}
