﻿using System.Threading.Tasks;

namespace EGUB.Core.Contracts.DAL
{
    public interface IMigrator
    {
        Task Execute();
    }
}
