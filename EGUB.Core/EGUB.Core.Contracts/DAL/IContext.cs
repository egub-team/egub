﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EGUB.Core.Contracts.DAL
{
    public interface IContext : IDisposable
    {
        string GetDataBaseName();
        Task Execute(string sql, object paramsObject = null, int? commandTimeout = default);
        Task Execute(string sql, IDictionary<string, object> paramsDictionary, int? commandTimeout = default);
        Task<ICollection<T>> Query<T>(Func<IDataRow, T> mapFunc, string sql, object paramsObject = null, int? commandTimeout = default);
        Task<ICollection<T>> Query<T>(Func<IDataRow, T> mapFunc, string sql, IDictionary<string, object> paramsDictionary, int? commandTimeout = default);
    }
}
