﻿using System;

namespace EGUB.Core.Contracts
{
    public class StatusInfo<TValue> : NoteInfo
        where TValue : struct, IConvertible
    {
        public TValue Status { get; set; }
    }
}
