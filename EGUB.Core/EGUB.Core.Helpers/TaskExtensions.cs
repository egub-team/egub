﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EGUB.Core.Helpers
{
    public static class TaskExtensions
    {
        public static async Task<ICollection<TItem>> SelectAsItemsAsync<T, TItem>(this Task<ICollection<T>> itemsTask, Func<T, TItem> selectorFunc)
        {
            var items = await itemsTask;
            return items
                .Select(selectorFunc)
                .ToList();
        }

        public static async Task<TItem> SelectAsItemAsync<T, TItem>(this Task<T> itemTask, Func<T, TItem> selectorFunc)
        {
            var item = await itemTask;
            return selectorFunc(item);
        }
    }
}
