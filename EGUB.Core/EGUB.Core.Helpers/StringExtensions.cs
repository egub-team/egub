﻿namespace EGUB.Core.Helpers
{
    public static class StringExtensions
    {
        public static bool IsNullOrEmpty(this string source) => string.IsNullOrEmpty(source);
    }
}