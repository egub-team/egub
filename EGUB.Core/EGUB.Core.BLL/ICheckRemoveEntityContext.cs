﻿using EGUB.Core.Contracts;
using EGUB.Core.Contracts.BLL;

namespace EGUB.Core.BLL
{
    public interface ICheckRemoveEntityContext<out TEntity> : IOperationContext
        where TEntity : IEntity
    {
        TEntity Entity { get; }
    }
}
