﻿using EGUB.Core.Contracts.BLL;
using System;
using System.Collections.Generic;

namespace EGUB.Core.BLL
{
    public class GetEntitiesContext : IGetEntitiesContext
    {
        private readonly IOperationContext _context;
        public GetEntitiesContext(IOperationContext context, ICollection<Guid> ids)
        {
            _context = context;
            Ids = ids;
        }

        public ICollection<Guid> Ids { get; }

        public Guid GetCurrentUserId() => _context.GetCurrentUserId();
    }
}
