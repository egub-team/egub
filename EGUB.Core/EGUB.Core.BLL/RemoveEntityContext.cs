﻿using EGUB.Core.Contracts.BLL;
using System;

namespace EGUB.Core.BLL
{
    public class RemoveEntityContext : IRemoveEntityContext
    {
        private readonly IOperationContext _context;

        public RemoveEntityContext(IOperationContext context, Guid id)
        {
            _context = context;
            Id = id;
        }

        public Guid Id { get; }

        public Guid GetCurrentUserId() => _context.GetCurrentUserId();
    }
}
