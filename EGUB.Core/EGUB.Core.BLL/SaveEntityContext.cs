﻿using EGUB.Core.Contracts;
using EGUB.Core.Contracts.BLL;
using System;

namespace EGUB.Core.BLL
{
    public class SaveEntityContext<TEntity> : ISaveEntityContext<TEntity>
        where TEntity : IEntity
    {
        private readonly IOperationContext _context;

        public SaveEntityContext(IOperationContext context, TEntity entity)
        {
            _context = context;
            Entity = entity;
        }

        public TEntity Entity { get; }

        public Guid GetCurrentUserId() => _context.GetCurrentUserId();
    }
}
