﻿using EGUB.Core.Contracts;
using EGUB.Core.Contracts.BLL;
using System.Collections.Generic;

namespace EGUB.Core.BLL
{
    public interface IInitEntitiesContext<TEntity> : IOperationContext
        where TEntity : IEntity
    {
        ICollection<TEntity> Entities { get; }
    }
}
