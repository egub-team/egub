﻿using EGUB.Core.Contracts;
using EGUB.Core.Contracts.BLL;
using System;
using System.Collections.Generic;

namespace EGUB.Core.BLL
{
    internal class InitEntitiesContext<TEntity> : IInitEntitiesContext<TEntity>
        where TEntity : IEntity
    {
        private readonly IOperationContext _context;

        public InitEntitiesContext(IOperationContext context, ICollection<TEntity> entities)
        {
            _context = context;
            Entities = entities;
        }
        
        public ICollection<TEntity> Entities { get; }

        public Guid GetCurrentUserId() => _context.GetCurrentUserId();
    }
}
