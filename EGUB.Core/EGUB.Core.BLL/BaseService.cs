﻿using EGUB.Core.Contracts.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.Core.BLL
{
    public abstract class BaseService
    {
        private readonly IContextContainer _contextContainer;

        protected BaseService(IContextContainer contextContainer)
        {
            _contextContainer = contextContainer;
        }

        protected abstract string ContextName { get; }

        protected Task Execute(Func<IRepositoryContainer, Task> task) =>
            _contextContainer.ExecuteWithContext(ContextName, task);

        protected async Task<T> Get<T>(Func<IRepositoryContainer, Task<T>> func)
        {
            var result = default(T);
            await Execute(async c => result = await func(c));
            return result;
        }

        protected Task ExecuteWithRepository<TRepository>(Func<TRepository, Task> task)
            where TRepository : IRepository
        {
            return Execute(c =>
            {
                var repository = c.GetRepository<TRepository>();
                return task(repository);
            });
        }

        protected async Task<T> GetWithRepository<T, TRepository>(Func<TRepository, Task<T>> func)
            where TRepository : IRepository
        {
            var result = default(T);
            await ExecuteWithRepository<TRepository>(async r => result = await func(r));
            return result;
        }
    }
}
