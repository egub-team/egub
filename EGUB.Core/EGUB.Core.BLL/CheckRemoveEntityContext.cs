﻿using EGUB.Core.Contracts;
using EGUB.Core.Contracts.BLL;
using System;

namespace EGUB.Core.BLL
{
    internal class CheckRemoveEntityContext<TEntity> : ICheckRemoveEntityContext<TEntity>
        where TEntity : IEntity
    {
        private readonly IOperationContext _context;

        public CheckRemoveEntityContext(IOperationContext context, TEntity entity)
        {
            _context = context;
            Entity = entity;
        }

        public TEntity Entity { get; }

        public Guid GetCurrentUserId() => _context.GetCurrentUserId();
    }
}
