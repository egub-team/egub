﻿using EGUB.Core.Contracts;
using EGUB.Core.Contracts.BLL;
using EGUB.Core.Contracts.DAL;
using EGUB.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EGUB.Core.BLL
{
    public abstract class BaseEntityService<TEntity> : BaseService, IEntityService<TEntity>
        where TEntity : BaseEntity, new()
    {
        protected BaseEntityService(IContextContainer contextContainer) : base(contextContainer)
        {
        }

        public async Task<ICollection<TEntity>> GetEntities(IGetEntitiesContext context)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));
            if (!context.Ids.Any())
                return ArraySegment<TEntity>.Empty;

            var result = default(ICollection<TEntity>);
            await Execute(async container => result = await GetEntities(container, context));

            return result;
        }

        public async Task<ICollection<TEntity>> GetEntities(IRepositoryContainer container, IGetEntitiesContext context)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));
            if (context.Ids.IsNullOrEmpty())
            {
                return ArraySegment<TEntity>.Empty;
            }

            var repository = container.GetRepository<IEntityRepository>();

            ICollection<Guid> entityIds = await repository.GetEntityIds(context.Ids, new TEntity().EntityType);

            if (entityIds.IsNullOrEmpty())
                return ArraySegment<TEntity>.Empty;

            var initContext = new InitEntitiesContext<TEntity>(
                context,
                entityIds
                    .Select(i =>
                        new TEntity
                        {
                            Id = i
                        })
                    .ToList());

            await InitEntities(container, initContext);
            return initContext.Entities;
        }

        public Task Save(ISaveEntityContext<TEntity> context) => 
            Execute(container => Save(container, context));

        public async Task Save(IRepositoryContainer container, ISaveEntityContext<TEntity> context)
        {
            await ValidateEntity(container, context);
            await SaveEntity(container, context);
            var entityData = Newtonsoft.Json.JsonConvert.SerializeObject(context.Entity);
            var repository = container.GetRepository<IEntityRepository>();
            await repository.SaveEntityChange(context.Entity.Id, entityData, context.GetCurrentUserId());
        }

        public Task Remove(IRemoveEntityContext context) =>
            Execute(container => Remove(container, context));

        public async Task Remove(IRepositoryContainer container, IRemoveEntityContext context)
        {
            var getEntitiesContext = new GetEntitiesContext(context, new[] { context.Id });
            var entities = await GetEntities(container, getEntitiesContext);
            var entity = entities.SingleOrDefault();
            if (entity == null)
                return;
            var checkEntityContext = new CheckRemoveEntityContext<TEntity>(context, entity);
            await CheckRemove(container, checkEntityContext);
            var repository = container.GetRepository<IEntityRepository>();
            await repository.Delete(entity.Id);
            await repository.SaveEntityChange(entity.Id, default, context.GetCurrentUserId());
        }

        protected void AddNote(TEntity entity, string note)
        {
            if (entity.Notes.IsNullOrEmpty())
            {
                entity.Notes ??= new List<NoteInfo>();
                entity.Notes.Add(new NoteInfo { Note = note, Created = DateTime.Now });
                return;
            }
            
            var maxCreated = entity.Notes.Max(i => i.Created);
            var lastNote = entity.Notes.Single(i => i.Created == maxCreated);
            if ((lastNote.Note ?? string.Empty) == (note ?? string.Empty))
                return;
            entity.Notes.Add(new NoteInfo { Note = note, Created = DateTime.Now });
        }

        protected virtual async Task InitEntities(IRepositoryContainer container, IInitEntitiesContext<TEntity> context)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));
            if (context.Entities == null)
                throw new ArgumentNullException(nameof(context.Entities));
            var ids = context.Entities
                .Select(i => i.Id)
                .ToArray();
            if (!ids.Any())
                return;

            var repository = container.GetRepository<IEntityRepository>();
            var notes = (await repository.GetNotes(ids)).ToLookup(i => i.Id);
            foreach (var entity in context.Entities)
            {
                var entityNotes = notes[entity.Id];
                entity.Notes = entityNotes
                    .Select(i => new NoteInfo { Note = i.NoteValue, Created = i.Created })
                    .ToList();
            }
        }

        protected virtual Task ValidateEntity(IRepositoryContainer container, ISaveEntityContext<TEntity> context)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));
            if (context.Entity == null)
                throw new ArgumentNullException(nameof(context.Entity));
            if (context.Entity.Id == Guid.Empty)
                throw new ArgumentNullException(nameof(context.Entity.Id));
            return Task.CompletedTask;
        }

        protected virtual async Task SaveEntity(IRepositoryContainer container, ISaveEntityContext<TEntity> context)
        {
            var repository = container
                .GetRepository<IEntityRepository>();
            await repository
                .SaveEntity(context.Entity.Id, context.Entity.EntityType);

            var notes = (context.Entity.Notes ?? Enumerable.Empty<NoteInfo>()) 
                .Select(i => new SaveEntityNoteItemData(i.Note, i.Created))
                .ToList();
            await repository
                .SaveEntityNotes(context.Entity.Id, notes);
        }

        protected virtual Task CheckRemove(IRepositoryContainer container, ICheckRemoveEntityContext<TEntity> context)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));
            if (context.Entity == null)
                throw new ArgumentNullException(nameof(context.Entity));
            return Task.CompletedTask;
        }
    }

    public abstract class BaseEntityService<TStatusValue, TEntity> : BaseEntityService<TEntity>, IEntityService<TStatusValue, TEntity>
        where TStatusValue : struct, IConvertible
        where TEntity : BaseEntity<TStatusValue>, new()
    {
        protected BaseEntityService(IContextContainer contextContainer) : base(contextContainer)
        {
        }

        protected override async Task InitEntities(IRepositoryContainer container, IInitEntitiesContext<TEntity> context)
        {
            await base.InitEntities(container, context);
            var ids = context.Entities
                .Select(i => i.Id)
                .ToArray();
            if (!ids.Any())
                return;
            var repository = container.GetRepository<IEntityRepository>();
            var statusesByEntity = (await repository.GetStatuses(ids))
                .ToLookup(i => i.Id);
            foreach (var entity in context.Entities)
            {
                var statuses = statusesByEntity[entity.Id]
                    .ToArray();
                if (!statuses.Any())
                {
                    continue;
                }
                var lastCreated = statuses.Max(i => i.Created);
                var lastStatus = statuses.First(i => i.Created == lastCreated);
                entity.StatusInfo = new StatusInfo<TStatusValue>
                {
                    Created = lastStatus.Created,
                    Note = lastStatus.NoteValue,
                    Status = Enum.TryParse<TStatusValue>(lastStatus.StatusValue, out var status) ? status : default
                };
            }
        }

        protected override async Task SaveEntity(IRepositoryContainer container, ISaveEntityContext<TEntity> context)
        {
            await base.SaveEntity(container, context);
            var repository = container.GetRepository<IEntityRepository>();
            await repository.SaveEntityStatus(context.Entity.Id, context.Entity.StatusInfo.Note, context.Entity.StatusInfo.Status.ToString());
        }

        protected override async Task ValidateEntity(IRepositoryContainer container, ISaveEntityContext<TEntity> context)
        {
            await base.ValidateEntity(container, context);
            if (context.Entity.StatusInfo == null)
                throw new ArgumentNullException(nameof(context.Entity.StatusInfo));
        }
    }
}
