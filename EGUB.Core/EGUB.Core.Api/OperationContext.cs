﻿using EGUB.Core.Contracts.BLL;
using Microsoft.AspNetCore.Http;
using System;
using System.Linq;

namespace EGUB.Core.Api
{
    public class OperationContext : IOperationContext
    {
        private readonly Lazy<Guid> _getCurrentUserLazy;

        public OperationContext(HttpContext httpContext)
        {
            HttpContext = httpContext;
            _getCurrentUserLazy = new Lazy<Guid>(() => GetCurrentUserId(httpContext));
        }

        protected HttpContext HttpContext { get; }

        public Guid GetCurrentUserId() => _getCurrentUserLazy.Value;

        private Guid GetCurrentUserId(HttpContext httpContext)
        {
            return httpContext.User.Identity is { IsAuthenticated: false }
                ? Guid.Empty
                : Guid.Parse(httpContext.User.Claims.Single(i => i.Type == "UserId").Value);
        }
    }
}
