﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace EGUB.Core.Api
{
    public static class ControllerExtensions
    {
        public static async Task<IActionResult> Execute(this ControllerBase controller, Func<Task> action)
        {
            try
            {
                await action();
                return controller.Ok();
            }
            catch (Exception ex)
            {
                return controller.UnprocessableEntity(ex.Message);
            }
        }

        public static async Task<IActionResult> Get<TResult>(this ControllerBase controller, Func<Task<TResult>> func)
        {
            try
            {
                var result = await func();
                return controller.Ok(result);
            }
            catch (Exception ex)
            {
                return controller.UnprocessableEntity(ex.Message);
            }
        }
    }
}
