﻿using EGUB.Core.Contracts.BLL;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;

namespace EGUB.Core.Api
{
    public class GetEntitiesContext : OperationContext, IGetEntitiesContext
    {
        public GetEntitiesContext(HttpContext httpContext, ICollection<Guid> ids) : base(httpContext)
        {
            Ids = ids;
        }

        public ICollection<Guid> Ids { get; }
    }
}
