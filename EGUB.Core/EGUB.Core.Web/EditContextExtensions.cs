﻿using Microsoft.AspNetCore.Components.Forms;
using System.Linq;

namespace EGUB.Core.Web
{
    public static class EditContextExtensions
    {
        public static bool IsAnyModified(this EditContext editContext) =>
            editContext.Model != null &&
            editContext.Model
                .GetType()
                .GetProperties()
                .Any(p =>
                {
                    var field = editContext.Field(p.Name);
                    return editContext.IsModified(field);
                });
    }
}
