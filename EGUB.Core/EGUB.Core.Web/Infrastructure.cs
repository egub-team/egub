﻿using Blazored.LocalStorage;
using Microsoft.Extensions.Configuration;
using System.Net.Http;

namespace EGUB.Core.Web
{
    internal class Infrastructure : IInfrastructure
    {
        public Infrastructure(
            IHostEnvironment hostEnvironment,
            IHttpClientFactory clientFactory,
            IConfiguration configuration,
            ILocalStorageService localStorage)
        {
            HostEnvironment = hostEnvironment;
            ClientFactory = clientFactory;
            Configuration = configuration;
            LocalStorage = localStorage;
        }

        public IHostEnvironment HostEnvironment { get; }

        public IHttpClientFactory ClientFactory { get; }

        public IConfiguration Configuration { get; }

        public ILocalStorageService LocalStorage { get; }
    }
}
