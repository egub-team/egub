﻿using Microsoft.AspNetCore.Components;
using System;

namespace EGUB.Core.Web.Components
{
    public partial class Error
    {
        [Inject]
        private NavigationManager NavigationManager { get; set; } = default!;

        [Parameter]
        public RenderFragment ChildContent { get; set; }

        private string _errorMessage;

        public void ProcessError(Exception ex)
        {
            var httpClientResponseException = ex as HttpClientResponseException;
            if (httpClientResponseException != null)
            {
                switch (httpClientResponseException.StatusCode)
                {
                    case System.Net.HttpStatusCode.Unauthorized:
                        NavigationManager.NavigateTo("/login");
                        return;
                    default:
                        break;
                }
            }

            _errorMessage = ex.Message;
            StateHasChanged();
        }

        public void ClearErrorMessages()
        {
            _errorMessage = null;
            StateHasChanged();
        }
    }
}
