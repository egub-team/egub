﻿using Blazored.LocalStorage;
using Microsoft.Extensions.Configuration;
using System.Net.Http;

namespace EGUB.Core.Web
{
    public interface IInfrastructure
    {
        IHostEnvironment HostEnvironment { get; }
        IHttpClientFactory ClientFactory { get; }
        IConfiguration Configuration { get; }
        ILocalStorageService LocalStorage { get; }
    }
}
