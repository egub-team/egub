﻿using System;
using System.Net;

namespace EGUB.Core.Web
{
    public class HttpClientResponseException : Exception
    {
        public HttpClientResponseException(string message, HttpStatusCode statusCode) : base(message)
        {
            StatusCode = statusCode;
        }

        public HttpStatusCode StatusCode { get; }
    }
}
