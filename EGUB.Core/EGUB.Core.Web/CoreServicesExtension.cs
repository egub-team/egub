﻿using Blazored.LocalStorage;
using Microsoft.Extensions.DependencyInjection;
using System.Net.Http;
using System;
using Microsoft.Extensions.Options;

namespace EGUB.Core.Web
{
    public static class CoreServicesExtension
    {
        public static void AddCore(this IServiceCollection services, string baseAddress, Func<HttpMessageHandler> messageHandlerFunc = null)
        {
            services.AddBlazoredLocalStorage();
            services.AddScoped<IHostEnvironment>(sp => new HostEnvironment(baseAddress));
            services.AddScoped<IInfrastructure, Infrastructure>();   

            var httpClientBuilder = services.AddHttpClient<HttpClient>(Options.DefaultName);
            if (messageHandlerFunc != null)
                httpClientBuilder.ConfigurePrimaryHttpMessageHandler(messageHandlerFunc);
        }
    }
}
