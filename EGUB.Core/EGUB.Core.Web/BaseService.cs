﻿using Microsoft.AspNetCore.Components.Forms;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace EGUB.Core.Web
{
    public abstract class BaseService
    {
        public const string UnexpectedError = "Enexpected error!";
        public const int MaxAllowedStreamSize = 1024 * 1024 * 50;

        protected BaseService(IInfrastructure infrastructure)
        {
            Infrastructure = infrastructure;
        }

        protected IInfrastructure Infrastructure { get; }

        protected async Task<FileContentDto> GetFileContentFromApi(string route)
        {
            var url = $@"{Infrastructure.Configuration["ApiUrl"]}/{route}";

            using var httpClient = await GetHttpClient();

            HttpResponseMessage response = await httpClient.GetAsync(url);
            if (response.IsSuccessStatusCode)
            {
                var contentType = response.Content.Headers.GetValues("Content-Type").First();
                return new FileContentDto
                {
                    Type = contentType,
                    Content = await response.Content.ReadAsStreamAsync()
                };
            }

            string text = await response.Content.ReadAsStringAsync();
            if (string.IsNullOrEmpty(text))
            {
                text = response.ReasonPhrase;
            }

            if (string.IsNullOrEmpty(text))
            {
                text = "Enexpected error!";
            }

            throw new HttpClientResponseException(text, response.StatusCode);
        }

        protected Task<TResult> GetFromApi<TResult>(string route) =>
            GetFromUrl<TResult>(() => $"{Infrastructure.Configuration["ApiUrl"]}/{route}");

        protected async Task<TResult> GetFromUrl<TResult>(Func<string> urlFunc)
        {
            var uri = urlFunc();
            using var httpClient = await GetHttpClient();

            var responce = await httpClient.GetAsync(uri);
            return await GetFromResponse<TResult>(responce);
        }

        protected Task PutToApi<TItem>(string route, TItem item) =>
            PutToUrl(() => $"{Infrastructure.Configuration["ApiUrl"]}/{route}", item);

        protected async Task PutToUrl<TItem>(Func<string> urlFunc, TItem item)
        {
            var uri = urlFunc();

            using var httpClient = await GetHttpClient();
            var responce = await httpClient.PutAsJsonAsync(uri, item);

            if (responce.IsSuccessStatusCode)
                return;

            var messageError = await responce.Content.ReadAsStringAsync();
            if (string.IsNullOrEmpty(messageError))
            {
                messageError = responce.ReasonPhrase;
            }
            if (string.IsNullOrEmpty(messageError))
            {
                messageError = UnexpectedError;
            }

            throw new HttpClientResponseException(messageError, responce.StatusCode);
        }

        protected Task<TResult> PostToApi<TItem, TResult>(string route, TItem item) =>
            PostToUrl<TItem, TResult>(() => $"{Infrastructure.Configuration["ApiUrl"]}/{route}", item);

        protected async Task<TResult> PostToUrl<TItem, TResult>(Func<string> urlFunc, TItem item)
        {
            var uri = urlFunc();

            using var httpClient = await GetHttpClient();
            var responce = await httpClient.PostAsJsonAsync(uri, item);
            return await GetFromResponse<TResult>(responce);
        }

        protected async Task PostToApiMultipartForm(
            string route,
            ICollection<IBrowserFile> files,
            string name = "files",
            IDictionary<string, string> data = null)
        {
            using var content = new MultipartFormDataContent();
            foreach (var file in files)
            {
                var fileContent = new StreamContent(file.OpenReadStream(MaxAllowedStreamSize));
                fileContent.Headers.ContentType = new MediaTypeHeaderValue(file.ContentType);

                content.Add(
                    content: fileContent,
                    name: $"\"{name}\"",
                    fileName: file.Name);
            }

            if (data != null)
            {
                foreach (var item in data)
                {
                    content.Add(new StringContent(item.Value), name: item.Key);
                }
            }

            using var httpClient = await GetHttpClient();
            var response = await httpClient.PostAsync($"{Infrastructure.Configuration["ApiUrl"]}/{route}", content);
            if (response.IsSuccessStatusCode)
                return;

            var text = await response.Content.ReadAsStringAsync();
            if (string.IsNullOrEmpty(text))
                text = response.ReasonPhrase;

            if (string.IsNullOrEmpty(text))
                text = "Enexpected error!";

            throw new HttpClientResponseException(text, response.StatusCode);
        }

        protected async Task<TResult> PostToApiMultipartForm<TResult>(
            string route,
            ICollection<IBrowserFile> files,
            string name = "files",
            IDictionary<string, string> data = null)
        {
            using var content = new MultipartFormDataContent();
            foreach (var file in files)
            {
                var fileContent = new StreamContent(file.OpenReadStream(MaxAllowedStreamSize));
                fileContent.Headers.ContentType = new MediaTypeHeaderValue(file.ContentType);

                content.Add(
                    content: fileContent,
                    name: $"\"{name}\"",
                    fileName: file.Name);
            }

            if (data != null)
            {
                foreach (var item in data)
                {
                    content.Add(new StringContent(item.Value), name: item.Key);
                }
            }

            using var httpClient = await GetHttpClient();
            var response = await httpClient.PostAsync($"{Infrastructure.Configuration["ApiUrl"]}/{route}", content);

            return await GetFromResponse<TResult>(response);
        }

        protected Task PostToApiFileStream(string route, IBrowserFile file) =>
            PostToApiStream(route, file.OpenReadStream(MaxAllowedStreamSize));

        protected Task<TResult> PostToApiFileStream<TResult>(string route, IBrowserFile file) =>
            PostToApiStream<TResult>(route, file.OpenReadStream(MaxAllowedStreamSize));

        protected async Task PostToApiStream(string route, Stream stream)
        {
            using var content = new StreamContent(stream);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

            using var httpClient = await GetHttpClient();
            var response = await httpClient.PostAsync($"{Infrastructure.Configuration["ApiUrl"]}/{route}", content);

            if (response.IsSuccessStatusCode)
                return;

            var text = await response.Content.ReadAsStringAsync();
            if (string.IsNullOrEmpty(text))
                text = response.ReasonPhrase;

            if (string.IsNullOrEmpty(text))
                text = "Enexpected error!";

            throw new HttpClientResponseException(text, response.StatusCode);
        }

        protected async Task<TResult> PostToApiStream<TResult>(string route, Stream stream)
        {
            using var content = new StreamContent(stream);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

            using var httpClient = await GetHttpClient();
            var response = await httpClient.PostAsync($"{Infrastructure.Configuration["ApiUrl"]}/{route}", content);

            return await GetFromResponse<TResult>(response);
        }

        private async Task<HttpClient> GetHttpClient()
        {
            var httpClient = Infrastructure.ClientFactory.CreateClient(Options.DefaultName);
            if (!string.IsNullOrEmpty(Infrastructure.HostEnvironment.BaseAddress))
                httpClient.BaseAddress = new Uri(Infrastructure.HostEnvironment.BaseAddress);
            await SetDefaultRequestHeaders(httpClient.DefaultRequestHeaders);
            return httpClient;
        }

        protected virtual Task SetDefaultRequestHeaders(HttpRequestHeaders requestHeaders)
        {
            return Task.CompletedTask;
        }

        private async Task<TResult> GetFromResponse<TResult>(HttpResponseMessage response)
        {
            if (response.IsSuccessStatusCode)
            {
                if (typeof(TResult) == typeof(string))
                {
                    object result = await response.Content.ReadAsStringAsync();
                    return (TResult)result;
                }

                var contentSize = response.Content.Headers.ContentLength.GetValueOrDefault();
                if (contentSize > 0)
                    return await response.Content.ReadFromJsonAsync<TResult>();

                return default;
            }

            var messageError = await response.Content.ReadAsStringAsync();
            if (string.IsNullOrEmpty(messageError))
            {
                messageError = response.ReasonPhrase;
            }
            if (string.IsNullOrEmpty(messageError))
            {
                messageError = UnexpectedError;
            }

            throw new HttpClientResponseException(messageError, response.StatusCode);
        }
    }
}
