﻿using System.IO;

namespace EGUB.Core.Web
{
    public class FileContentDto
    {
        public string Type { get; set; } = string.Empty;
        public Stream Content { get; set; } = default!;
    }
}
