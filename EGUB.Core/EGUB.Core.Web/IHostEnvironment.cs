﻿namespace EGUB.Core.Web
{
    public interface IHostEnvironment
    {
        string BaseAddress { get; }
    }
}
