﻿namespace EGUB.Core.Web
{
    internal class HostEnvironment : IHostEnvironment
    {
        public HostEnvironment(string baseAddress)
        {
            BaseAddress = baseAddress;    
        }

        public string BaseAddress { get; }
    }
}
