﻿using System;

namespace EGUB.Core.Web.Models
{
    public class NoteModel
    {
        public DateTime Created { get; set; }

        public string Note { get; set; }
    }
}
