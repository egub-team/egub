﻿using EGUB.Security.Web.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;

namespace EGUB.Security.Mobile.Layout
{
    public partial class MainMenu
    {
        [Inject]
        private IAuthService AuthenticationService { get; set; } = default!;

        [Inject]
        private NavigationManager NavigationManager { get; set; } = default!;

        [Parameter, EditorRequired]
        public AuthenticationState AuthenticationState { get; set; } = default!;


        private string _userName = string.Empty;
        private string _avatarText = string.Empty;

        protected override void OnInitialized()
        {
            base.OnInitialized();

            LoadUserInfo();
        }

        private void LoadUserInfo()
        {
            var personName = AuthenticationState.User.Claims
                .Where(i => !string.IsNullOrEmpty(i.Value) && i.Type == "PersonName")
                .Select(i => i.Value)
                .FirstOrDefault();

            var email = AuthenticationState.User.Claims
                .Where(i => i.Type == "Email")
                .Select(i => i.Value)
                .FirstOrDefault();

            _userName = personName ?? email ?? AuthenticationState!.User!.Identity!.Name!;
            _avatarText = _userName.First().ToString().ToUpper();
        }

        private async Task SignOut()
        {
            await AuthenticationService.Logout();
            NavigationManager.Refresh(true);
        }
    }
}
