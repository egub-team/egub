﻿using Microsoft.AspNetCore.Components;
using MudBlazor;

namespace EGUB.Security.Mobile.Layout
{
    public partial class PageContainer
    {
        [CascadingParameter]
        private IMainLayout MainLayout { get; set; } = default!;
        
        [Parameter, EditorRequired]
        public string Caption { get; set; } = string.Empty;

        [Parameter, EditorRequired]
        public IEnumerable<BreadcrumbItem> BreadcrumbItems { get; set; } = default!;

        [Parameter, EditorRequired]
        public RenderFragment ChildContent { get; set; } = default!;

        private IReadOnlyList<BreadcrumbItem> _breadcrumbs = ArraySegment<BreadcrumbItem>.Empty;

        protected override void OnInitialized()
        {
            base.OnInitialized();

            MainLayout.SetBreadcrumbs(GetBreadcrumbItems());
        }

        private IEnumerable<BreadcrumbItem> GetBreadcrumbItems()
        {
            foreach (var item in BreadcrumbItems)
                yield return item;

            yield return new BreadcrumbItem(Caption, null, disabled: true);
        }
    }
}
