﻿using MudBlazor;

namespace EGUB.Security.Mobile.Layout
{
    public partial class MainLayout : IMainLayout
    {
        private bool _isMenuOpen;
        private IReadOnlyList<BreadcrumbItem> _breadcrumbs = ArraySegment<BreadcrumbItem>.Empty;

        public void SetBreadcrumbs(IEnumerable<BreadcrumbItem> items)
        {
            _breadcrumbs = items
                .ToList();

            StateHasChanged();
        }

        private void OnMenuClick()
        {
            _isMenuOpen = !_isMenuOpen;
        }
    }
}
