﻿using MudBlazor;

namespace EGUB.Security.Mobile.Layout
{
    internal interface IMainLayout
    {
        void SetBreadcrumbs(IEnumerable<BreadcrumbItem> items);
    }
}
