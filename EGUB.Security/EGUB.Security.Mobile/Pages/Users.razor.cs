﻿using EGUB.Core.Helpers;
using EGUB.Security.Mobile.Models;
using EGUB.Security.Web.Services;
using Microsoft.AspNetCore.Components;
using MudBlazor;

namespace EGUB.Security.Mobile.Pages
{
    [Route(@"/users")]
    public partial class Users
    {
        [Inject]
        private IUserService Service { get; set; } = default!;

        private string _caption = "Users";

        private UserModel? _user;
        private UserModel? User 
        {
            get => _user;
            set
            {
                if (_user == value) 
                    return;

                _user = value;
                _isExpanded = false;
            }
        }
        private bool _isExpanded = false;


        private IEnumerable<BreadcrumbItem> GetBreadcrumbs()
        {
            yield return new BreadcrumbItem("Home", "#");
        }

        private async Task<IEnumerable<UserModel?>> SearchItems(string text, CancellationToken cancellationToken)
        {
            if (string.IsNullOrEmpty(text))
                return ArraySegment<UserModel?>.Empty;

            var filter = new Web.Models.SearchUsersModel
            {
                PersonName = text,
            };

            return await Service
                .SearchUsers(filter)
                .SelectAsItemsAsync(i => i.SelectAsModel());
        }

        private string? GetDysplayText(UserModel? model) =>
            model?.GetDysplayText();
    }
}
