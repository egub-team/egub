﻿using EGUB.Security.Mobile.Models;
using Microsoft.AspNetCore.Components;
using MudBlazor;

namespace EGUB.Security.Mobile.Dialogs
{
    public partial class WaitDialog
    {
        [Parameter]
        public bool IsVisibled { get; set; }
        [Parameter]
        public EventCallback<bool> IsVisibledChanged { get; set; }

        [Parameter, EditorRequired]
        public WaitDialogModel Model { get; set; } = default!;
        

        private DialogOptions _dialogOptions = new DialogOptions
        {
            BackdropClick = false,
        };
    }
}
