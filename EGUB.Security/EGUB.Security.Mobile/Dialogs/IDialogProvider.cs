﻿using EGUB.Security.Mobile.Models;

namespace EGUB.Security.Mobile.Dialogs
{
    internal interface IDialogProvider
    {
        Task<bool> Confirm(string title, string message);
        Task<bool> Execute(Func<Task> func);
        Task<bool> Execute(Func<Task> func, WaitDialogModel waitDialogModel);
    }
}
