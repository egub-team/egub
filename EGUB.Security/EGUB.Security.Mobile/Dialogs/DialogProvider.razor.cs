﻿using EGUB.Security.Mobile.Models;
using Microsoft.AspNetCore.Components;
using MudBlazor;

namespace EGUB.Security.Mobile.Dialogs
{
    public partial class DialogProvider : IDialogProvider
    {
        [Inject]
        private IDialogService DialogService { get; set; } = default!;

        [Parameter, EditorRequired]
        public RenderFragment ChildContent { get; set; } = default!;


        private bool _isWaitDialogVisibled = false;
        private WaitDialogModel _waitDialogModel = new();
        private bool _isErrorDialogVisibled = false;
        private string _error = string.Empty;

        public async Task<bool> Confirm(string title, string message)
        {
            var result = await DialogService
                .ShowMessageBox(
                    title,
                    message,
                    cancelText: "Cancel");

            return result.GetValueOrDefault();
        }

        public Task<bool> Execute(Func<Task> func, WaitDialogModel waitDialogModel)
        {
            _waitDialogModel = waitDialogModel;
            _isWaitDialogVisibled = true;
            StateHasChanged();

            return Execute(func);
        }

        public async Task<bool> Execute(Func<Task> func)
        {
            _error = string.Empty;
            try
            {
                await func();
                
                _waitDialogModel = new();
                _isWaitDialogVisibled = false;
                StateHasChanged();

                return true;
            }
            catch (Exception ex)
            {
                _waitDialogModel = new();
                _isWaitDialogVisibled = false;

                _error = ex.Message;
                _isErrorDialogVisibled = true;
                StateHasChanged();
                return false;
            }
        }
    }
}
