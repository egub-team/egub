﻿using Microsoft.AspNetCore.Components;
using MudBlazor;

namespace EGUB.Security.Mobile.Dialogs
{
    public partial class ErrorDialog
    {
        [Parameter]
        public bool IsVisibled { get; set; }
        [Parameter]
        public EventCallback<bool> IsVisibledChanged { get; set; }

        [Parameter, EditorRequired]
        public string Message { get; set; } = string.Empty;

        private DialogOptions _dialogOptions = new DialogOptions
        {
            CloseOnEscapeKey = true,
        };

        private Task Close()
        {
            IsVisibled = false;
            return IsVisibledChanged.InvokeAsync(IsVisibled);
        }
    }
}
