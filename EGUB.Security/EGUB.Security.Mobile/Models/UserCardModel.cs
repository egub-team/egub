﻿using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;
using UserDetailsDto = EGUB.Security.Web.Models.EditUserModel;


namespace EGUB.Security.Mobile.Models
{
    internal class UserCardModel
    {
        public string Email { get; set; } = string.Empty;
        [Required]
        public string PersonName { get; set; } = string.Empty;
        public Expression<Func<string>> ForPersonName => () => PersonName;
        public string PhoneNumber { get; set; } = string.Empty;
        public IReadOnlyCollection<string> Roles { get; set; } = ArraySegment<string>.Empty;
    }

    internal static class UserCardModelExtensions
    {
        public static UserCardModel SelectAsModel(this UserDetailsDto dto) =>
            new UserCardModel
            {
                Email = dto.Email,
                PersonName = dto.PersonName,
                PhoneNumber = dto.PhoneNumber,
                Roles = dto.Roles.ToList()
            };

        public static UserDetailsDto SelectAsDto(this UserCardModel model, Guid id) =>
            new UserDetailsDto
            {
                Id = id,
                Email = model.Email,
                PersonName = model.PersonName,
                PhoneNumber= model.PhoneNumber,
                Roles = model.Roles.ToArray()
            };
    }
}
