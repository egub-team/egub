﻿using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;
using RegisterUserDto = EGUB.Security.Web.Models.RegisterModel;

namespace EGUB.Security.Mobile.Models
{
    internal class UserRegisterModel
    {
        [Required]
        [Display(Name = "Person Name")]
        public string PersonName { get; set; } = string.Empty;
        public Expression<Func<string>> ForPersonName => () => PersonName;

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; } = string.Empty;
        public Expression<Func<string>> ForEmail => () => Email;

        [Required]
        [Display(Name = "Login Name")]
        public string Code { get; set; } = string.Empty;
        public Expression<Func<string>> ForCode => () => Code;

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; } = string.Empty;
        public Expression<Func<string>> ForPassword => () => Password;

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; } = string.Empty;
        public Expression<Func<string>> ForConfirmPassword => () => ConfirmPassword;
    }

    internal static class UserRegisterModelExtensions
    {
        public static RegisterUserDto SelectAsDto(this UserRegisterModel model) =>
            new RegisterUserDto
            {
                Code = model.Code,
                Email = model.Email,
                PersonName = model.PersonName,
                Password = model.Password,
                ConfirmPassword = model.ConfirmPassword,
            };
    }
}
