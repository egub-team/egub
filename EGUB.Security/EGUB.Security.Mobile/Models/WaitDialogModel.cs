﻿namespace EGUB.Security.Mobile.Models
{
    public class WaitDialogModel
    {
        public string Title { get; set; } = string.Empty;
        public string Message { get; set; } = "Please, wait";
    }
}
