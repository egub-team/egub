﻿using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;
using SignInLoginDto = EGUB.Security.Web.Models.LoginModel;

namespace EGUB.Security.Mobile.Models
{
    internal class LoginModel
    {
        [Required]
        public string Name { get; set; } = string.Empty;
        public Expression<Func<string>> ForName => () => Name;

        [Required] 
        public string Password { get; set; } = string.Empty;
        public Expression<Func<string>> ForPassword => () => Password;
    }

    internal static class LoginModelExtensions
    {
        public static SignInLoginDto SelectAsDto(this LoginModel model) =>
            new SignInLoginDto
            {
                LoginName = model.Name,
                Password = model.Password,
                Roles = new[] { "Admin", "Candidate", "Member", "Guest" }
            };
    }
}
