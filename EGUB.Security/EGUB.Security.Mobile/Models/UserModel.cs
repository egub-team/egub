﻿using UserDto = EGUB.Security.Web.Models.UserListItemModel;

namespace EGUB.Security.Mobile.Models
{
    internal class UserModel
    {
        public Guid UserId { get; set; }
        public string Name { get; set; } = string.Empty;
        public string PersonName { get; set; } = string.Empty;
    }

    internal static class UserModelExtensions
    {
        public static UserModel SelectAsModel(this UserDto dto) =>
            new UserModel
            {
                UserId = dto.Id,
                Name = dto.Code,
                PersonName = dto.PersonName,
            };

        public static string GetDysplayText(this UserModel model) =>
            model.PersonName;

        public static string GetFullName(this UserModel model) =>
            $"{model.PersonName} ({model.Name})";
    }
}
