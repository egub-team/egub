﻿using Microsoft.AspNetCore.Components;
using MudBlazor;

namespace EGUB.Security.Mobile.Extensions
{
    internal static class ComponentExtensions
    {
        public static IEnumerable<BreadcrumbItem> GetEmptyBreadcrumbs(this IComponent component) => 
            Enumerable.Empty<BreadcrumbItem>();
    }
}
