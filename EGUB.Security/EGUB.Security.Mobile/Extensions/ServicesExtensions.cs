﻿using EGUB.Security.Web;
using Microsoft.Extensions.DependencyInjection;
using MudBlazor.Services;

namespace EGUB.Security.Mobile.Extensions
{
    public static class ServicesExtensions
    {
        public static void AddSecurityMobile(this IServiceCollection services, string baseAddress, Func<HttpMessageHandler>? messageHandlerFunc = null)
        {
            services.AddInternalItems();

            services.AddSecurity(baseAddress, messageHandlerFunc);
        }

        public static void AddTestSecurityMobile(this IServiceCollection services, string baseAddress, Func<HttpMessageHandler>? messageHandlerFunc = null)
        {
            services.AddInternalItems();

            services.AddTestSecurity(baseAddress, messageHandlerFunc);
        }

        public static void AddInternalItems(this IServiceCollection services)
        {
            services.AddMudServices();
        }
    }
}
