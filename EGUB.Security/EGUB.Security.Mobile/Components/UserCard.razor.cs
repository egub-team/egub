﻿using EGUB.Core.Helpers;
using EGUB.Core.Web;
using EGUB.Security.Mobile.Dialogs;
using EGUB.Security.Mobile.Models;
using EGUB.Security.Web.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;

namespace EGUB.Security.Mobile.Components
{
    public partial class UserCard
    {
        private static ICollection<string> _roles = [
            "Admin",
            "Member",
            "Candidate"
        ];

        [CascadingParameter]
        private IDialogProvider DialogProvider { get; set; } = default!;

        [Inject]
        private IUserService Service { get; set; } = default!;

        [Parameter, EditorRequired]
        public Guid UserId { get; set; }

        private UserCardModel? _model = null;

        protected override async Task OnInitializedAsync()
        {
            await base.OnInitializedAsync();
            await LoadModel();
        }

        private async Task LoadModel()
        {
            _model = null;
            StateHasChanged();
            try
            {
                _model = await Service
                    .GetUserDetails(UserId)
                    .SelectAsItemAsync(i => i!.SelectAsModel());
            }
            finally
            {
                StateHasChanged();
            }
        }

        private bool IsSaveDisabled(EditContext editContext) =>
            !editContext.IsAnyModified() || !editContext.Validate();

        private async Task SaveModel(EditContext editContext)
        {
            if (!await DialogProvider.Execute(
                () => Service.EditUser(_model!.SelectAsDto(UserId)),
                new WaitDialogModel { Title = "Update the User..." }))
                return;

            editContext.MarkAsUnmodified();
        }

        private Func<IReadOnlyCollection<string>, Task> OnRolesChanged(EditContext editContext) =>
            roles => RolesChanged(editContext, roles);

        private Task RolesChanged(EditContext editContext, IReadOnlyCollection<string> roles)
        {
            _model!.Roles = roles;
            editContext.NotifyFieldChanged(editContext.Field(nameof(_model.Roles)));
            return Task.CompletedTask;
        }
    }
}
