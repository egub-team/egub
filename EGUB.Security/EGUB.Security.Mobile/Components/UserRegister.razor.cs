﻿using EGUB.Core.Web;
using EGUB.Security.Mobile.Dialogs;
using EGUB.Security.Mobile.Models;
using EGUB.Security.Web.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;

namespace EGUB.Security.Mobile.Components
{
    public partial class UserRegister
    {
        [CascadingParameter]
        private IDialogProvider DialogProvider { get; set; } = default!;
        [Inject]
        private IAuthService AuthenticationService { get; set; } = default!;
        [Inject]
        private NavigationManager NavigationManager { get; set; } = default!;

        private UserRegisterModel _model = new();

        private bool IsSubmitDisabled(EditContext editContext) =>
            !editContext.IsAnyModified() || !editContext.Validate();

        private async Task Submit()
        {
            if (!await DialogProvider.Execute(
                () => AuthenticationService.Register(_model.SelectAsDto()),
                new WaitDialogModel { Title = "Register an User..." }))
                return;

            NavigationManager.Refresh(true);
        }
    }
}
