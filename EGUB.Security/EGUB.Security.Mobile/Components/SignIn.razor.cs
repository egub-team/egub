﻿using EGUB.Core.Web;
using EGUB.Security.Mobile.Dialogs;
using EGUB.Security.Mobile.Models;
using EGUB.Security.Web.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;

namespace EGUB.Security.Mobile.Components
{
    public partial class SignIn
    {
        [CascadingParameter]
        private IDialogProvider DialogProvider { get; set; } = default!;
        [Inject]
        private IAuthService AuthenticationService { get; set; } = default!;
        [Inject]
        private NavigationManager NavigationManager { get; set; } = default!;

        private LoginModel _model = new();

        private bool IsSubmitDisabled(EditContext editContext) =>
            !editContext.IsAnyModified() || !editContext.Validate();

        private async Task Submit()
        {
            if (!await DialogProvider.Execute(
                () => AuthenticationService.Login(_model.SelectAsDto()),
                new WaitDialogModel { Title = "Login..." }))
                return;

            NavigationManager.Refresh(true);
        }
    }
}
