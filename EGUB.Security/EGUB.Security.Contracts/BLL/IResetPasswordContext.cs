﻿using EGUB.Core.Contracts.BLL;
using System;

namespace EGUB.Security.Contracts.BLL
{
    public interface IResetPasswordContext : IOperationContext
    {
        Guid Id { get; }
        string NewPassword { get; }
    }
}
