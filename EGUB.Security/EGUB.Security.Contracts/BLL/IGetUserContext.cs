﻿using EGUB.Core.Contracts.BLL;

namespace EGUB.Security.Contracts.BLL
{
    public interface IGetUserContext : IOperationContext
    {
        string Code { get; }
    }
}
