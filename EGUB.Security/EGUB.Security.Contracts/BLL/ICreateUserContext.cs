﻿using EGUB.Core.Contracts.BLL;

namespace EGUB.Security.Contracts.BLL
{
    public interface ICreateUserContext : IOperationContext
    {
        string Code { get; }
        string Password { get; }
        string Email { get; }
        string PersonName { get; }
        string PhoneNumber { get; }
        string[] Roles { get; }
        string Note { get; }
        string ConfirmationEmailUrl { get; }
    }
}
