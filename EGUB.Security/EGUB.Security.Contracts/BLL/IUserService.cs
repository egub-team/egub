﻿using EGUB.Core.Contracts.BLL;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EGUB.Security.Contracts.BLL
{
    public interface IUserService : IEntityService<UserStatus, User>
    {
        Task<Guid> CreateUser(ICreateUserContext context);
        Task UpdateUser(IUpdateUserContext context);
        Task<User> GetUser(IGetUserContext context);
        Task<ICollection<User>> GetUsers(IGetUsersContext context);
        Task<bool> CheckPassword(string passwordHash, string password);
        Task ChangePassword(IChangePasswordContext context);
        Task ResetPassword(IResetPasswordContext context);
        Task ConfirmEmail(IConfirmEmailContext context);
    }
}
