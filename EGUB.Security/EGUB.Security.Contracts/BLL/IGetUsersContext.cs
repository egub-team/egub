﻿using EGUB.Core.Contracts.BLL;

namespace EGUB.Security.Contracts.BLL
{
    public interface IGetUsersContext : IOperationContext
    {
        string Code { get; }
        string Email { get; }
        string PersonName { get; }
        string PhoneNumber { get; }
        string[] Roles { get; }
    }
}
