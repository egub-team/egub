﻿using EGUB.Core.Contracts.BLL;
using System;

namespace EGUB.Security.Contracts.BLL
{
    public interface IUpdateUserContext : IOperationContext
    {
        Guid Id { get; }
        string Note { get; }
        string StatusNote { get; }
        UserStatus StatusValue { get; }
        string Email { get; }
        string PersonName { get; }
        string PhoneNumber { get; }
        string[] Roles { get; }
    }
}
