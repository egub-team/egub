﻿using EGUB.Core.Contracts.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.Security.Contracts.BLL
{
    public interface IConfirmEmailContext : IOperationContext
    {
        Guid Id { get; }
        string Password { get; }
    }
}
