﻿using EGUB.Core.Contracts.BLL;
using System;

namespace EGUB.Security.Contracts.BLL
{
    public interface IChangePasswordContext : IOperationContext
    {
        Guid Id { get; }
        string CurrentPassword { get; }
        string NewPassword { get; }
    }
}
