﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EGUB.Security.Contracts.DAL
{
    public interface IUserRepository : Core.Contracts.DAL.IRepository
    {
        Task<ICollection<UserItemData>> GetItems(ICollection<Guid> ids);
        Task<ICollection<UserRoleItemData>> GetRoles(ICollection<Guid> ids);
        Task<Guid> GetUserId(string code);
        Task SaveUser(Guid id, string code, string passwordHash, string email, string personName, string phoneNumber);
        Task SaveRoles(Guid id, string[] roles);
        Task<ICollection<Guid>> GetUserIds(string code, string email, string personName, string phoneNumber, string[] roles);
    }

    public record UserItemData(Guid Id, string Code, string PasswordHash, string Email, string PersonName, string PhoneNumber);

    public record UserRoleItemData(Guid Id, string Code);
}
