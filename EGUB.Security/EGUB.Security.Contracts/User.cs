﻿namespace EGUB.Security.Contracts
{
    public class User : Core.Contracts.BaseEntity<UserStatus>
    {
        public override string EntityType => EntityTypes.USER.ToString();
        public string Code { get; set; }
        public string PasswordHash { get; set; }
        public string Email { get; set; }
        public string PersonName { get; set; }
        public string PhoneNumber { get; set; }
        public string[] Roles { get; set; }
    }
}
