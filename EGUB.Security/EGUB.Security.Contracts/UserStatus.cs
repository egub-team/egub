﻿namespace EGUB.Security.Contracts
{
    public enum UserStatus
    {
        Enable,
        Disable
    }
}
