﻿namespace EGUB.Security.Contracts
{
    public enum UserRoles
    {
        Guest,
        Candidate,
        Member,
        Admin
    }
}
