﻿using Autofac;
using EGUB.Core.Contracts.Common.Email;
using EGUB.Core.Contracts.DAL;
using EGUB.Security.Contracts.BLL;
using Microsoft.Extensions.Configuration;

namespace EGUB.Security.BLL
{
    public class ContainerFactory
    {
        public static void RegisterUserService(ContainerBuilder builder, string contextName)
        {
            builder
                .Register(c => new UserService(contextName, c.Resolve<IContextContainer>(), c.Resolve<IConfiguration>(), c.Resolve<IEmailService>()))
                .As<IUserService>()
                .InstancePerLifetimeScope();
        }
    }
}
