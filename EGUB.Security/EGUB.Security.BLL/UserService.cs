﻿using EGUB.Core.BLL;
using EGUB.Core.Contracts;
using EGUB.Core.Contracts.BLL;
using EGUB.Core.Contracts.Common.Email;
using EGUB.Core.Contracts.DAL;
using EGUB.Core.Helpers;
using EGUB.Security.Contracts;
using EGUB.Security.Contracts.BLL;
using EGUB.Security.Contracts.DAL;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.Security.BLL
{
    internal class UserService : BaseEntityService<UserStatus, User>, IUserService
    {
        private readonly IConfiguration _configuration;
        private readonly IEmailService _emailService;

        public UserService(
            string contextName,
            IContextContainer contextContainer,
            IConfiguration configuration,
            IEmailService emailService) : base(contextContainer)
        {
            ContextName = contextName;
            _configuration = configuration;
            _emailService = emailService;
        }

        protected override string ContextName { get; }

        public async Task<Guid> CreateUser(ICreateUserContext context)
        {
            var user = new User
            {
                StatusInfo = new StatusInfo<UserStatus>
                {
                    Status = UserStatus.Enable,
                },
                Code = context.Code,
                Email = context.Email,
                PasswordHash = GetPasswordHash(context.Password),
                PersonName = context.PersonName,
                PhoneNumber = context.PhoneNumber,
                Roles = context.Roles,
            };

            AddNote(user, context.Note);
            var saveEntityContext = new SaveEntityContext<User>(context, user);
            await Save(saveEntityContext);

            var emailSettings = _configuration.GetSection("EmailSettings");
            if (emailSettings.Exists())
            {
                var emailInfo = new EmailInfo
                {
                    From = emailSettings["FromAddress"],
                    To = [context.Email],
                    Subject = "Confirmation Email",
                    Html = $"<a href = \"{GetConfirmationLink(context, user)}\">Confirm</a>",
                };

                try
                {
                    await _emailService.SendEmail(emailInfo);
                }
                catch
                {
                }
            }

            return user.Id;
        }

        public Task ConfirmEmail(IConfirmEmailContext context)
        {
            return Execute(async container =>
            {
                var getEntitiesContext = new GetEntitiesContext(context, new[] { context.Id });
                var users = await GetEntities(container, getEntitiesContext);
                var user = users.SingleOrDefault();
                if (user == null)
                    throw new ArgumentNullException(nameof(context.Id), "User is not found!");

                var checkedPassword = await CheckPassword(user.PasswordHash, context.Password);
                if (!checkedPassword)
                    throw new ArgumentOutOfRangeException(nameof(context.Password), "The current password is not suitable!");

                if (user.Roles.IsNullOrEmpty() || !user.Roles.Contains(UserRoles.Candidate.ToString()))
                    throw new Exception("The user is not registered!");

                if (user.Roles.Contains(UserRoles.Member.ToString()))
                    throw new Exception("The user's email has already confirmed!");

                var roles = new List<string>(user.Roles);
                roles.Add(UserRoles.Member.ToString());

                user.Roles = roles.ToArray();

                var saveEntityContext = new SaveEntityContext<User>(context, user);
                await Save(container, saveEntityContext);
            });
        }

        public async Task UpdateUser(IUpdateUserContext context)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));
            if (context.Id == Guid.Empty)
                throw new ArgumentNullException(nameof(context.Id));

            await Execute(async container =>
            {
                var getEntitiesContext = new GetEntitiesContext(context, new[] { context.Id });
                var users = await GetEntities(container, getEntitiesContext);
                var user = users.SingleOrDefault();
                if (user == null)
                    throw new ArgumentNullException(nameof(context.Id), "User is not found!");

                user.StatusInfo.Note = context.StatusNote;
                user.StatusInfo.Status = context.StatusValue;
                user.Email = context.Email;
                user.PersonName = context.PersonName;
                user.PhoneNumber = context.PhoneNumber;
                user.Roles = context.Roles;
                AddNote(user, context.Note);
                var saveEntityContext = new SaveEntityContext<User>(context, user);
                await Save(container, saveEntityContext);
            });
        }

        public Task<bool> CheckPassword(string passwordHash, string password) =>
            Task.FromResult(passwordHash == GetPasswordHash(password));

        public async Task<User> GetUser(IGetUserContext context)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));
            var result = default(User);
            await Execute(async container =>
            {
                var repository = container.GetRepository<IUserRepository>();
                var id = await repository.GetUserId(context.Code);
                var getEntitiesContext = new GetEntitiesContext(context, new[] { id });
                var entities = await GetEntities(container, getEntitiesContext);
                result = entities.SingleOrDefault();
            });

            return result;
        }

        public async Task ChangePassword(IChangePasswordContext context)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));
            if (context.CurrentPassword == context.NewPassword)
                throw new ArgumentOutOfRangeException(nameof(context.NewPassword), "The new password is the same as the old one!");

            await Execute(async container =>
            {
                var getEntitiesContext = new GetEntitiesContext(context, new[] { context.Id });
                var entities = await GetEntities(container, getEntitiesContext);
                var user = entities.SingleOrDefault();
                if (user == null)
                    throw new ArgumentNullException(nameof(context.Id), "User is not found!");

                var checkedPassword = await CheckPassword(user.PasswordHash, context.CurrentPassword);
                if (!checkedPassword)
                    throw new ArgumentOutOfRangeException(nameof(context.CurrentPassword), "The current password is not suitable!");

                user.PasswordHash = GetPasswordHash(context.NewPassword);

                var saveEntityContext = new SaveEntityContext<User>(context, user);
                await Save(container, saveEntityContext);
            });
        }

        public async Task ResetPassword(IResetPasswordContext context)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));
            await Execute(async container =>
            {
                var getEntitiesContext = new GetEntitiesContext(context, new[] { context.Id });
                var entities = await GetEntities(container, getEntitiesContext);
                var user = entities.SingleOrDefault();
                if (user == null)
                    throw new ArgumentNullException(nameof(context.Id), "User is not found!");

                user.PasswordHash = GetPasswordHash(context.NewPassword);
                var saveEntityContext = new SaveEntityContext<User>(context, user);
                await Save(container, saveEntityContext);
            });
        }

        #region overrides
        protected override async Task SaveEntity(IRepositoryContainer container, ISaveEntityContext<User> context)
        {
            await base.SaveEntity(container, context);
            var repository = container.GetRepository<IUserRepository>();
            var entity = context.Entity;
            await repository.SaveUser(entity.Id, entity.Code, entity.PasswordHash, entity.Email, entity.PersonName, entity.PhoneNumber);
            await repository.SaveRoles(entity.Id, entity.Roles);
        }

        protected override async Task InitEntities(IRepositoryContainer container, IInitEntitiesContext<User> context)
        {
            await base.InitEntities(container, context);
            var ids = context.Entities
                .Select(i => i.Id)
                .ToList();
            if (!ids.Any())
            {
                return;
            }

            var repository = container.GetRepository<IUserRepository>();
            var items = (await repository.GetItems(ids)).ToDictionary(i => i.Id);
            var rolesByItem = (await repository.GetRoles(ids)).ToLookup(i => i.Id);
            foreach (var user in context.Entities)
            {
                if (items.TryGetValue(user.Id, out var item))
                {
                    user.Code = item.Code;
                    user.PasswordHash = item.PasswordHash;
                    user.Email = item.Email;
                    user.PersonName = item.PersonName;
                    user.PhoneNumber = item.PhoneNumber;
                }
                user.Roles = rolesByItem[user.Id].Select(i => i.Code).ToArray();
            }
        }

        protected override async Task ValidateEntity(IRepositoryContainer container, ISaveEntityContext<User> context)
        {
            await base.ValidateEntity(container, context);
            var entity = context.Entity;
            if (entity.Code.IsNullOrEmpty())
            {
                throw new ArgumentNullException(nameof(entity.Code));
            }

            if (entity.Email.IsNullOrEmpty())
            {
                throw new ArgumentNullException(nameof(entity.Email));
            }

            if (entity.PersonName.IsNullOrEmpty())
            {
                throw new ArgumentNullException(nameof(entity.PersonName));
            }

            var repository = container.GetRepository<IUserRepository>();
            var userId = await repository.GetUserId(entity.Code);
            if (userId != Guid.Empty && userId != entity.Id)
            {
                throw new ArgumentOutOfRangeException(nameof(entity.Code), "There is a user with the same Code");
            }

            userId = await repository.GetUserId(entity.Email);
            if (userId != Guid.Empty && userId != entity.Id)
            {
                throw new ArgumentOutOfRangeException(nameof(entity.Email), "There is a user with the same Email");
            }
        }
        #endregion

        private string GetConfirmationLink(ICreateUserContext context, User user)
        {
            return $"{context.ConfirmationEmailUrl}/{user.Id}";
        }

        private string GetPasswordHash(string password)
        {
            var md5Hasher = MD5.Create();
            var data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(password ?? string.Empty));
            var stringBuilder = new StringBuilder();
            foreach (var b in data)
            {
                stringBuilder.Append(b.ToString("x2"));
            }
            return stringBuilder.ToString();
        }

        public async Task<ICollection<User>> GetUsers(IGetUsersContext context)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));
            var result = default(ICollection<User>);
            await Execute(async container =>
            {
                var repository = container.GetRepository<IUserRepository>();
                var ids = await repository.GetUserIds(
                    context.Code,
                    context.Email,
                    context.PersonName,
                    context.PhoneNumber,
                    context.Roles);
                var getEntitiesContext = new GetEntitiesContext(context, ids);
                result = await GetEntities(container, getEntitiesContext);
            });
            return result;
        }
    }
}
