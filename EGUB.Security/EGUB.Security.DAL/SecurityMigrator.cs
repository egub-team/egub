﻿using EGUB.Core.Contracts;
using EGUB.Core.Contracts.DAL;
using EGUB.Core.DAL;
using EGUB.Security.Contracts;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace EGUB.Security.DAL
{
    public class SecurityMigrator : Migrator
    {
        public SecurityMigrator(string name, bool isDropIfExists, IContainer container, IConfiguration configuration) : base(name, isDropIfExists, container, configuration)
        {
            AddScript<SecurityMigrator>(CreateUserTable);
            AddScript<SecurityMigrator>(CreateUserRoleTable);
            AddScript<SecurityMigrator>(CreateAdminUser);
        }

        private async Task CreateUserTable(IContext context)
        {
            await context.Execute(SQL.User.Commands.CreateUserTable);
            await context.Execute(SQL.User.Commands.CreateUserReferences);
        }

        private async Task CreateUserRoleTable(IContext context)
        {
            await context.Execute(SQL.User.Commands.CreateUserRoleTable);
            await context.Execute(SQL.User.Commands.CreateUserRoleReferences);
        }

        private async Task CreateAdminUser(IContext context)
        {
            await context.Execute(
                SQL.User.Commands.CreateAdminUser, 
                new 
                { 
                    id = Constants.User.AdminUserId, 
                    entityType = EntityTypes.USER.ToString(),
                    statusValue = UserStatus.Enable.ToString(),
                    code = "Admin",
                    passwordHash = "f83fcbe4503dc79c9d1900ec3a2adba4",
                    email = "jeka.egub@outlook.com",
                    personName = "Eugene Gubriy",
                    phoneNumber = "+380987757119",
                    roleCode = UserRoles.Admin.ToString()
                });
        }
    }
}
