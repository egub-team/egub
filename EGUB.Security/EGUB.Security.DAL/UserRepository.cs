﻿using EGUB.Core.Helpers;
using EGUB.Security.Contracts.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGUB.Security.DAL
{
    internal class UserRepository : Core.DAL.BaseRepository, IUserRepository
    {
        public async Task<ICollection<UserItemData>> GetItems(ICollection<Guid> ids)
        {
            if (!ids.Any())
            {
                return ArraySegment<UserItemData>.Empty;
            }
            return await this.Context
                .Query(
                    r => new UserItemData(
                        Guid.Parse(r["Id"].ToString()!),
                        r.GetValue<string>("Code"),
                        r.GetValue<string>("PasswordHash"),
                        r.GetValue<string>("Email"),
                        r.GetValue<string>("PersonName"),
                        r.GetValue<string>("PhoneNumber")),
                    SQL.User.Commands.GetItems,
                    new { xmlData = this.ConvertToXmlData(ids) });
        }

        public async Task<ICollection<UserRoleItemData>> GetRoles(ICollection<Guid> ids)
        {
            if (!ids.Any())
            {
                return ArraySegment<UserRoleItemData>.Empty;
            }

            return await Context
                .Query(
                    r => new UserRoleItemData(
                        Guid.Parse(r["SecurityUserId"].ToString()!),
                        r["Code"].ToString()),
                    SQL.User.Commands.GetRoles,
                    new { xmlData = this.ConvertToXmlData(ids) });
        }

        public async Task<Guid> GetUserId(string code)
        {
            var result = await this.Context.Query(
                r => Guid.Parse(r["Id"].ToString()!),
                SQL.User.Commands.GetUserByCode, new { code });
            return result.SingleOrDefault();
        }

        public async Task<ICollection<Guid>> GetUserIds(string code, string email, string personName, string phoneNumber, string[] roles)
        {
            var sql = new StringBuilder($@"select u.Id from SecurityUser u")
                .AppendLine();
            var paramsDictionary = new Dictionary<string, object>();
            if (!roles.IsNullOrEmpty())
            {
                foreach (var item in roles.Select((value, index) => new { value, name = $"role{index + 1}" }))
                {
                    paramsDictionary.Add(item.name, item.value);
                }

                sql.AppendLine($"join SecurityUserRole r on r.SecurityUserId = u.Id and r.Code in (@{string.Join(", @", paramsDictionary.Keys)})");
            }
            
            var filterSql = new List<string>();
            if (!code.IsNullOrEmpty())
            {
                paramsDictionary.Add(nameof(code), code);
                filterSql.Add("u.Code like '%' + @code + '%'");
            }
            if (!email.IsNullOrEmpty())
            {
                paramsDictionary.Add(nameof(email), email);
                filterSql.Add("u.Email like '%' + @email + '%'");
            }
            if (!personName.IsNullOrEmpty())
            {
                paramsDictionary.Add(nameof(personName), personName);
                filterSql.Add("u.PersonName like '%' + @personName + '%'");
            }
            if (!phoneNumber.IsNullOrEmpty())
            {
                paramsDictionary.Add(nameof(phoneNumber), phoneNumber);
                filterSql.Add("u.PhoneNumber like '%' + @phoneNumber + '%'");
            }

            if (!filterSql.IsNullOrEmpty())
            {
                sql.AppendLine($@"
where 
    {string.Join(@" and 
    ", filterSql)}");
            }

            var query = sql.ToString();
            var ids = await Context
                .Query(
                    r => r.GetValue<Guid>("Id"),
                    query,
                    paramsDictionary); 
            return ids
                .Distinct()
                .ToList();
        }

        public Task SaveRoles(Guid id, string[] roles) =>
            Context.Execute(SQL.User.Commands.SaveRoles, new { id, xmlData = this.ConvertToXmlData(roles, "code") });

        public Task SaveUser(Guid id, string code, string passwordHash, string email, string personName, string phoneNumber) =>
            Context.Execute(SQL.User.Commands.SaveUser, new { id, code, passwordHash, email, personName, phoneNumber });
    }
}
