﻿update SecurityUser
set 
	Code = @code,
	PasswordHash = @passwordHash,
	Email = @email,
	PersonName = @personName,
	PhoneNumber = @phoneNumber
where Id = @id

if (@@ROWCOUNT > 0)
begin
	return;
end

insert into SecurityUser(Id, Code, PasswordHash, Email, PersonName, PhoneNumber)
values(@id, @code, @passwordHash, @email, @personName, @phoneNumber)