﻿create table SecurityUserRole(
    SecurityUserId uniqueidentifier not null,
    Code nvarchar(50) not null,
    constraint PK_SecurityUserRole primary key clustered
    (
        SecurityUserId asc,
        Code asc
    ));