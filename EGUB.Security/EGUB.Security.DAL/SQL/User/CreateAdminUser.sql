﻿insert into core.Entity(Id, EntityType, Created)
values (@id, @entityType, getdate());

insert into core.EntityStatus(Id, Created, StatusValue)
values(@id, getdate(), @statusValue);

insert into dbo.SecurityUser(Id, Code, PasswordHash, Email, PersonName, PhoneNumber)
values(@id, @code, @passwordHash, @email, @personName, @phoneNumber);

insert into dbo.SecurityUserRole(SecurityUserId, Code)
values(@id, @roleCode);