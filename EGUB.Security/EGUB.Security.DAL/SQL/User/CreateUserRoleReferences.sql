﻿alter table SecurityUserRole  
    add constraint FK_SecurityUserRole_SecurityUser foreign key(SecurityUserId)
    references SecurityUser (Id)
    on delete cascade;