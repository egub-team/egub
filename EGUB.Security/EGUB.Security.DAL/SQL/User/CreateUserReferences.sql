﻿alter table SecurityUser  
    add constraint FK_SecurityUser_Entity foreign key(Id)
    references core.Entity (Id)
    on delete cascade;