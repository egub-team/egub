﻿declare @xml xml = @xmlData

declare @data table (Id uniqueidentifier)
insert into @data(Id)
select x.value('.', 'uniqueidentifier')
from @xml.nodes('id') f(x)

select s.*
from @data i
join SecurityUser s on s.Id = i.Id