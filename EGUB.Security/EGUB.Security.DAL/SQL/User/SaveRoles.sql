﻿declare @xml xml = @xmlData

declare @data table (Code nvarchar(50))
insert into @data(Code)
select x.value('.', 'nvarchar(50)')
from @xml.nodes('code') f(x)

delete s 
from SecurityUserRole s
left join @data i on 
	i.Code = s.Code
where 
	s.SecurityUserId = @id and 
	i.Code is null

insert into SecurityUserRole(SecurityUserId, Code)
select @id, i.Code
from @data i
left join SecurityUserRole s on 
	s.SecurityUserId = @id and
	s.Code = i.Code
where s.SecurityUserId is null
