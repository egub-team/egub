﻿create table SecurityUser(
    Id uniqueidentifier not null,
    Code nvarchar(255) not null,
    PasswordHash nvarchar(50) null,
    Email nvarchar(255) not null,
    PersonName nvarchar(500) not null,
    PhoneNumber nvarchar(50) null,
    constraint PK_SecurityUser primary key clustered 
    (
        Id asc
    ));