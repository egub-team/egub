﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EGUB.Security.DAL.SQL.User {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "17.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Commands {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Commands() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("EGUB.Security.DAL.SQL.User.Commands", typeof(Commands).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to insert into core.Entity(Id, EntityType, Created)
        ///values (@id, @entityType, getdate());
        ///
        ///insert into core.EntityStatus(Id, Created, StatusValue)
        ///values(@id, getdate(), @statusValue);
        ///
        ///insert into dbo.SecurityUser(Id, Code, PasswordHash, Email, PersonName, PhoneNumber)
        ///values(@id, @code, @passwordHash, @email, @personName, @phoneNumber);
        ///
        ///insert into dbo.SecurityUserRole(SecurityUserId, Code)
        ///values(@id, @roleCode);.
        /// </summary>
        internal static string CreateAdminUser {
            get {
                return ResourceManager.GetString("CreateAdminUser", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to alter table SecurityUser  
        ///    add constraint FK_SecurityUser_Entity foreign key(Id)
        ///    references core.Entity (Id)
        ///    on delete cascade;.
        /// </summary>
        internal static string CreateUserReferences {
            get {
                return ResourceManager.GetString("CreateUserReferences", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to alter table SecurityUserRole  
        ///    add constraint FK_SecurityUserRole_SecurityUser foreign key(SecurityUserId)
        ///    references SecurityUser (Id)
        ///    on delete cascade;.
        /// </summary>
        internal static string CreateUserRoleReferences {
            get {
                return ResourceManager.GetString("CreateUserRoleReferences", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to create table SecurityUserRole(
        ///    SecurityUserId uniqueidentifier not null,
        ///    Code nvarchar(50) not null,
        ///    constraint PK_SecurityUserRole primary key clustered
        ///    (
        ///        SecurityUserId asc,
        ///        Code asc
        ///    ));.
        /// </summary>
        internal static string CreateUserRoleTable {
            get {
                return ResourceManager.GetString("CreateUserRoleTable", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to create table SecurityUser(
        ///    Id uniqueidentifier not null,
        ///    Code nvarchar(255) not null,
        ///    PasswordHash nvarchar(50) null,
        ///    Email nvarchar(255) not null,
        ///    PersonName nvarchar(500) not null,
        ///    PhoneNumber nvarchar(50) null,
        ///    constraint PK_SecurityUser primary key clustered 
        ///    (
        ///        Id asc
        ///    ));.
        /// </summary>
        internal static string CreateUserTable {
            get {
                return ResourceManager.GetString("CreateUserTable", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to declare @xml xml = @xmlData
        ///
        ///declare @data table (Id uniqueidentifier)
        ///insert into @data(Id)
        ///select x.value(&apos;.&apos;, &apos;uniqueidentifier&apos;)
        ///from @xml.nodes(&apos;id&apos;) f(x)
        ///
        ///select s.*
        ///from @data i
        ///join SecurityUser s on s.Id = i.Id.
        /// </summary>
        internal static string GetItems {
            get {
                return ResourceManager.GetString("GetItems", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to declare @xml xml = @xmlData
        ///
        ///declare @data table (Id uniqueidentifier)
        ///insert into @data(Id)
        ///select x.value(&apos;.&apos;, &apos;uniqueidentifier&apos;)
        ///from @xml.nodes(&apos;id&apos;) f(x)
        ///
        ///select s.*
        ///from @data i
        ///join SecurityUserRole s on s.SecurityUserId = i.Id.
        /// </summary>
        internal static string GetRoles {
            get {
                return ResourceManager.GetString("GetRoles", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to select Id
        ///from SecurityUser s
        ///where @code in (s.Code, s.Email).
        /// </summary>
        internal static string GetUserByCode {
            get {
                return ResourceManager.GetString("GetUserByCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to declare @xml xml = @xmlData
        ///
        ///declare @data table (Code nvarchar(50))
        ///insert into @data(Code)
        ///select x.value(&apos;.&apos;, &apos;nvarchar(50)&apos;)
        ///from @xml.nodes(&apos;code&apos;) f(x)
        ///
        ///delete s 
        ///from SecurityUserRole s
        ///left join @data i on 
        ///	i.Code = s.Code
        ///where 
        ///	s.SecurityUserId = @id and 
        ///	i.Code is null
        ///
        ///insert into SecurityUserRole(SecurityUserId, Code)
        ///select @id, i.Code
        ///from @data i
        ///left join SecurityUserRole s on 
        ///	s.SecurityUserId = @id and
        ///	s.Code = i.Code
        ///where s.SecurityUserId is null
        ///.
        /// </summary>
        internal static string SaveRoles {
            get {
                return ResourceManager.GetString("SaveRoles", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to update SecurityUser
        ///set 
        ///	Code = @code,
        ///	PasswordHash = @passwordHash,
        ///	Email = @email,
        ///	PersonName = @personName,
        ///	PhoneNumber = @phoneNumber
        ///where Id = @id
        ///
        ///if (@@ROWCOUNT &gt; 0)
        ///begin
        ///	return;
        ///end
        ///
        ///insert into SecurityUser(Id, Code, PasswordHash, Email, PersonName, PhoneNumber)
        ///values(@id, @code, @passwordHash, @email, @personName, @phoneNumber).
        /// </summary>
        internal static string SaveUser {
            get {
                return ResourceManager.GetString("SaveUser", resourceCulture);
            }
        }
    }
}
