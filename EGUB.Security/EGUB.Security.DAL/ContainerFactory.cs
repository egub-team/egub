﻿using Autofac;
using EGUB.Security.Contracts.DAL;

namespace EGUB.Security.DAL
{
    public class ContainerFactory : Module
    {
        public static void RegisterUserRepository(ContainerBuilder builder)
        {
            builder
                .RegisterType<UserRepository>()
                .As<IUserRepository>();
        }

        protected override void Load(ContainerBuilder builder)
        {
            RegisterUserRepository(builder);
        }
    }
}
