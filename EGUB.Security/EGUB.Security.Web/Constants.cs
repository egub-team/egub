﻿using System.Collections.Generic;

namespace EGUB.Security.Web
{
    public static class Constants
    {
        internal const string BearerKey = "authToken";

        public static class Roles
        {
            public const string Guest = nameof(Guest);
            public const string Candidate = nameof(Candidate);
            public const string Member = nameof(Member);
            public const string Admin = nameof(Admin);

            public static readonly IReadOnlyCollection<string> Items = new HashSet<string>
            {
                 Guest,
                 Candidate,
                 Member,
                 Admin
            };
        }

        public static class Statuses
        {
            public const string Enable = nameof(Enable);
            public const string Disable = nameof(Disable);

            public static readonly IReadOnlyCollection<string> Items = new HashSet<string>
            {
                 Enable,
                 Disable
            };
        }
    }
}
