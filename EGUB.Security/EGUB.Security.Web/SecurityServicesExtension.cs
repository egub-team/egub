﻿using EGUB.Core.Web;
using EGUB.Security.Web.Services;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Net.Http;

namespace EGUB.Security.Web
{
    public static class SecurityServicesExtension
    {
        public static void AddSecurity(
            this IServiceCollection services, 
            string baseAddress,
            Func<HttpMessageHandler> messageHandlerFunc = null,
            IDictionary<string, string> customRoles = null)
        {
            services.AddInternalItems(baseAddress, messageHandlerFunc, customRoles);
            services.AddScoped<AuthenticationStateProvider, ApiAuthenticationStateProvider>();
            services.AddScoped<IAuthService, AuthService>();
            services.AddScoped<IUserService, UserService>();
        }

        public static void AddTestSecurity(
            this IServiceCollection services,
            string baseAddress,
            Func<HttpMessageHandler> messageHandlerFunc = null,
            IDictionary<string, string> customRoles = null)
        {
            services.AddInternalItems(baseAddress, messageHandlerFunc, customRoles);

            customRoles = customRoles ?? new Dictionary<string, string>();
            services.AddScoped<AuthenticationStateProvider>(c => new TestAuthenticationStateProvider(customRoles.Keys));
            services.AddScoped<IAuthService, TestAuthService>();
            services.AddScoped<IUserService, TestUserService>();
        }

        private static void AddInternalItems(
            this IServiceCollection services,
            string baseAddress,
            Func<HttpMessageHandler> messageHandlerFunc = null,
            IDictionary<string, string> customRoles = null)
        {
            services.AddCore(baseAddress, messageHandlerFunc);
            services.AddAuthorizationCore();
            services.AddSingleton<IUserRoleProvider>(new UserRoleProvider(customRoles));
        }
    }
}
