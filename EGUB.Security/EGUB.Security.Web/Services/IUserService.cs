﻿using EGUB.Security.Web.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EGUB.Security.Web.Services
{
    public interface IUserService
    {
        Task<ICollection<UserListItemModel>> SearchUsers(SearchUsersModel info);
        Task EditUser(EditUserModel info);
        Task<EditUserModel> GetUserDetails(Guid id);
    }
}
