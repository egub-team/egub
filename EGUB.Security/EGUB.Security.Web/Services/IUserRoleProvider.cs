﻿using System.Collections.Generic;

namespace EGUB.Security.Web.Services
{
    public interface IUserRoleProvider
    {
        IDictionary<string, string> GetUserRoles();
    }
}
