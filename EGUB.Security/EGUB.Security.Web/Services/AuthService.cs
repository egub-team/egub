﻿using EGUB.Core.Web;
using EGUB.Security.Web.Models;
using Microsoft.AspNetCore.Components.Authorization;
using System;
using System.Threading.Tasks;

namespace EGUB.Security.Web.Services
{
    internal class AuthService : BaseSecurityService, IAuthService
    {
        private const string Route = "account";

        private readonly ApiAuthenticationStateProvider _authenticationStateProvider;

        public AuthService(
            IInfrastructure infrastructure,
            AuthenticationStateProvider authenticationStateProvider) : base(infrastructure)
        {
            _authenticationStateProvider = (ApiAuthenticationStateProvider)authenticationStateProvider;
        }

        public async Task<Guid> Register(RegisterModel registerModel)
        {
            var info = new CreateUserModel
            {
                Code = registerModel.Code,
                Email = registerModel.Email,
                Password = registerModel.Password,
                PersonName = registerModel.PersonName,
                ConfirmationEmailUrl = $"{Infrastructure.HostEnvironment.BaseAddress}/confirmEmail"
            };

            return await PostToApi<CreateUserModel, Guid>(Route, info);
        }

        public async Task<LoginResult> Login(LoginModel loginModel)
        {
            var loginResult = new LoginResult();

            var loginUrl = Infrastructure.Configuration["LoginUrl"];
            var info = await PostToUrl<LoginModel, TokenModel>(() => loginUrl, loginModel);

            loginResult.LoginName = info.LoginName;
            loginResult.AccessToken = info.AccessToken;

            await Infrastructure.LocalStorage.SetItemAsync(Constants.BearerKey, loginResult.AccessToken);
            _authenticationStateProvider.MarkUserAsAuthenticated(loginResult.LoginName);

            return loginResult;
        }

        public async Task Logout()
        {
            await Infrastructure.LocalStorage.RemoveItemAsync(Constants.BearerKey);
            _authenticationStateProvider.MarkUserAsLoggedOut();
        }

        public Task ConfirmEmail(ConfirmEmailModel info) =>
            PutToApi($"{Route}/ConfirmEmail", info);
    }

    internal class TestAuthService : IAuthService
    {
        public Task ConfirmEmail(ConfirmEmailModel info) =>
            Task.Delay(TimeSpan.FromSeconds(1));

        public async Task<LoginResult> Login(LoginModel loginModel)
        {
            await Task.Delay(TimeSpan.FromSeconds(1));
            return new LoginResult
            {
                LoginName = loginModel.LoginName,
                AccessToken = Guid.NewGuid().ToString(),
            };
        }

        public Task Logout() =>
            Task.Delay(TimeSpan.FromSeconds(1));

        public async Task<Guid> Register(RegisterModel registerModel)
        {
            await Task.Delay(TimeSpan.FromSeconds(1));
            return Guid.NewGuid();
        }
            
    }
}