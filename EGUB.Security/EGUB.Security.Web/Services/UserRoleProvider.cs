﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EGUB.Security.Web.Services
{
    internal class UserRoleProvider : IUserRoleProvider
    {
        private readonly IDictionary<string, string> _userRoles;

        public UserRoleProvider(IDictionary<string, string> customRoles)
        {
            _userRoles = Constants.Roles.Items
                .ToDictionary(i => i);
            if (customRoles?.Any() ?? false)
            {
                foreach (var role in customRoles)
                {
                    _userRoles.Add(role.Key, role.Value);
                }
            }
        }

        public IDictionary<string, string> GetUserRoles() => _userRoles;
    }
}
