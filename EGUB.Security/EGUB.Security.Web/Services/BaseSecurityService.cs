﻿using EGUB.Core.Web;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace EGUB.Security.Web.Services
{
    public abstract class BaseSecurityService : BaseService
    {
        protected BaseSecurityService(IInfrastructure infrastructure) : base(infrastructure)
        {
        }

        protected override async Task SetDefaultRequestHeaders(HttpRequestHeaders requestHeaders)
        {
            await base.SetDefaultRequestHeaders(requestHeaders);

            var savedToken = await Infrastructure.LocalStorage.GetItemAsync<string>(Constants.BearerKey);
            if (string.IsNullOrEmpty(savedToken))
                return;

            requestHeaders.Authorization = new AuthenticationHeaderValue("bearer", savedToken);
        }
    }
}
