﻿using EGUB.Core.Web;
using EGUB.Core.Web.Models;
using EGUB.Security.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EGUB.Security.Web.Services
{
    internal class UserService : BaseSecurityService, IUserService
    {
        private const string Route = "user";

        public UserService(IInfrastructure infrastructure) : base(infrastructure)
        {
        }

        public Task<EditUserModel> GetUserDetails(Guid id) =>
            GetFromApi<EditUserModel>($"{Route}/{nameof(GetUserDetails)}/{id}");

        public Task EditUser(EditUserModel info) =>
            PutToApi(Route, info);

        public async Task<ICollection<UserListItemModel>> SearchUsers(SearchUsersModel info)
        {
            if (string.IsNullOrEmpty(info.Code) && string.IsNullOrEmpty(info.Email) && string.IsNullOrEmpty(info.PhoneNumber) &&
                string.IsNullOrEmpty(info.PersonName) && !info.Roles.Any())
            {
                return new List<UserListItemModel>();
            }

            return await PostToApi<SearchUsersModel, ICollection<UserListItemModel>>($"{Route}/{nameof(SearchUsers)}", info);
        }
    }

    internal class TestUserService : IUserService
    {
        public Task EditUser(EditUserModel info) =>
            Task.Delay(TimeSpan.FromSeconds(1));

        public async Task<EditUserModel> GetUserDetails(Guid id)
        {
            await Task.Delay(TimeSpan.FromSeconds(1));
            return new EditUserModel
            {
                Id = id,
                Email = "Test@email.net",
                Notes = new[]
                {
                    new NoteModel
                    {
                        Created = DateTime.Now,
                        Note = "Test Note"
                    }
                },
                PersonName = "Test Person Name",
                PhoneNumber = "1234567890",
                StatusValue = UserStatus.Enable,
                StatusNote = "Test Status Note",
                Roles = ["TestAdmin"]
            };
        }

        public async Task<ICollection<UserListItemModel>> SearchUsers(SearchUsersModel info)
        {
            await Task.Delay(TimeSpan.FromSeconds(1));
            return Enumerable.Range(0, 5)
                .Select(i => 
                    new UserListItemModel
                    {
                        Id = Guid.NewGuid(),
                        Code = $"Test code{i}",
                        PersonName = $"Test Person Name{i}"
                    })
                .ToList();
        }
    }
}
