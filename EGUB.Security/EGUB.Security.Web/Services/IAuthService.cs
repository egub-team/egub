﻿using EGUB.Security.Web.Models;
using System;
using System.Threading.Tasks;

namespace EGUB.Security.Web.Services
{
    public interface IAuthService
    {
        Task<LoginResult> Login(LoginModel loginModel);
        Task Logout();
        Task<Guid> Register(RegisterModel registerModel);
        Task ConfirmEmail(ConfirmEmailModel info);
    }
}