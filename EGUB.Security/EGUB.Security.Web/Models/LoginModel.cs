﻿using System.ComponentModel.DataAnnotations;

namespace EGUB.Security.Web.Models
{
    public class LoginModel
    {
        [Required]
        public string LoginName { get; set; }

        [Required]
        public string Password { get; set; }

        public bool RememberMe { get; set; }

        public string[] Roles { get; set; }
    }
}