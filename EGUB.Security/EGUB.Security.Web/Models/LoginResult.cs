﻿namespace EGUB.Security.Web.Models
{
    public class LoginResult
    {
        public string AccessToken { get; set; }
        public string LoginName { get; set; }
    }
}