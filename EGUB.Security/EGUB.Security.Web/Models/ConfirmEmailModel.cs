﻿using System;

namespace EGUB.Security.Web.Models
{
    public class ConfirmEmailModel
    {
        public Guid Id { get; set; }
        public string Password { get; set; }
    }
}
