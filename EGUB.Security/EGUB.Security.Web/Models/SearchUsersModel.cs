﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EGUB.Security.Web.Models
{
    public class SearchUsersModel
    {
        public string Code { get; set; } = string.Empty;
        public string Email { get; set; } = string.Empty;
        public string PersonName { get; set; } = string.Empty;
        [Display(Name = "Phone Number")]
        [Range(0, Int64.MaxValue, ErrorMessage = "Contact number should not contain characters")]
        public string PhoneNumber { get; set; } = string.Empty;
        public string[] Roles { get; set; } = new string[] { };
    }
}
