﻿namespace EGUB.Security.Web.Models
{
    public enum UserStatus
    {
        Enable,
        Disable
    }
}
