﻿namespace EGUB.Security.Web.Models
{
    public class CreateUserModel
    {
        public string PersonName { get; set; }
        public string Email { get; set; }
        public string Code { get; set; }
        public string Password { get; set; }
        public string ConfirmationEmailUrl { get; set; }
    }
}
