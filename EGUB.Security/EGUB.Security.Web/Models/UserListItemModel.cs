﻿using System;

namespace EGUB.Security.Web.Models
{
    public class UserListItemModel
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string PersonName { get; set; }
    }
}
