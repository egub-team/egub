﻿using EGUB.Core.Web.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EGUB.Security.Web.Models
{
    public class EditUserModel
    {
        private static readonly IDictionary<bool, UserStatus> mapUserStatus = new Dictionary<bool, UserStatus>
        {
            { true, UserStatus.Enable },
            { false, UserStatus.Disable },
        };

        public Guid Id { get; set; }

        public ICollection<NoteModel> Notes { get; set; } = new List<NoteModel>();

        [Display(Name = "Status Note")]
        public string StatusNote { get; set; } = string.Empty;

        public UserStatus StatusValue { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; } = string.Empty;

        [Required]
        [Display(Name = "Person Name")]
        public string PersonName { get; set; } = string.Empty;

        [Display(Name = "Phone Number")]
        [RegularExpression(@"^\+?\(?([0-9]{2})\)?[-. ]?\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Entered phone format is not valid.")]
        public string PhoneNumber { get; set; } = string.Empty;

        [Required, MinLength(1)]
        public string[] Roles { get; set; } = new string[] { };
    }
}
