﻿namespace EGUB.Security.Web.Models
{
    public class TokenModel
    {
        public string AccessToken { get; set; }
        public string LoginName { get; set; }
    }
}
