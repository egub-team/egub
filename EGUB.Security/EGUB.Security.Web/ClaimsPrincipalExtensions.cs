﻿using EGUB.Security.Web.Models;
using System.Linq;
using System.Security.Claims;

namespace EGUB.Security.Web
{
    public static class ClaimsPrincipalExtensions
    {
        public static string GetEmail(this ClaimsPrincipal principal)
        {
            return principal?.Claims
                .Where(i => i.Type == nameof(CreateUserModel.Email))
                .Select(i => i.Value)
                .FirstOrDefault();
        }
    }
}
