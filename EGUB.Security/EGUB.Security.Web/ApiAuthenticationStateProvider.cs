﻿using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text.Json;
using System.Threading.Tasks;

namespace EGUB.Security.Web
{
    internal class ApiAuthenticationStateProvider : AuthenticationStateProvider
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IConfiguration _configuration;
        private readonly ILocalStorageService _localStorage;

        public ApiAuthenticationStateProvider(
            IHttpClientFactory httpClientFactory,
            IConfiguration configuration,
            ILocalStorageService localStorage)
        {
            _httpClientFactory = httpClientFactory;
            _configuration = configuration;
            _localStorage = localStorage;
        }

        public override async Task<AuthenticationState> GetAuthenticationStateAsync()
        {
            var savedToken = await _localStorage.GetItemAsync<string>(Constants.BearerKey);

            if (string.IsNullOrWhiteSpace(savedToken))
            {
                return new AuthenticationState(new ClaimsPrincipal(new ClaimsIdentity()));
            }

            using var httpClient = GetHttpClient(savedToken);

            var response = await httpClient.GetAsync($"{_configuration["ApiUrl"]}/account");
            if (!response.IsSuccessStatusCode)
            {
                MarkUserAsLoggedOut();
                return new AuthenticationState(new ClaimsPrincipal(new ClaimsIdentity()));
            }

            return new AuthenticationState(new ClaimsPrincipal(new ClaimsIdentity(ParseClaimsFromJwt(savedToken), "jwt")));
        }

        public void MarkUserAsAuthenticated(string email)
        {
            var authenticatedUser = new ClaimsPrincipal(new ClaimsIdentity(new[] { new Claim(ClaimTypes.Name, email) }, "apiauth"));
            var authState = Task.FromResult(new AuthenticationState(authenticatedUser));
            NotifyAuthenticationStateChanged(authState);
        }

        public void MarkUserAsLoggedOut()
        {
            var anonymousUser = new ClaimsPrincipal(new ClaimsIdentity());
            var authState = Task.FromResult(new AuthenticationState(anonymousUser));
            NotifyAuthenticationStateChanged(authState);
        }

        private IEnumerable<Claim> ParseClaimsFromJwt(string jwt)
        {
            var claims = new List<Claim>();
            var payload = jwt.Split('.')[1];
            var jsonBytes = ParseBase64WithoutPadding(payload);
            var keyValuePairs = JsonSerializer.Deserialize<Dictionary<string, object>>(jsonBytes);

            keyValuePairs.TryGetValue(ClaimTypes.Role, out object roles);

            if (roles != null)
            {
                if (roles.ToString().Trim().StartsWith("["))
                {
                    var parsedRoles = JsonSerializer.Deserialize<string[]>(roles.ToString());

                    foreach (var parsedRole in parsedRoles)
                    {
                        claims.Add(new Claim(ClaimTypes.Role, parsedRole));
                    }
                }
                else
                {
                    claims.Add(new Claim(ClaimTypes.Role, roles.ToString()));
                }

                keyValuePairs.Remove(ClaimTypes.Role);
            }

            claims.AddRange(keyValuePairs.Select(kvp => new Claim(kvp.Key, kvp.Value.ToString())));

            return claims;
        }

        private byte[] ParseBase64WithoutPadding(string base64)
        {
            switch (base64.Length % 4)
            {
                case 2: base64 += "=="; break;
                case 3: base64 += "="; break;
            }
            return Convert.FromBase64String(base64);
        }

        private HttpClient GetHttpClient(string authToken)
        {
            HttpClient httpClient = _httpClientFactory.CreateClient(Options.DefaultName);
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", authToken);
            return httpClient;
        }
    }

    internal class TestAuthenticationStateProvider : AuthenticationStateProvider
    {
        private readonly Lazy<AuthenticationState> _lazyAuthenticationState;

        public TestAuthenticationStateProvider(ICollection<string> roles)
        {
            _lazyAuthenticationState = new Lazy<AuthenticationState>(() => GetTestAuthenticationState(roles));
        }

        public override Task<AuthenticationState> GetAuthenticationStateAsync() =>
            Task.FromResult(_lazyAuthenticationState.Value);

        private AuthenticationState GetTestAuthenticationState(ICollection<string> roles) =>
            new AuthenticationState(new ClaimsPrincipal(GetTestClaimsIdentity(roles)));

        private ClaimsIdentity GetTestClaimsIdentity(ICollection<string> roles)
        {
            var claims = new List<Claim>
            {
                new(ClaimsIdentity.DefaultNameClaimType, "testuser"),
                new("UserId", Guid.NewGuid().ToString()),
                new("Email", "testuser@email.com"),
                new("PersonName", "Test Person")
            };

            claims.AddRange(Constants.Roles.Items.Select(i => new Claim(ClaimsIdentity.DefaultRoleClaimType, i)));
            claims.AddRange(roles.Select(i => new Claim(ClaimsIdentity.DefaultRoleClaimType, i)));

            var claimsIdentity = new ClaimsIdentity(
                claims, "Token", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            return claimsIdentity;
        }
    }
}