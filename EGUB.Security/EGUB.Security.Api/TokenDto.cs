﻿namespace EGUB.Security.Api
{
    public class TokenDto
    {
        public string AccessToken { get; set; }
        public string LoginName { get; set; }
    }
}
