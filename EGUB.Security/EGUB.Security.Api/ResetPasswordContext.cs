﻿using EGUB.Core.Api;
using EGUB.Security.Contracts.BLL;
using Microsoft.AspNetCore.Http;
using System;

namespace EGUB.Security.Api
{
    internal class ResetPasswordContext : OperationContext, IResetPasswordContext
    {
        private readonly ResetPasswordDto _dto;

        public ResetPasswordContext(ResetPasswordDto dto, HttpContext httpContext) : base(httpContext)
        {
            _dto = dto;
        }

        public Guid Id => _dto.Id;

        public string NewPassword => _dto.NewPassword;
    }
}
