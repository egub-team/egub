﻿using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using System.Threading.Tasks;

namespace EGUB.Security.Api
{
    public interface IIdentityProvider
    {
        Task<ClaimsIdentity> GetIdentity(HttpContext httpContext, LoginDto info);
    }
}
