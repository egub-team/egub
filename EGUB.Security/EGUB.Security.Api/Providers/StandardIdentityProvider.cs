﻿using EGUB.Security.Contracts.BLL;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace EGUB.Security.Api.Providers
{
    internal class StandardIdentityProvider : IStandardIdentityProvider
    {
        private readonly IUserService _userService;

        public StandardIdentityProvider(IUserService userService)
        {
            _userService = userService;
        }

        public async Task<ClaimsIdentity> GetIdentity(HttpContext httpContext, LoginDto info)
        {
            var context = new GetUserContext(httpContext, info.LoginName);
            var user = await _userService.GetUser(context);

            // if user is not found
            if (user == null)
                return default;

            if (info.Roles?.Any() ?? false)
            {
                if (info.Roles.All(i => !user.Roles.Contains(i)))
                    return default;
            }

            if (!await _userService.CheckPassword(user.PasswordHash, info.Password))
                return default;

            var claims = new List<Claim>
            {
                new(ClaimsIdentity.DefaultNameClaimType, user.Code),
                new("UserId", user.Id.ToString()),
                new(nameof(user.Email), user.Email),
                new(nameof(user.PersonName), user.PersonName)
            };
            claims.AddRange(user.Roles.Select(r => new Claim(ClaimsIdentity.DefaultRoleClaimType, r)));

            var claimsIdentity = new ClaimsIdentity(
                claims, "Token", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            return claimsIdentity;
        }
    }
}
