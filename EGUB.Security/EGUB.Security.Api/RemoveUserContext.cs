﻿using EGUB.Core.Api;
using EGUB.Core.Contracts.BLL;
using Microsoft.AspNetCore.Http;
using System;

namespace EGUB.Security.Api
{
    public class RemoveUserContext : OperationContext, IRemoveEntityContext
    {
        public RemoveUserContext(Guid id, HttpContext httpContext) : base(httpContext)
        {
            Id = id;
        }

        public Guid Id { get; }
    }
}
