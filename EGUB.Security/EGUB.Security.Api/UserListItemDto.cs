﻿using System;

namespace EGUB.Security.Api
{
    public class UserListItemDto
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string PersonName { get; set; }
    }
}