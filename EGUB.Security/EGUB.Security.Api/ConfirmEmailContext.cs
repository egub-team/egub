﻿using EGUB.Core.Api;
using EGUB.Security.Contracts.BLL;
using Microsoft.AspNetCore.Http;
using System;

namespace EGUB.Security.Api
{
    internal class ConfirmEmailContext : OperationContext, IConfirmEmailContext
    {
        private readonly ConfirmEmailDto _dto;

        public ConfirmEmailContext(ConfirmEmailDto dto, HttpContext httpContext) : base(httpContext)
        {
            _dto = dto;
        }

        public Guid Id => _dto.Id;

        public string Password => _dto.Password;
    }
}
