﻿using System.Collections.Generic;

namespace EGUB.Security.Api
{
    public class LoginDto
    {
        public string LoginName { get; set; }
        public string Password { get; set; }
        public ICollection<string> Roles { get; set; }
    }
}
