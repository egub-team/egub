﻿namespace EGUB.Security.Api
{
    public class CreateUserDto
    {
        public string Code { get; set; }

        public string Password { get; set; }

        public string Email { get; set; }

        public string PersonName { get; set; }

        public string PhoneNumber { get; set; }

        public string Note { get; set; }

        public string ConfirmationEmailUrl { get; set; }

        public string[] Roles { get; set; }
    }
}
