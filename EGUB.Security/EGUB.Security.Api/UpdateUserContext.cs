﻿using EGUB.Core.Api;
using EGUB.Security.Contracts;
using EGUB.Security.Contracts.BLL;
using Microsoft.AspNetCore.Http;
using System;

namespace EGUB.Security.Api
{
    internal class UpdateUserContext : OperationContext, IUpdateUserContext
    {
        public UpdateUserContext(UpdateAccountDto dto, HttpContext httpContext) : base(httpContext)
        {
            Id = GetCurrentUserId();
            Note = dto.Note;
            StatusNote = dto.StatusNote;
            StatusValue = dto.StatusValue;
            Email = dto.Email;
            PersonName = dto.PersonName;
            PhoneNumber = dto.PhoneNumber;
            Roles = dto.Roles;
        }

        public UpdateUserContext(UpdateUserDto dto, HttpContext httpContext) : this((UpdateAccountDto)dto, httpContext)
        {
            Id = dto.Id;
        }

        public Guid Id { get; } 
        public string Note { get; }
        public string StatusNote { get; }
        public UserStatus StatusValue { get; }
        public string Email { get; }
        public string PersonName { get; }
        public string PhoneNumber { get; }
        public string[] Roles { get; }
    }
}
