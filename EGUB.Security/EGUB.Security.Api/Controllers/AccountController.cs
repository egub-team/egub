﻿using EGUB.Core.Api;
using EGUB.Security.Contracts;
using EGUB.Security.Contracts.BLL;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;

namespace EGUB.Security.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AccountController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IIdentityProvider _identityProvider;
        private readonly IConfiguration _configuration;

        public AccountController(
            IUserService userService,
            IIdentityProvider identityProvider,
            IConfiguration configuration)
        {
            _userService = userService;
            _identityProvider = identityProvider;
            _configuration = configuration;
        }

        [HttpPost("/Login")]
        [ProducesResponseType(typeof(TokenDto), StatusCodes.Status200OK)]        
        public async Task<IActionResult> Login([FromBody] LoginDto info)
        {
            try
            {
                var identity = await _identityProvider.GetIdentity(HttpContext, info);
                if (identity == null)
                {
                    throw new SecurityTokenException("Invalid username or password.");
                }

                var now = DateTime.UtcNow;
                // create JWT-token
                var jwt = new JwtSecurityToken(
                    issuer: AuthOptions.ISSUER,
                    audience: AuthOptions.AUDIENCE,
                    notBefore: now,
                    claims: identity.Claims,
                    expires: now.Add(TimeSpan.FromHours(_configuration.GetValue<int>("SecurityTokenExpireHours"))),
                    signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
                var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
                return Ok(new TokenDto
                {
                    AccessToken = encodedJwt,
                    LoginName = identity.Name
                });
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Authorize]
        [ProducesResponseType(typeof(UserDto), StatusCodes.Status200OK)]
        public Task<IActionResult> Get() => 
            this.Get(async () =>
            {
                var operationContext = new OperationContext(HttpContext);
                var context = new GetEntitiesContext(HttpContext, new[] { operationContext.GetCurrentUserId() });
                var users = await _userService.GetEntities(context);
                var user = users.Single();
                return new UserDto
                {
                    Id = user.Id,
                    Code = user.Code,
                    Status = user.StatusInfo?.Status.ToString(),
                    PersonName = user.PersonName,
                    Roles = user.Roles,
                };
            });

        [HttpPost]
        [ProducesResponseType(typeof(Guid), StatusCodes.Status200OK)]
        public Task<IActionResult> Post([FromBody] CreateUserDto dto) =>
            this.Get(() =>
            {
                var roles = new[] { UserRoles.Candidate.ToString() };
                var context = new CreateUserContext(dto, roles, HttpContext);
                return _userService.CreateUser(context);
            });

        [HttpPut]
        [Authorize]
        public Task<IActionResult> Put([FromBody] UpdateAccountDto dto) =>
            this.Execute(() => _userService.UpdateUser(new UpdateUserContext(dto, HttpContext)));

        [HttpPut(nameof(ChangePassword))]
        [Authorize]
        public Task<IActionResult> ChangePassword([FromBody] ChangePasswordDto dto) =>
            this.Execute(() => _userService.ChangePassword(new ChangePasswordContext(dto, HttpContext)));

        [HttpPut(nameof(ConfirmEmail))]
        public Task<IActionResult> ConfirmEmail([FromBody] ConfirmEmailDto dto) =>
            this.Execute(() => _userService.ConfirmEmail(new ConfirmEmailContext(dto, HttpContext)));
    }
}
