﻿using EGUB.Core.Api;
using EGUB.Core.Helpers;
using EGUB.Security.Contracts;
using EGUB.Security.Contracts.BLL;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EGUB.Security.Api.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles = nameof(UserRoles.Admin))]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly ILogger _logger;
        private readonly IUserService _userService;

        public UserController(ILogger<UserController> logger, IUserService userService)
        {
            _logger = logger;
            _userService = userService;
        }

        [HttpGet($"{nameof(GetUserDetails)}/{{id:guid}}")]
        [ProducesResponseType(typeof(UserDetailsDto), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetUserDetails([FromRoute] Guid id)
        {
            try
            {
                var context = new GetEntitiesContext(HttpContext, new[] { id });
                var users = await _userService.GetEntities(context);
                var user = users.SingleOrDefault();
                if (user == null)
                    return Ok(default(UserDetailsDto));

                return Ok(new UserDetailsDto
                {
                    Id = user.Id,
                    Notes = user.Notes,
                    StatusNote = user.StatusInfo.Note,
                    StatusValue = user.StatusInfo.Status,
                    Email = user.Email,
                    PersonName = user.PersonName,
                    PhoneNumber = user.PhoneNumber,
                    Roles = user.Roles
                });
            }
            catch (Exception ex) 
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost(nameof(SearchUsers))]
        [ProducesResponseType(typeof(ICollection<UserListItemDto>), StatusCodes.Status200OK)]
        public Task<IActionResult> SearchUsers([FromBody] SearchUsersDto dto) =>
            this.Get(() => _userService
                .GetUsers(new GetUsersContext(dto, HttpContext))
                .SelectAsItemsAsync(i => 
                    new UserListItemDto
                    {
                        Id = i.Id,
                        Code = i.Code,
                        PersonName = i.PersonName,
                    }));

        [HttpPost]
        [ProducesResponseType(typeof(Guid), StatusCodes.Status200OK)]
        public Task<IActionResult> Post([FromBody] CreateUserDto dto) =>
            this.Get(() => _userService.CreateUser(new CreateUserContext(dto, dto.Roles, HttpContext)));

        [HttpPut]
        public Task<IActionResult> Put([FromBody] UpdateUserDto dto) =>
            this.Execute(() => _userService.UpdateUser(new UpdateUserContext(dto, HttpContext)));

        [HttpPut(nameof(ResetPassword))]
        public Task<IActionResult> ResetPassword([FromBody] ResetPasswordDto dto) =>
            this.Execute(() => _userService.ResetPassword(new ResetPasswordContext(dto, HttpContext)));

        [HttpDelete]
        public Task<IActionResult> Delete(Guid id) =>
            this.Execute(() => _userService.Remove(new RemoveUserContext(id, HttpContext)));
    }
}
