﻿using Autofac;
using EGUB.Security.Api.Controllers;
using EGUB.Security.Api.Providers;

namespace EGUB.Security.Api
{
    public class ContainerFactory : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .RegisterType<AccountController>()
                .AsSelf()
                .InstancePerLifetimeScope();
            builder
                .RegisterType<UserController>()
                .AsSelf()
                .InstancePerLifetimeScope();

            builder
                .RegisterType<StandardIdentityProvider>()
                .As<IStandardIdentityProvider>()
                .InstancePerLifetimeScope();

            builder
                .Register<IIdentityProvider>(c =>
                    c.IsRegistered<ICustomIdentityProvider>()
                        ? c.Resolve<ICustomIdentityProvider>()
                        : c.Resolve<IStandardIdentityProvider>())
                .InstancePerLifetimeScope();
        }
    }
}
