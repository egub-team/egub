﻿using EGUB.Core.Api;
using EGUB.Security.Contracts.BLL;
using Microsoft.AspNetCore.Http;

namespace EGUB.Security.Api
{
    internal class CreateUserContext : OperationContext, ICreateUserContext
    {
        private readonly CreateUserDto _dto;
        private readonly string[] _roles;

        public CreateUserContext(CreateUserDto dto, string[] roles, HttpContext httpContext) : base(httpContext)
        {
            _dto = dto;
            _roles = roles;
        }

        public string Code => _dto.Code;

        public string Password => _dto.Password;

        public string Email => _dto.Email;

        public string PersonName => _dto.PersonName;

        public string PhoneNumber => _dto.PhoneNumber;

        public string[] Roles => _roles;

        public string Note => _dto.Note;

        public string ConfirmationEmailUrl => _dto.ConfirmationEmailUrl;
    }
}
