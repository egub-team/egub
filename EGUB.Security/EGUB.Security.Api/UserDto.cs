﻿using System;

namespace EGUB.Security.Api
{
    public class UserDto
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Status { get; set; }
        public string PersonName { get; set; }
        public string[] Roles { get; set; }
    }
}
