﻿using EGUB.Core.Api;
using EGUB.Security.Contracts.BLL;
using Microsoft.AspNetCore.Http;

namespace EGUB.Security.Api
{
    public class GetUserContext : OperationContext, IGetUserContext
    {
        public GetUserContext(HttpContext httpContext, string code) : base(httpContext)
        {
            Code = code;
        }

        public string Code { get; }
    }
}
