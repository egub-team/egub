﻿using System;

namespace EGUB.Security.Api
{
    public class ConfirmEmailDto
    {
        public Guid Id { get; set; }

        public string Password { get; set; }
    }
}
