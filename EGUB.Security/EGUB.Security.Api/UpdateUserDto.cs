﻿using EGUB.Security.Contracts;
using System;

namespace EGUB.Security.Api
{
    public class UpdateUserDto : UpdateAccountDto
    {
        public Guid Id { get; set; }
    }
}
