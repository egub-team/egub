﻿using EGUB.Core.Api;
using EGUB.Security.Contracts.BLL;
using Microsoft.AspNetCore.Http;
using System;

namespace EGUB.Security.Api
{
    internal class ChangePasswordContext : OperationContext, IChangePasswordContext
    {
        private readonly ChangePasswordDto _dto;

        public ChangePasswordContext(ChangePasswordDto dto, HttpContext httpContext) :base(httpContext)
        {
            _dto = dto;
        }

        public Guid Id => GetCurrentUserId();

        public string CurrentPassword => _dto.CurrentPassword;

        public string NewPassword => _dto.CurrentPassword;
    }
}
