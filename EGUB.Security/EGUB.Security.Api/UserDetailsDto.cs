﻿using EGUB.Core.Contracts;
using EGUB.Security.Contracts;
using System;
using System.Collections.Generic;

namespace EGUB.Security.Api
{
    public class UserDetailsDto
    {
        public Guid Id { get; set; }
        public ICollection<NoteInfo> Notes { get; set; }
        public string StatusNote { get; set; }
        public UserStatus StatusValue { get; set; }
        public string Email { get; set; }
        public string PersonName { get; set; }
        public string PhoneNumber { get; set; }
        public string[] Roles { get; set; }
    }
}
