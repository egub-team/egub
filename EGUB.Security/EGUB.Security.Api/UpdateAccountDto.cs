﻿using EGUB.Security.Contracts;

namespace EGUB.Security.Api
{
    public class UpdateAccountDto
    {
        public string Note { get; set; }

        public string StatusNote { get; set; }

        public UserStatus StatusValue { get; set; }

        public string Email { get; set; }

        public string PersonName { get; set; }

        public string PhoneNumber { get; set; }

        public string[] Roles { get; set; }
    }
}
