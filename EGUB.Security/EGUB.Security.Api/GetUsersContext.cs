﻿using EGUB.Core.Api;
using EGUB.Security.Contracts.BLL;
using Microsoft.AspNetCore.Http;

namespace EGUB.Security.Api
{
    public class GetUsersContext : OperationContext, IGetUsersContext
    {
        public GetUsersContext(SearchUsersDto dto, HttpContext httpContext) : base(httpContext)
        {
            Code = dto.Code;
            Email = dto.Email;
            PersonName = dto.PersonName;
            PhoneNumber = dto.PhoneNumber;
            Roles = dto.Roles;
        }

        public string Code { get; }

        public string Email { get; }

        public string PersonName { get; }

        public string PhoneNumber { get; }

        public string[] Roles { get; }
    }
}
