﻿namespace EGUB.Security.Api
{
    public class SearchUsersDto
    {
        public string Code { get; set; }
        public string Email { get; set; }
        public string PersonName { get; set; }
        public string PhoneNumber { get; set; }
        public string[] Roles { get; set; }
    }
}
