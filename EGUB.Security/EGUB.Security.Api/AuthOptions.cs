﻿using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace EGUB.Security.Api
{
    public class AuthOptions
    {
        public const string ISSUER = "EGUB.Security.Api"; // издатель токена
        public const string AUDIENCE = "EGUB.Security"; // потребитель токена
        private const string KEY = "71ED6584-4CC1-4D30-B42C-DB3D721A55B7";   // ключ для шифрации
        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}
