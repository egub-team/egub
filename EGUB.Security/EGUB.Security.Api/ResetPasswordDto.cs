﻿using System;

namespace EGUB.Security.Api
{
    public class ResetPasswordDto
    {
        public Guid Id { get; set; }

        public string NewPassword { get; set; }
    }
}
