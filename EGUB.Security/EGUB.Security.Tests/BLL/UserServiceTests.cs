﻿using Autofac;
using EGUB.Core.BLL;
using EGUB.Core.Contracts.DAL;
using EGUB.Core.DAL;
using EGUB.Security.Contracts;
using EGUB.Security.Contracts.BLL;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Threading.Tasks;
using IContainer = EGUB.Core.Contracts.IContainer;

namespace EGUB.Security.Tests.BLL
{
    [TestClass]
    public class UserServiceTests 
    {
        [TestMethod]
        public Task Test() =>
           Core.Common.ContainerFactory.Execute(
                builder =>
                {
                    builder
                        .Register(c => new DAL.SecurityMigrator(nameof(UserServiceTests), true, c.Resolve<IContainer>(), c.Resolve<IConfiguration>()))
                        .Named<IMigrator>(nameof(UserServiceTests))
                        .InstancePerLifetimeScope();

                    ContainerFactory.RegisterContextContainer(builder);
                    ContainerFactory.RegisterRepositoryContainer(builder);
                    ContainerFactory.RegisterEntityRepository(builder);
                    DAL.ContainerFactory.RegisterUserRepository(builder);
                    Security.BLL.ContainerFactory.RegisterUserService(builder, nameof(UserServiceTests));
                },
                async container =>
                {
                    var migrator = container.GetItem<IMigrator>(nameof(UserServiceTests));
                    await migrator.Execute();
                    var userService = container.GetItem<IUserService>();
                    var createInfo = new CreateUserContextTest()
                    {
                        Note = "TestNote",
                        Code = "testuser",
                        Email = "admin@egub.com",
                        Password = "123",
                        PersonName = "User Test",
                        PhoneNumber = "1234567",
                        Roles = new[] { UserRoles.Admin.ToString() }
                    };

                    var userId = await userService.CreateUser(createInfo);

                    var getEntitiesContext = new GetEntitiesContext(createInfo, new[] { userId });
                    var entities = await userService.GetEntities(getEntitiesContext);
                    var user = entities.Single();
                    Assert.AreEqual(user.Notes.OrderByDescending(i => i.Created).First().Note, createInfo.Note);
                    Assert.AreEqual(user.StatusInfo.Status, UserStatus.Enable);
                    Assert.IsNull(user.StatusInfo.Note);
                    Assert.AreEqual(user.Code, createInfo.Code);
                    Assert.AreEqual(user.Email, createInfo.Email);
                    Assert.IsNotNull(user.PasswordHash);
                    Assert.AreEqual(user.PersonName, createInfo.PersonName);
                    Assert.AreEqual(user.PhoneNumber, createInfo.PhoneNumber);
                    Assert.AreEqual(user.Roles[0], createInfo.Roles[0]);

                    var updateInfo = new UpdateUserContextTest
                    {
                        Id = user.Id,
                        Note = "UpdateNote",
                        StatusNote = "StatusNote",
                        StatusValue = UserStatus.Disable,
                        Email = "admin.update@egub.com",
                        PersonName = "Update person name",
                        PhoneNumber = "777",
                        Roles = new[] { UserRoles.Guest.ToString() }
                    };

                    await userService.UpdateUser(updateInfo);

                    var getUsersContext = new GetUsersContextTest
                    { 
                        Code = "test%",
                        Email = updateInfo.Email,
                        PersonName = updateInfo.PersonName,
                        PhoneNumber = updateInfo.PhoneNumber,
                        Roles = updateInfo.Roles
                    };

                    var users = await userService.GetUsers(getUsersContext);
                    user = users.Single();
                    Assert.AreEqual(user.Notes.OrderByDescending(o => o.Created).First().Note, updateInfo.Note);
                    Assert.AreEqual(user.StatusInfo.Status, UserStatus.Disable);
                    Assert.AreEqual(user.StatusInfo.Note, updateInfo.StatusNote);
                    Assert.AreEqual(user.Email, updateInfo.Email);
                    Assert.AreEqual(user.PersonName, updateInfo.PersonName);
                    Assert.AreEqual(user.PhoneNumber, updateInfo.PhoneNumber);
                    Assert.AreEqual(user.Roles[0], updateInfo.Roles[0]);

                    var removeEntityContext = new RemoveEntityContext(updateInfo, userId);
                    await userService.Remove(removeEntityContext);
                });

        [TestMethod]
        public Task CreateTest() =>
           Core.Common.ContainerFactory.Execute(
                builder =>
                {
                    builder
                        .Register(c => new DAL.SecurityMigrator(nameof(UserServiceTests), true, c.Resolve<IContainer>(), c.Resolve<IConfiguration>()))
                        .Named<IMigrator>(nameof(UserServiceTests))
                        .InstancePerLifetimeScope();

                    ContainerFactory.RegisterContextContainer(builder);
                    ContainerFactory.RegisterRepositoryContainer(builder);
                    ContainerFactory.RegisterEntityRepository(builder);
                    DAL.ContainerFactory.RegisterUserRepository(builder);
                    Security.BLL.ContainerFactory.RegisterUserService(builder, nameof(UserServiceTests));
                },
                async container =>
                {
                    var migrator = container.GetItem<IMigrator>(nameof(UserServiceTests));
                    await migrator.Execute();
                    var userService = container.GetItem<IUserService>();
                    var createInfo = new CreateUserContextTest()
                    {
                        Note = null,
                        Code = "testuser",
                        Email = "admin@egub.com",
                        Password = "123",
                        PersonName = "user",
                        PhoneNumber = null,
                        Roles = new string[] { UserRoles.Member.ToString() } 
                    };

                    var userId = await userService.CreateUser(createInfo);

                    var getEntitiesContext = new GetEntitiesContext(createInfo, new[] { userId });
                    var entities = await userService.GetEntities(getEntitiesContext);
                    var user = entities.Single();
                    //Assert.AreEqual(user.Notes.OrderByDescending(i => i.Created).First().Note, createInfo.Note);
                    Assert.AreEqual(user.StatusInfo.Status, UserStatus.Enable);
                    Assert.IsNull(user.StatusInfo.Note);
                    Assert.AreEqual(user.Code, createInfo.Code);
                    Assert.AreEqual(user.Email, createInfo.Email);
                    Assert.IsNotNull(user.PasswordHash);
                    Assert.AreEqual(user.PersonName, createInfo.PersonName);
                    Assert.AreEqual(user.PhoneNumber, createInfo.PhoneNumber);
                    Assert.AreEqual(user.Roles[0], createInfo.Roles[0]);
                });
    }

    internal class GetUsersContextTest : IGetUsersContext
    {
        public string Code { get; set; }

        public string Email { get; set; }

        public string PersonName { get; set; }

        public string PhoneNumber { get; set; }

        public string[] Roles { get; set; }

        public Guid GetCurrentUserId() => Guid.Empty;
    }

    internal class CreateUserContextTest : ICreateUserContext
    {
        public string Code { get; set; }

        public string Password { get; set; }

        public string Email { get; set; }

        public string PersonName { get; set; }

        public string PhoneNumber { get; set; }

        public string[] Roles { get; set; }

        public string Note { get; set; }

        public string ConfirmationEmailUrl { get; set; }

        public Guid GetCurrentUserId() => Guid.Empty;
    }

    internal class UpdateUserContextTest : IUpdateUserContext
    {
        public Guid Id { get; set; }

        public string Note { get; set; }

        public string StatusNote { get; set; }

        public UserStatus StatusValue { get; set; }

        public string Email { get; set; }

        public string PersonName { get; set; }

        public string PhoneNumber { get; set; }

        public string[] Roles { get; set; }

        public Guid GetCurrentUserId() => Guid.Empty;
    }
}
