﻿using Microsoft.AspNetCore.Components;
using System;

namespace EGUB.Security.Client.Pages
{
    public partial class ConfirmEmail
    {
        [Parameter]
        public Guid UserId { get; set; }
    }
}
