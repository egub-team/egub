﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;

namespace EGUB.Security.Client.Components
{
    public partial class LoginDisplayComponent
    {
        [Inject]
        private NavigationManager NavigationManager { get; set; } = default!;
        [Inject]
        private IWebAssemblyHostEnvironment HostEnvironment { get; set; } = default!;

        private bool IsCurrentRegistration() => NavigationManager.Uri == $"{HostEnvironment.BaseAddress}registerUser";
        private bool IsCurrentLogin() => NavigationManager.Uri == $"{HostEnvironment.BaseAddress}login";
    }
}
