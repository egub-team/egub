﻿using EGUB.Security.Web.Models;
using EGUB.Security.Web.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using EGUB.Core.Web.Components;
using System.Threading.Tasks;
using System;

namespace EGUB.Security.Client.Components
{
    public partial class LoginComponent
    {
        [Inject]
        private IAuthService AuthService { get; set; } = default!;
        [Inject]
        private NavigationManager NavigationManager { get; set; } = default!;

        [Inject]
        private AuthenticationStateProvider AuthenticationStateProvider { get; set; } = default!;

        [Parameter]
        [SupplyParameterFromQuery(Name = "returnUrl")]
        public string ReturnUrl { get; set; }

        [CascadingParameter]
        public Error Error { get; set; }

        [Parameter]
        public string[] Roles { get; set; }

        private LoginModel _loginModel = new LoginModel();

        protected override async Task OnInitializedAsync()
        {
            var authenticationState = await AuthenticationStateProvider.GetAuthenticationStateAsync();

            if (authenticationState.User.Identity.IsAuthenticated)
            {
                NavigationManager.NavigateTo("/");
                return;
            }
        }

        private async Task HandleLogin()
        {
            try
            {
                _loginModel.Roles = Roles;
                var result = await AuthService.Login(_loginModel);
                NavigationManager.NavigateTo($"/{ReturnUrl}", forceLoad: true);
                return;
            }
            catch (Exception ex)
            {
                Error.ProcessError(ex);
            }
        }
    }
}
