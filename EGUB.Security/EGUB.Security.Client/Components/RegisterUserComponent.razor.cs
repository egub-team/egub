﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using EGUB.Security.Web.Models;
using EGUB.Security.Web.Services;
using EGUB.Core.Web.Components;

namespace EGUB.Security.Client.Components
{
    public partial class RegisterUserComponent
    {
        [Inject]
        private IAuthService AuthService { get; set; } = default!;
        [Inject]
        private NavigationManager NavigationManager { get; set; } = default!;
        [Inject]
        private AuthenticationStateProvider AuthenticationStateProvider { get; set; } = default!;

        [CascadingParameter]
        public Error Error { get; set; }

        private RegisterModel _model = new RegisterModel();

        protected override async Task OnInitializedAsync()
        {
            var authenticationState = await AuthenticationStateProvider.GetAuthenticationStateAsync();

            if (authenticationState.User.Identity.IsAuthenticated)
            {
                NavigationManager.NavigateTo("/");
                return;
            }
        }

        private async Task HandleRegistration()
        {
            try
            {
                await AuthService.Register(_model);
                NavigationManager.NavigateTo("/login");
            }
            catch (Exception ex)
            {
                Error.ProcessError(ex);
            }
        }
    }
}
