﻿using EGUB.Security.Web.Services;
using Microsoft.AspNetCore.Components;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System;
using Microsoft.AspNetCore.Components.Forms;

namespace EGUB.Security.Client.Components
{
    public class InputSelectRolesComponentBase : InputBase<string[]>
    {
        [Inject]
        private IUserRoleProvider UserRoleProvider { get; set; } = default!;

        [Parameter]
        public RenderFragment ChildContent { get; set; }


        protected ICollection<ItemInfo> _items = new List<ItemInfo>();

        protected override void OnInitialized()
        {
            base.OnInitialized();


            _items = UserRoleProvider.GetUserRoles()
                .Select(i =>
                    new ItemInfo
                    {
                        Key = i.Key,
                        Name = i.Value,
                        Value = this.CurrentValue.Contains(i.Key)
                    })
                .ToList();
        }


        public string[] HandleChange
        {
            get { return CurrentValue; }
            set { CurrentValue = value; }
        }

        protected override bool TryParseValueFromString(string value, [MaybeNullWhen(false)] out string[] result, [NotNullWhen(false)] out string validationErrorMessage)
        {
            throw new NotSupportedException();
        }

        protected void ItemChanged(bool selected, ItemInfo item)
        {
            item.Value = selected;

            this.CurrentValue = _items.Where(i => i.Value).Select(i => i.Key).ToArray();
        }

        protected class ItemInfo
        {
            public string Key { get; set; }
            public string Name { get; set; }
            public bool Value { get; set; }
        }
    }
}
