﻿using System.Threading.Tasks;
using EGUB.Security.Web.Services;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components;

namespace EGUB.Security.Client.Components
{
    public partial class LogoutComponent
    {
        [Inject]
        private IAuthService AuthService { get; set; } = default!;
        [Inject]
        private NavigationManager NavigationManager { get; set; } = default!;
        [Inject]
        private AuthenticationStateProvider AuthenticationStateProvider { get; set; } = default!;
        
        protected override async Task OnInitializedAsync()
        {
            var authenticationState = await AuthenticationStateProvider.GetAuthenticationStateAsync();

            if (!authenticationState.User.Identity.IsAuthenticated)
            {
                NavigationManager.NavigateTo("/");
                return;
            }

            await AuthService.Logout();
            NavigationManager.NavigateTo("/", true);
        }
    }
}
