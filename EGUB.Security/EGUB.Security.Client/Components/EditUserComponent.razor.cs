﻿using System;
using System.Linq;
using System.Threading.Tasks;
using EGUB.Security.Web.Models;
using EGUB.Security.Web.Services;
using EGUB.Core.Web.Components;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using EGUB.Security.Web;

namespace EGUB.Security.Client.Components
{
    public partial class EditUserComponent
    {
        [Inject]
        private IUserService UserService { get; set; } = default!;

        [Inject]
        private NavigationManager NavigationManager { get; set; } = default!;

        [Inject]
        private AuthenticationStateProvider AuthenticationStateProvider { get; set; } = default!;

        [Parameter]
        public Guid UserId { get; set; }

        [CascadingParameter]
        public Error Error { get; set; }

        private EditUserModel _info = new();
        
        private UserStatus[] _statuses = Enum.GetValues(typeof(UserStatus)).Cast<UserStatus>().ToArray();

        protected override async Task OnInitializedAsync()
        {
            try
            {
                var authenticationState = await AuthenticationStateProvider.GetAuthenticationStateAsync();

                if (!authenticationState.User.Identity.IsAuthenticated)
                {
                    NavigationManager.NavigateTo($"/login?returnUrl=editUser/{UserId}");
                    return;
                }

                if (!authenticationState.User.IsInRole(Constants.Roles.Admin))
                {
                    NavigationManager.NavigateTo("/");
                    return;
                }

                if (UserId == Guid.Empty)
                {
                    NavigationManager.NavigateTo($"/");
                    return;
                }

                _info = await UserService.GetUserDetails(UserId);
            }
            catch (Exception ex)
            {
                Error.ProcessError(ex);
            }
        }

        private async Task SaveUser()
        {
            try
            {
                await UserService.EditUser(_info);
                _info = await UserService.GetUserDetails(UserId);
                StateHasChanged();
            }
            catch (Exception ex)
            {
                Error.ProcessError(ex);
            }
        }

        private void Cancel()
        {
            NavigationManager.NavigateTo("/userlist");
        }
    }
}
