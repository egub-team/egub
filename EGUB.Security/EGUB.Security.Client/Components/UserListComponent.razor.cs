﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EGUB.Core.Web.Components;
using EGUB.Security.Web;
using EGUB.Security.Web.Models;
using EGUB.Security.Web.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.Forms;

namespace EGUB.Security.Client.Components
{
    public partial class UserListComponent
    {
        [Inject] 
        private IUserService UserService { get; set; } = default!;
        [Inject]
        private NavigationManager NavigationManager { get; set; } = default!;
        [Inject]
        private AuthenticationStateProvider AuthenticationStateProvider { get; set; } = default!;

        [CascadingParameter]
        public Error Error { get; set; }

        private EditContext _editContext;

        private List<UserListItemModel> _userItemsList = new List<UserListItemModel>();
        private SearchUsersModel _info = new SearchUsersModel
        {
            Roles = new[] { Constants.Roles.Member }
        };

        protected override async Task OnInitializedAsync()
        {
            _editContext = new(_info);
            var authenticationState = await AuthenticationStateProvider.GetAuthenticationStateAsync();

            if (!authenticationState.User.Identity.IsAuthenticated)
            {
                NavigationManager.NavigateTo($"/login?returnUrl=userlist");
                return;
            }

            if (!authenticationState.User.IsInRole(Constants.Roles.Admin))
            {
                NavigationManager.NavigateTo("/");
                return;
            }
        }

        private async Task BindItems()
        {
            try
            {
                var res = await UserService.SearchUsers(_info);
                _userItemsList = res.ToList();
            }
            catch (Exception ex)
            {
                Error.ProcessError(ex);
            }
        }

        private bool IsSearchDisable()
        {
            return !_editContext.Validate() ||
                (string.IsNullOrEmpty(_info.Code) &&
                string.IsNullOrEmpty(_info.Email) &&
                string.IsNullOrEmpty(_info.PhoneNumber) &&
                string.IsNullOrEmpty(_info.PersonName) &&
                !_info.Roles.Any());
        }

    }
}
