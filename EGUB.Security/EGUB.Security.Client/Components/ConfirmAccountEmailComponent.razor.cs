﻿using EGUB.Security.Web.Models;
using EGUB.Security.Web.Services;
using EGUB.Core.Web.Components;
using Microsoft.AspNetCore.Components;
using System;
using System.Threading.Tasks;

namespace EGUB.Security.Client.Components
{
    public partial class ConfirmAccountEmailComponent
    {
        [Inject]
        private IAuthService AuthService { get; set; } = default!;

        [Inject]
        private NavigationManager NavigationManager { get; set; } = default!;

        [Parameter]
        public Guid UserId { get; set; }

        [CascadingParameter]
        public Error Error { get; set; }

        private ConfirmEmailModel _info = new ConfirmEmailModel();

        private async Task HandleConfirmEmail()
        {
            try
            {
                _info.Id = UserId;
                await AuthService.ConfirmEmail(_info);
                NavigationManager.NavigateTo("/");
            }
            catch (Exception ex)
            {
                Error.ProcessError(ex);
            }
        }
    }
}
