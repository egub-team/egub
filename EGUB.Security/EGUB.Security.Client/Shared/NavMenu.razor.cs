﻿using Microsoft.AspNetCore.Components;
using EGUB.Core.Web.Components;

namespace EGUB.Security.Client.Shared
{
    public partial class NavMenu
    {
        private bool collapseNavMenu = true;

        private string NavMenuCssClass => collapseNavMenu ? "collapse" : null;

        [CascadingParameter]
        public Error Error { get; set; }

        private void ToggleNavMenu()
        {
            collapseNavMenu = !collapseNavMenu;
            ClearErrorMessages();
        }

        private void ClearErrorMessages()
        {
            Error.ClearErrorMessages();
        }
    }
}
