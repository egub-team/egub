using EGUB.Security.Web;
using MatBlazor;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using System.Threading.Tasks;

namespace EGUB.Security.Client
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);

            builder.RootComponents.Add<App>("#app");
            builder.RootComponents.Add<HeadOutlet>("head::after");

            builder.Services.AddSecurity(builder.HostEnvironment.BaseAddress);
            //builder.Services.AddTestSecurity(builder.HostEnvironment.BaseAddress);

            builder.Services.AddMatBlazor();

            await builder.Build().RunAsync();
        }
    }
}