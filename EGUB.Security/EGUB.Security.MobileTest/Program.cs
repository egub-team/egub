using EGUB.Security.Mobile.Extensions;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;

var builder = WebAssemblyHostBuilder.CreateDefault(args);
builder.RootComponents.Add<EGUB.Security.Mobile.App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");

builder.Services.AddTestSecurityMobile(builder.HostEnvironment.BaseAddress, null);
//builder.Services.AddSecurityMobile(builder.HostEnvironment.BaseAddress, null);

await builder.Build().RunAsync();
