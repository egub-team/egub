﻿using EGUB.Security.Mobile.Extensions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;
using System.Reflection;

namespace EGUB.Security.MobileClient
{
    public static class MauiProgram
    {
        public static MauiApp CreateMauiApp()
        {
            var builder = MauiApp.CreateBuilder();
            builder
                .UseMauiApp<App>()
                .ConfigureFonts(fonts =>
                {
                    fonts.AddFont("OpenSans-Regular.ttf", "OpenSansRegular");
                });

            var assembly = typeof(MauiProgram).GetTypeInfo().Assembly;
            var config = new ConfigurationBuilder()
                .AddJsonFile(new EmbeddedFileProvider(assembly), "appsettings.json", optional: false, false)
                .Build();
            builder.Services.AddSingleton<IConfiguration>(config);
            builder.Services.AddMauiBlazorWebView();
            builder.Services.AddSecurityMobile(string.Empty, GetPlatformMessageHandler);

#if DEBUG
            builder.Services.AddBlazorWebViewDeveloperTools();
    		builder.Logging.AddDebug();
#endif

            return builder.Build();
        }

        private static HttpMessageHandler GetPlatformMessageHandler()
        {
#if ANDROID
            var handler = new Xamarin.Android.Net.AndroidMessageHandler();
            handler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) =>
            {
                if (cert != null && cert.Issuer.Equals("CN=localhost"))
                    return true;
                return errors == System.Net.Security.SslPolicyErrors.None;
            };
            return handler;
#elif IOS
            var handler = new NSUrlSessionHandler
            {
                TrustOverrideForUrl = IsHttpsLocalhost
            };
            return handler;
#else
            throw new PlatformNotSupportedException("Only Android and iOS supported.");
#endif
        }

#if IOS
        private static bool IsHttpsLocalhost(NSUrlSessionHandler sender, string url, global::Security.SecTrust trust)
        {
            return url.StartsWith("https://localhost");
        }
#endif
    }
}
