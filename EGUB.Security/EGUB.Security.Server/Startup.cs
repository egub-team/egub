﻿using Autofac;
using EGUB.Core.Contracts.DAL;
using EGUB.Security.Api;
using EGUB.Security.DAL;
using Microsoft.Extensions.Configuration;
using IContainer = EGUB.Core.Contracts.IContainer;

namespace EGUB.Security.Server
{
    public class Startup : SecurityStartup
    {
        public const string SecurityContext = nameof(SecurityContext);
        protected override string Title => "EGUB.Security.Server";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterModule<Core.Common.ContainerFactory>();
            builder.RegisterModule<Core.DAL.ContainerFactory>();

            builder
                .RegisterInstance(this.Configuration)
                .SingleInstance();
            builder.RegisterModule<DAL.ContainerFactory>();
            builder
                .Register(c => new SecurityMigrator(SecurityContext, false, c.Resolve<IContainer>(), c.Resolve<IConfiguration>()))
                .Named<IMigrator>(SecurityContext)
                .InstancePerLifetimeScope();

            BLL.ContainerFactory.RegisterUserService(builder, SecurityContext);
            builder.RegisterModule<Api.ContainerFactory>();
        }
    }
}
